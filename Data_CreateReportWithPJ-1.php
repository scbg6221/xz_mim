﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
	$DBtable7="m_main_table";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
date_default_timezone_set('Asia/Taipei');

$Button_Date=$_POST['Button_DateSearch'];
if($Button_Date){
	foreach($_POST['Infor1'] as $nn){
		$Status_loc[$nn] = "checked";
	}
	$Status1=implode (',',$_POST['Infor1']);
	$Date1=$_POST['Infor3'];
	$Date2=$_POST['Infor4'];
	$Infortxt6=$_POST['Infor6'];
	$Infortxt7=$_POST['Infor7'];
	$Infortxt8=$_POST['Infor8'];
	$Infortxt9=$_POST['Infor9'];
}else{
	$Status1 = '10' ;
	$Status_loc[10] = "checked";
	$Date1=date("Y-m-d",strtotime("-7 day"));
	$Date2=date("Y-m-d");
	$Infortxt6='';
	$Infortxt7='';
	$Infortxt8='';
	$Infortxt9='';
};

mysqli_select_db($connect,$database);
if (strstr($Status1,"9")==true && strstr($Status1,"10")==true){
	mysqli_select_db($connect2,$database2);
	$query_listout1="SELECT * FROM (SELECT * FROM fq_molding_ipqc_proj.".$DBtable7." WHERE End_Date BETWEEN '".$Date1."' AND '".$Date2."')AS T1 RIGHT JOIN (SELECT * FROM fq_icbu_ipqc_gv.".$DBtable3." WHERE Status IN (9,".$Status1.") and DateTime BETWEEN '".$Date1."' AND '".$Date2."' and PartNumber LIKE '%".$Infortxt6."%' AND PartMold LIKE '%".$Infortxt7."%' AND FormingMachine LIKE '%".$Infortxt8."%' AND TicketNumber LIKE '%".$Infortxt9."%')AS T2 ON T1.Ticket_Number = T2.TicketNumber ORDER BY T2.ServiceNumber Desc";
	$listout1 = mysqli_query($connect,$query_listout1) or die(mysqli_error());
}elseif(strstr($Status1,"10")==true){
	$query_listout1="SELECT * FROM (SELECT * FROM fq_molding_ipqc_proj.".$DBtable7." WHERE End_Date BETWEEN '".$Date1."' AND '".$Date2."')AS T1 INNER JOIN (SELECT * FROM fq_icbu_ipqc_gv.".$DBtable3." WHERE Status IN (9,".$Status1.") and DateTime BETWEEN '".$Date1."' AND '".$Date2."' and PartNumber LIKE '%".$Infortxt6."%' AND PartMold LIKE '%".$Infortxt7."%' AND FormingMachine LIKE '%".$Infortxt8."%' AND TicketNumber LIKE '%".$Infortxt9."%')AS T2 ON T1.Ticket_Number = T2.TicketNumber ORDER BY T2.ServiceNumber Desc";
	$listout1 = mysqli_query($connect,$query_listout1) or die(mysqli_error());
}else{
	$query_listout1="SELECT * FROM ".$DBtable3." WHERE Status IN (".$Status1.") and DateTime BETWEEN '".$Date1."' AND '".$Date2."' and PartNumber LIKE '%".$Infortxt6."%' AND PartMold LIKE '%".$Infortxt7."%' AND FormingMachine LIKE '%".$Infortxt8."%' AND TicketNumber LIKE '%".$Infortxt9."%' ORDER BY ServiceNumber Desc";
	$listout1 = mysqli_query($connect,$query_listout1) or die(mysqli_error());
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CreateReportWithPJ-1.php</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/tableExport/dist/tableExport.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<script type="text/javascript" src="../../Public/library/JQuery/blockui_plugin/jquery.blockUI.js"></script> 

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 1050px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #FFDD55;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
.sortable1 {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-top: 0.5%;
	width: 1050px;
}
.sortable1 td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-right: 1px solid #fff;
}
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:14px;
	}
Z1{
	background-color:#ffffff;
	color:#0080FF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#ffffff;
	color:#FF0080;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	background-color:#ffffff;
	color:#00E800;
	font-weight:bolder;
	font-size:16px;
	}
Z4{
	color:#000000 ;
	font-size:10px;
	}
Z5{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
Z6{
	background-color:#FF3333;
	font-weight:bolder;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#B9FFB7;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#CCEEFF;
	font-size:10px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:10px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#BFBFFF;
	font-size:10px;
	}

</style>

<script>
$(function(){
	parent.document.getElementById("bottom_frameset").setAttribute("cols","195,*");
	parent.document.getElementById("Index_Content").style.backgroundImage="url(Images/b2.jpg)";
	parent.document.getElementById("Index_Content").style.padding="0px 0px 0px 25px";
	
	$( "#Infor3" ).datepicker(
		$.extend(
		$.datepicker.regional['zh-TW'],
		{
//			maxDate:5,
//			minDate:0,
			dateFormat:"yy-mm-dd",
      		onClose: function( selectedDate ) {$( "#Infor4" ).datepicker( "option", "minDate", selectedDate );},
    	}));
    $( "#Infor4" ).datepicker(
		$.extend(
		$.datepicker.regional['zh-TW'],
		{
			maxDate:0,
//			minDate:0,
			dateFormat:"yy-mm-dd",
    	}));
});

function DateSearch(){
	document['form1'].action = 'Data_CreateReportWithPJ-1.php?floor=<?php echo $floor ?>';
    document['form1'].target = 'Index_Content';
}
function CheckboxNumber(obj){
	var checkboxs = document.getElementsByName('Infor5[]');
	var CK_N=0;
    for(var i=0;i<checkboxs.length;i++){
		if(checkboxs[i].checked==true){
			CK_N=CK_N+1;
		}
	};
	if(CK_N>100){
		alert('請勿選擇超過100份報告，請重新選擇。');
		obj.checked = false;
	}
}
function AllCheck(obj,cName){
	var checkboxs = document.getElementsByName(cName);
	if(checkboxs.length>100){
		alert('請勿選擇超過100份報告，請重新選擇。');
		obj.checked = false;
	}else{
    	for(var i=0;i<checkboxs.length;i++){checkboxs[i].checked = obj.checked;};
	};
}
function ReportCreate(){
	parent.document.getElementById("bottom_frameset").setAttribute("cols","1,*");
	parent.document.getElementById("Index_Content").style.backgroundImage="url(Images/b3.jpg)";
	parent.document.getElementById("Index_Content").style.padding="0px 0px 0px 0px";
	$.blockUI({
		message:$('<h1>報告刷新中，請稍後...</h1><img src="./Images/foxlinklogo/WaitPic1.gif"/>'),
		css:{
			border: 'none', 
            padding: '15px', 
			border: '3px solid #aaa',
            backgroundColor: '#fff', 
            opacity: .8,
		},
		centerX: true, 
    	centerY: true, 
	});
	document['form1'].action = 'Data_CreateReportWithPJ-2.php?floor=<?php echo $floor ?>';
    document['form1'].target = 'Index_Content';
}

</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<table id="table-1" class="sortable1">
<tr>
	<td align="center"><Z0>報告狀態 :</Z0></td>
    <td align="center" colspan="3">
        <input type="checkbox" name="Infor1[]" id="Infor1[]" style="width:20px; height:20px; text-align:center;" value="10" <?php echo $Status_loc[10] ?>><Z3>GV & PJ 完成報告</Z3> &Iota;
        <input type="checkbox" name="Infor1[]" id="Infor1[]" style="width:20px; height:20px; text-align:center;" value="9" <?php echo $Status_loc[9] ?>><Z1>GV 完成報告</Z1>
    </td>
    <td align="center"><Z0>產品機種 :</Z0></td>
    <td align="center">
        <input type="text" name="Infor6" id="Infor6" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Infortxt6 ?>">
    </td>
    <td align="center"><Z0>模具編號 :</Z0></td>
    <td align="center">
        <input type="text" name="Infor7" id="Infor7" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Infortxt7 ?>">
    </td>
    <td rowspan="3">
        <input type="submit" name="Button_DateSearch" id="Button_DateSearch" class="BT1" style="font-size:15px; font-weight:bolder; width:100px; height:30px" value="搜尋案件" onclick="DateSearch()">
    </td>
</tr>
<tr>
	<td align="center"><Z0>生產日期 :</Z0></td>
    <td align="center" colspan="3">
        <input type="text" name="Infor3" id="Infor3" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Date1 ?>">
		<z>─</z>
        <input type="text" name="Infor4" id="Infor4" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Date2 ?>">
    </td>
    <td align="center"><Z0>成型機台 :</Z0></td>
    <td align="center">
        <input type="text" name="Infor8" id="Infor8" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Infortxt8 ?>">
    </td>
    <td align="center"><Z0>產品批號 :</Z0></td>
    <td align="center">
        <input type="text" name="Infor9" id="Infor9" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Infortxt9 ?>">
    </td>
</tr>
</table>
<br>
<?php echo "【 點擊表頭可以更改排序 】"?>&ensp;&ensp;&ensp;&ensp;&ensp;
<input type="checkbox" name="Button_AllCheck" id="Button_AllCheck" onclick="AllCheck(this,'Infor5[]')"><Z0>全部 勾選</Z0></input>&ensp;&ensp;&ensp;&ensp;&ensp;
<input type="submit" name="Button_ReportCreate" id="Button_ReportCreate" class="BT3" style="font-size:15px; font-weight:bolder; width:100px; height:30px" value="下載 報告" onclick="ReportCreate()">
<br>
<table id="table-T" class="sortable">
<thead>
	<th>報告狀態【共<?php echo mysqli_num_rows($listout1); ?>份】</th>
    <th>勾選</th>
	<th>產品批號</th>
	<th>日期</th>
	<th>產品機種</th>
	<th>模具編號</th>
	<th>成型機台</th>
	<th>GV量測</th>
	<th>PJ量測</th>
</thead>
<?php
$N=0;
while($listout = mysqli_fetch_assoc($listout1)){
	$N=$N+1;
	echo "<tr height=35px>";
	$PJcheck=0;
	if ($listout['End_Time']){
		$PJcheck=1;
	};
	if ($PJcheck==1){
		echo "<td><Z3>GV & PJ 完成報告</Z3></td>";
	}else{
		echo "<td><Z1>GV 完成報告</Z1></td>";
	}
	echo "<td><Z0>"."<input type='checkbox' name='Infor5[]' id='Infor5[]' style='width:20px;height:20px;' value='".$listout['ServiceNumber']."' onclick='CheckboxNumber(this)'>" . "</Z0></td>";
	echo "<td><Z0>" . $listout['TicketNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['DateTime'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['PartNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['PartMold'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['FormingMachine'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['GV_Inspector'] . "</Z0><br><Z4>" . substr($listout['EndDate'],5,11) . "</Z4></td>";
	if ($PJcheck==1){
		echo "<td><Z0>" . $listout['IPQC_Inspector'] . "</Z0><br><Z4>" . substr($listout['End_Date'],5,5). " " .substr($listout['End_Time'],0,5) . "</Z4></td>";
	}else{
		echo "<td><Z0>-</Z0></td>";
	}
echo "</tr>";
}
?>
</table>
</form>
</body>
</html>
