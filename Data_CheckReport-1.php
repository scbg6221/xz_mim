﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$SN=$_POST['SN'];
$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d H:i:s");
if (date("H")<8){
	$DatePoint=date("Y-m-d 08:00:00",strtotime("-7 day"));
}else{
	$DatePoint=date("Y-m-d 08:00:00");
}

if ($SN){
	mysqli_select_db($connect,$database);
	
	$sql="SELECT MAX(Sequence) FROM ( SELECT Sequence FROM ".$DBtable5." WHERE InspectionMethod = 'GV' and StartTime > '".$DatePoint."' ) AS T";
	$query= mysqli_query($connect,$sql) ;
	$AA = mysqli_fetch_array($query);
	
	$sql="SELECT InspectionMethod , Sequence , GVDim FROM ".$DBtable5." WHERE ServiceNumber = '".$SN."' and Sequence <> '0' and InspectionMethod = 'GV' ";
	$query= mysqli_query($connect,$sql) ;
	$checkgv = mysqli_fetch_array($query);
	$sql="SELECT InspectionMethod , Sequence , GVDim FROM ".$DBtable5." WHERE ServiceNumber = '".$SN."' and Sequence <> '0' and InspectionMethod = 'PJ' ";
	$query= mysqli_query($connect,$sql) ;
	$checkpj = mysqli_fetch_array($query);
	//$query_listoutT="SELECT * FROM ".$DBtable3." WHERE ServiceNumber = '".$SN."'";
	$query_listoutT="SELECT a.*,b.`Machine_Number` FROM `".$DBtable3."` a LEFT OUTER JOIN `machine_list` b ON a.`MeasureMachine`=b.`id` WHERE a.`ServiceNumber` = '".$SN."'";

	$listoutT = mysqli_query($connect,$query_listoutT) or die(mysqli_error());
	$listoutT = mysqli_fetch_assoc($listoutT);
	
	
	$query_listoutD="SELECT T1.ServiceNumber, T1.DimNO, T1.DimNOOrder, T1.DimSpec, T1.DimUpper, T1.DimLow, T1.Sample1, T1.Sample1Modify, T1.Sample2, T1.Sample2Modify, T1.Sample3, T1.Sample3Modify, T1.Sample4, T1.Sample4Modify, T1.Sample5, T1.Sample5Modify, T1.Sample6, T1.Sample6Modify, T1.Sample7, T1.Sample7Modify, T1.Sample8, T1.Sample8Modify, T2.Sample9, T2.Sample9Modify, T2.Sample10, T2.Sample10Modify, T2.Sample11, T2.Sample11Modify, T2.Sample12, T2.Sample12Modify, T2.Sample13, T2.Sample13Modify, T2.Sample14, T2.Sample14Modify, T2.Sample15, T2.Sample15Modify, T2.Sample16, T2.Sample16Modify, T1.SCAR FROM (SELECT * FROM ".$DBtable4." WHERE ServiceNumber = '".$SN."')AS T1 LEFT JOIN (SELECT * FROM ".$DBtable6." WHERE ServiceNumber = '".$SN."')AS T2 ON T1.DimNO=T2.DimNO AND T1.DimSpec=T2.DimSpec ORDER BY T1.DimNOOrder ASC";
	$listoutD = mysqli_query($connect,$query_listoutD) or die(mysqli_error());

	$N=0;
	$listoutD_array = array();
	while($listout = mysqli_fetch_assoc($listoutD)){
		$upperN=$listout['DimSpec'] + $listout['DimUpper']+0.00001;
		$lowN=$listout['DimSpec'] + $listout['DimLow']-0.00001;
		if($listoutT['CavityNumber']>8){
			$listoutD_array[$N] = array($listout['ServiceNumber'],$listout['DimNO'],$listout['DimNOOrder'],$listout['DimSpec'],$listout['DimUpper'],$listout['DimLow'],$listout['Sample1'],$listout['Sample1Modify'],$listout['Sample2'],$listout['Sample2Modify'],$listout['Sample3'],$listout['Sample3Modify'],$listout['Sample4'],$listout['Sample4Modify'],$listout['Sample5'],$listout['Sample5Modify'],$listout['Sample6'],$listout['Sample6Modify'],$listout['Sample7'],$listout['Sample7Modify'],$listout['Sample8'],$listout['Sample8Modify'],$listout['Sample9'],$listout['Sample9Modify'],$listout['Sample10'],$listout['Sample10Modify'],$listout['Sample11'],$listout['Sample11Modify'],$listout['Sample12'],$listout['Sample12Modify'],$listout['Sample13'],$listout['Sample13Modify'],$listout['Sample14'],$listout['Sample14Modify'],$listout['Sample15'],$listout['Sample15Modify'],$listout['Sample16'],$listout['Sample16Modify'],$listout['SCAR'],between($lowN,$upperN, array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'],$listout['Sample5'],$listout['Sample6'],$listout['Sample7'],$listout['Sample8'],$listout['Sample9'],$listout['Sample10'],$listout['Sample11'],$listout['Sample12'],$listout['Sample13'],$listout['Sample14'],$listout['Sample15'],$listout['Sample16'])));
		}elseif($listoutT['CavityNumber']>4){
			$listoutD_array[$N] = array($listout['ServiceNumber'],$listout['DimNO'],$listout['DimNOOrder'],$listout['DimSpec'],$listout['DimUpper'],$listout['DimLow'],$listout['Sample1'],$listout['Sample1Modify'],$listout['Sample2'],$listout['Sample2Modify'],$listout['Sample3'],$listout['Sample3Modify'],$listout['Sample4'],$listout['Sample4Modify'],$listout['Sample5'],$listout['Sample5Modify'],$listout['Sample6'],$listout['Sample6Modify'],$listout['Sample7'],$listout['Sample7Modify'],$listout['Sample8'],$listout['Sample8Modify'],$listout['SCAR'],between($lowN,$upperN, array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'],$listout['Sample5'],$listout['Sample6'],$listout['Sample7'],$listout['Sample8'])));
		}else{
			$listoutD_array[$N] = array($listout['ServiceNumber'],$listout['DimNO'],$listout['DimNOOrder'],$listout['DimSpec'],$listout['DimUpper'],$listout['DimLow'],$listout['Sample1'],$listout['Sample1Modify'],$listout['Sample2'],$listout['Sample2Modify'],$listout['Sample3'],$listout['Sample3Modify'],$listout['Sample4'],$listout['Sample4Modify'],$listout['SCAR'],between($lowN,$upperN, array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'])));
		}	
		$N=$N+1;
	}
	$listoutD_num = $N;
}

function between($min, $max, $value){
	if(array_search("-999.0000",$value)!=0){
		$value=array_splice($value,0,array_search("-999.0000",$value));
	}
 //處理成陣列
	if (is_array($value)){
		$limit = $value;
	}else{
		$limit = explode(",", $value);
	}
 //合併成多個數值
	$value = array_merge($limit, $limit);
	$limit[] = $max;
	$limit[] = $min;
 //使用max及min函數判斷是否在區間內
	if ((max($limit) == $max && min($limit) == $min) || (max($value) == $max && min($value) == $min)){
		$result = 0;
	}else{
		$result = 1;
	}
	return $result;
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CheckReport-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../Public/library/buttons/js/buttons.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/buttons/css/buttons.css"/>

<style type="text/css">
#abgne_float_ad {
	display: none;
	position: absolute;
	background-color: #B7FF6F;
	text-align:center;
}
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 980px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #B9FFB7;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:16px;
	}
Z1{
	background-color:#D1BBFF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#FFB7DD;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	color:#00FF00;
	font-size:14px;
	}
Z4{
	color:#FF00FF ;
	font-size:14px;
	}
Z5{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
Z6{
	background-color:#FF3333;
	font-weight:bolder;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#99FF99;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#CCEEFF;
	font-size:12px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:12px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:12px;
	}

</style>

<script>
var OKE = 0;
var CavN = <?php echo $listoutT['CavityNumber'] ;?>;

var jsNum = <?php echo $listoutD_num ;?>;
var jsArray = new Array();//再声明一维
<?php 
for( $i=0;$i<$listoutD_num;$i++){
?>
   jsArray[<?php echo $i;?>]=new Array();  //再声明二维 
   jsArray[<?php echo $i;?>] = ["<?php echo join("\", \"", $listoutD_array[$i]); ?>"];
<?php
}
?>

$(window).load(function(){
	var $win = $(window),
		$ad = $('#abgne_float_ad').css('opacity', 0).show(),	// 讓廣告區塊變透明且顯示出來
		_width = $ad.width(),
		_height = $ad.height(),
		_diffY = 20, _diffX = 20,	// 距離右及下方邊距
		_moveSpeed = 800;	// 移動的速度
	// 先把 #abgne_float_ad 移動到定點
	$ad.css({
		top: $(document).height(),
		left: $win.width() - _width - _diffX,
		opacity: 1
	});
	// 幫網頁加上 scroll 及 resize 事件
	$win.bind('scroll resize', function(){
		var $this = $(this);
		// 控制 #abgne_float_ad 的移動
		$ad.stop().animate({
			top: $this.scrollTop() + $this.height() - _height - _diffY,
			left: $this.scrollLeft() + $this.width() - _width - _diffX
		}, _moveSpeed);
	}).scroll();	// 觸發一次 scroll()
});

function ChSample(N){
	if(CavN>8){
		document.getElementById('ChSample1').className="button button-pill button-flat-primary";
		document.getElementById('ChSample2').className="button button-pill button-flat-primary";
		document.getElementById('ChSample3').className="button button-pill button-flat-primary";
		document.getElementById('ChSample4').className="button button-pill button-flat-primary";
	}else if(CavN>4){
		document.getElementById('ChSample1').className="button button-pill button-flat-primary";
		document.getElementById('ChSample2').className="button button-pill button-flat-primary";
	}else{
		document.getElementById('ChSample1').className="button button-pill button-flat-primary";
	}
	document.getElementById('ChSample'+N).className="button button-pill button-flat-highlight";
	document.getElementById('table-D').rows[0].cells[4].innerHTML="樣品"+(N*4-3);
	document.getElementById('table-D').rows[0].cells[6].innerHTML="樣品"+(N*4-3+1);
	document.getElementById('table-D').rows[0].cells[8].innerHTML="樣品"+(N*4-3+2);
	document.getElementById('table-D').rows[0].cells[10].innerHTML="樣品"+(N*4-3+3);
	var TnameA
	var TnameB
	var TnameC
	var TnameD

	for(var j=0; j<jsNum; j++){
		
		var upperN=Number(jsArray[j][3])+Number(jsArray[j][4])+0.00001;
		var lowN=Number(jsArray[j][3])+Number(jsArray[j][5])-0.00001;
		
		if(jsArray[j][2*((N*4-3)-1)+6]>upperN){
			TnameA = "<Z1>"+jsArray[j][2*((N*4-3)-1)+6]+"</Z1>";
		}else if(jsArray[j][2*((N*4-3)-1)+6]<lowN && jsArray[j][2*((N*4-3)-1)+6]!='-999.0000'){
			TnameA = "<Z2>"+jsArray[j][2*((N*4-3)-1)+6]+"</Z2>";
		}else if(jsArray[j][2*((N*4-3)-1)+6]=='-999.0000'){
			TnameA = "N/A";
		}else{
			TnameA = jsArray[j][2*((N*4-3)-1)+6];
		}
		if(jsArray[j][2*((N*4-3)-1)+6+2]>upperN){
			TnameB = "<Z1>"+jsArray[j][2*((N*4-3)-1)+6+2]+"</Z1>";
		}else if(jsArray[j][2*((N*4-3)-1)+6+2]<lowN && jsArray[j][2*((N*4-3)-1)+6+2]!='-999.0000'){
			TnameB = "<Z2>"+jsArray[j][2*((N*4-3)-1)+6+2]+"</Z2>";
		}else if(jsArray[j][2*((N*4-3)-1)+6+2]=='-999.0000'){
			TnameB = "N/A";
		}else{
			TnameB = jsArray[j][2*((N*4-3)-1)+6+2];
		}
		if(jsArray[j][2*((N*4-3)-1)+6+4]>upperN){
			TnameC = "<Z1>"+jsArray[j][2*((N*4-3)-1)+6+4]+"</Z1>";
		}else if(jsArray[j][2*((N*4-3)-1)+6+4]<lowN && jsArray[j][2*((N*4-3)-1)+6+4]!='-999.0000'){
			TnameC = "<Z2>"+jsArray[j][2*((N*4-3)-1)+6+4]+"</Z2>";
		}else if(jsArray[j][2*((N*4-3)-1)+6+4]=='-999.0000'){
			TnameC = "N/A";
		}else{
			TnameC = jsArray[j][2*((N*4-3)-1)+6+4];
		}
		if(jsArray[j][2*((N*4-3)-1)+6+6]>upperN){
			TnameD = "<Z1>"+jsArray[j][2*((N*4-3)-1)+6+6]+"</Z1>";
		}else if(jsArray[j][2*((N*4-3)-1)+6+6]<lowN && jsArray[j][2*((N*4-3)-1)+6+6]!='-999.0000'){
			TnameD = "<Z2>"+jsArray[j][2*((N*4-3)-1)+6+6]+"</Z2>";
		}else if(jsArray[j][2*((N*4-3)-1)+6+6]=='-999.0000'){
			TnameD = "N/A";
		}else{
			TnameD = jsArray[j][2*((N*4-3)-1)+6+6];
		}
		
		document.getElementById('table-D').rows[j+2].cells[4].innerHTML=TnameA;
		document.getElementById('table-D').rows[j+2].cells[5].innerHTML=jsArray[j][2*((N*4-3)-1)+6+1];
		document.getElementById('table-D').rows[j+2].cells[6].innerHTML=TnameB;
		document.getElementById('table-D').rows[j+2].cells[7].innerHTML=jsArray[j][2*((N*4-3)-1)+6+3];
		document.getElementById('table-D').rows[j+2].cells[8].innerHTML=TnameC;
		document.getElementById('table-D').rows[j+2].cells[9].innerHTML=jsArray[j][2*((N*4-3)-1)+6+5];
		document.getElementById('table-D').rows[j+2].cells[10].innerHTML=TnameD;
		document.getElementById('table-D').rows[j+2].cells[11].innerHTML=jsArray[j][2*((N*4-3)-1)+6+7];
	};
};

function CkeckReport(){
	var GVVC = document.getElementById("GVV").textContent;
	var PJVC = document.getElementById("PJV").textContent;
	var SCARVC = document.getElementById("SCARV").textContent;
	var OKR = document.getElementById("OKR").value - OKE;
	if (OKR < 0){
		alert ("產品量測數據不全 ，\n\n請與量測室確認 。");
		return false;
	} else if (OKR == 0){
		sue = confirm('量測報告尺寸確認無誤 ，\n\n將完成此巡檢量測報告？')
		if (sue == true ){
			var SN = <?php echo $SN ?>;
			$("#form1").append("<input type=hidden name=GVT value='"+SN+":SC:"+SCARVC+"'/>");
			document['form1'].action = "Data_CheckReport-2.php?floor=<?php echo $floor ?>";
			document['form1'].target = 'Index_Content';
		}else{
			return false;
		};	
	}else{
		var NN = (GVVC.split(";").length-1)+(PJVC.split(";").length-1)
		if (OKR==NN){	
			sue = confirm('確認尺寸覆判方式 ： \n\nGV覆判 ： '+GVVC+'\n\n自行覆判 ： '+PJVC+'\n\n是否確認資料？')
			if (sue == true ){
				var SN = <?php echo $SN ?>;
				$("#form1").append("<input type=hidden name=GVT value='"+SN+"::"+GVVC+"::"+PJVC+"'/>");
				document['form1'].action = "Data_CheckReport-2.php?floor=<?php echo $floor ?>";
				document['form1'].target = 'Index_Content';
			}else{
				return false;
			};
		}else{
			alert ("尚有NG尺寸未確定覆判方式 ，\n\n請再次確認覆判方式 。");
			return false;
		};
	};
};
function GVGO(N){
	var dimno = document.getElementById('GV'+N).value;
	$("#GVV").append(dimno+'; ');
	document.getElementById('div'+N).innerHTML="<Z4>排定覆判 - GV</Z4>";
};
function PJGO(N){
	var dimno = document.getElementById('PJ'+N).value;
	$("#PJV").append(dimno+'; ');
	document.getElementById('div'+N).innerHTML="<Z4>排定覆判 - 自行</Z4>";
};
function CKGO(N){
//	var dimno = document.getElementById('CK'+N).value;
	document.getElementById('div'+N).innerHTML="<Z5>確認 NG</Z5>";
	OKE = OKE + 1 ;
};
function SCARGO(N){
	var dimno = document.getElementById('SCAR'+N).value;
	document.getElementById('div'+N).innerHTML="<input type=text size=12 name=SCART"+N+" id=SCART"+N+"><button type=Button class=BT1 name=SCARB"+N+" id=SCARB"+N+" value="+dimno+" onClick=SCARCK("+N+")>確認處理</button>";
};
function SCARCK(N){
	var SCARCKN = document.getElementById('SCART'+N).value;
	if (SCARCKN==''){
	}else{
		$("#SCARV").append(document.getElementById('SCARB'+N).value+'#'+document.getElementById('SCART'+N).value+'; ');
		$('#SCART'+N).attr("readonly","readonly");
		$('#SCART'+N).attr("style","width:50px;border:hidden;text-align:center;background-color:#f2f2f2");
		$('#SCARB'+N).remove();
		OKE = OKE + 1 ;
	}
};
</script>


</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<table id="table-T" class="sortable">
<thead>
	<th>報告狀態</th>
	<th>批號</th>
	<th>機種</th>
	<th>模號(穴數)</th>
	<th>成型機台</th>
	<th>GV測量員</th>
    <th>測量機台</th>
	<th>IPQC檢測員</th>
</thead>
<?php
echo "<tr height=35px>";
	switch ($listoutT['Status']){
		Case "0" : echo "<td><Z0>首次檢視報告</Z0></td>"; break;
		Case "1" : echo "<td><Z0>GV & 自行覆判</Z0></td>"; break;
		Case "2" : echo "<td><Z0>自行覆判</Z0></td>"; break;
		Case "3" : echo "<td><Z0>GV覆判</Z0></td>"; break;
		Case "4" : echo "<td><Z0>重複檢視報告</Z0></td>"; break;
		Case "9" : echo "<td><Z0>報告確認完成</Z0></td>"; break;
	}
	
	echo "<td><Z0>" . $listoutT['TicketNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['PartNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['PartMold'] . "(" . $listoutT['CavityNumber'] . ")" . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['FormingMachine'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['GV_Inspector'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['Machine_Number'] . "</Z0></td>";
	if ($listoutT['Status']=="0"){
		echo "<td><Z0>" . $ChineseName . "-" . $ID . "</Z0></td>";
	}else{
		echo "<td><Z0>" . $listoutT['IPQC_Inspector'] . "</Z0></td>";
	}
echo "</tr>";
?>
<thead>
	<th>GV覆判案件</th>
	<th colspan="3">重新送GV室覆判的尺寸</th>
	<th colspan="3">自行覆判的尺寸</th>
	<th>提交申請</th>
</thead>
<?php
echo "<tr>";
switch ($listoutT['Status']){
	Case "0" : $BB = "<td><button type=submit id=Bck class=BT0 onClick='return CkeckReport()'>送出</td>";$AA['0']="排定 ".($AA['0']+1)." 件";break;
	Case "4" : $BB = "<td><button type=submit id=Bck class=BT0 onClick='return CkeckReport()'>送出</td>";$AA['0']="排定 ".($AA['0']+1)." 件";break;
	Case "1" : $BB = "<td><z0>覆判中</Z0></td>";$GVV=$checkgv['GVDim'];$PJV=$checkpj['GVDim'];$AA['0']="等候 ".($checkgv['Sequence'])." 件";break;
	Case "2" : $BB = "<td><z0>覆判中</Z0></td>";$PJV=$checkpj['GVDim'];$AA['0']="-";break;
	Case "3" : $BB = "<td><z0>覆判中</Z0></td>";$GVV=$checkgv['GVDim'];$AA['0']="等候 ".($checkgv['Sequence'])." 件";break;
	Case "9" : $BB = "<td><z0>已完成</Z0></td>"; break;
}
echo "<td height=35px><Z0>".($AA['0'])."</Z0></td>";
echo "<td width=350px colspan=3><div name=GVV id=GVV>".$GVV."</div></td>";
echo "<td width=350px colspan=3><div name=PJV id=PJV>".$PJV."</div></td>";
echo $BB;
echo "</tr>";
?>
</table>
<table id="table-D" class="sortable">
<thead>
<tr>
    <th>尺寸名稱</th>
    <th>規格</th>
    <th>上公差</th>
    <th>下公差</th>
    <th id='Tname_A' name='Tname_A'>樣品1</th>
    <th id='Tchack_A' name='Tchack_A'>覆判</th>
    <th id='Tname_B' name='Tname_B'>樣品2</th>
    <th id='Tchack_B' name='Tchack_B'>覆判</th>
    <th id='Tname_C' name='Tname_C'>樣品3</th>
    <th id='Tchack_C' name='Tchack_C'>覆判</th>
    <th id='Tname_D' name='Tname_D'>樣品4</th>
    <th id='Tchack_D' name='Tchack_D'>覆判</th>
    <th>操作欄位</th>
<tr>
</thead>
<?php
$N=0;
while($listout = $listoutD_array[$N]){
	$N=$N+1;
	echo "<tr>";
	echo "<td style='border-right:0.1px solid #000'>" . $listout[1] . "<input type=hidden name=DimNO".$N." id=DimNO".$N." value=".$listout[1]."></td>";
	echo "<td style='border-right:0.1px solid #000'>" . $listout[3] . "</td>";
	echo "<td>" . $listout[4] . "</td>";
	echo "<td style='border-right:0.1px solid #000'>" . $listout[5] . "</td>";
	
	$upperN=$listout[3] + $listout[4]+0.00001;
	$lowN=$listout[3] + $listout[5]-0.00001;

	if ($listout[6]>$upperN){
		echo "<td><Z1>" . $listout[6] . "</Z1></td>";
	}elseif($listout[6]<$lowN && $listout[6]!='-999.0000'){
		echo "<td><Z2>" . $listout[6] . "</Z2></td>";
	}elseif($listout[6]=='-999.0000'){
		echo "<td>N/A</td>";
	}else{
		echo "<td>" . $listout[6] . "</td>";
	}
	echo "<td>" . $listout[7] . "</td>";
		
	if ($listout[8]>$upperN){
		echo "<td><Z1>" . $listout[8] . "</Z1></td>";
	}elseif($listout[8]<$lowN && $listout[8]!='-999.0000'){
		echo "<td><Z2>" . $listout[8] . "</Z2></td>";
	}elseif($listout[8]=='-999.0000'){
		echo "<td>N/A</td>";
	}else{
		echo "<td>" . $listout[8] . "</td>";	
	}
	echo "<td>" . $listout[9] . "</td>";
	
	if ($listout[10]>$upperN){
		echo "<td><Z1>" . $listout[10] . "</Z1></td>";
	}elseif($listout[10]<$lowN && $listout[10]!='-999.0000'){
		echo "<td><Z2>" . $listout[10] . "</Z2></td>";
	}elseif($listout[10]=='-999.0000'){
		echo "<td>N/A</td>";
	}else{
		echo "<td>" . $listout[10] . "</td>";	
	}
	echo "<td>" . $listout[11] . "</td>";

	if ($listout[12]>$upperN){
		echo "<td><Z1>" . $listout[12] . "</Z1></td>";
	}elseif($listout[12]<$lowN && $listout[12]!='-999.0000'){
		echo "<td><Z2>" . $listout[12] . "</Z2></td>";
	}elseif($listout[12]=='-999.0000'){
		echo "<td>N/A</td>";
	}else{
		echo "<td>" . $listout[12] . "</td>";	
	}
	echo "<td style='border-right:0.1px solid #000'>" . $listout[13] . "</td>";
	
	if($listoutT['CavityNumber']>8){
		$checkarrayno=39;
	}elseif($listoutT['CavityNumber']>4){
		$checkarrayno=23;
	}else{
		$checkarrayno=15;
	}
	switch ($listoutT['Status']){
		Case "0" :
			if ($listout[$checkarrayno] == 1){
				$OKReport = $OKReport+1 ;
				echo "<td><div id=div".$N.">
				<button type=Button id=GV".$N." value=".$listout[1]." class=BT1 onClick=GVGO(".$N.")>委託GV</button>
				<button type=Button id=PJ".$N." value=".$listout[1]." class=BT2 onClick='PJGO(".$N.")'>自行覆判</button>"
				."</div></td>";
			}else{
				echo "<td><Z3>PASS</Z3></td>";
			} 
			break;
		Case "4" :
			if ($listout[$checkarrayno] == 1){
				$OKReport = $OKReport+1 ;
				echo "<td><div id=div".$N.">".
				"<button type=Button id=GV".$N." value=".$listout[1]." class=BT1 onClick=GVGO(".$N.")>委託GV</button>
				<button type=Button id=PJ".$N." value=".$listout[1]." class=BT2 onClick='PJGO(".$N.")'>自行覆判</button>
				<button type=Button id=SCAR".$N." value=".$listout[1]." class=BT3 onClick='SCARGO(".$N.")'>異常處理</button>"
				."</div></td>";
			}else{
				echo "<td><Z3>PASS</Z3></td>";
			} 
			break;
		Case "1" :
		Case "2" :
		Case "3" :
			if ($listout[$checkarrayno] == 1){
				echo "<td><Z4>排定覆判</Z4></td>";
			}else{
				echo "<td><Z3>PASS</Z3></td>";
			}
			break;
		Case "9" : 
			if ($listout[$checkarrayno] == 1){
				echo "<td><z5>已確認 NG</Z5></td>";
			}else{
				echo "<td><Z3>PASS</Z3></td>";
			}
			break;
	}
	echo "</tr>";
}
echo "<input type=hidden id=OKR value=".$OKReport.">";
echo "<div hidden name=SCARV id=SCARV></div>";
?>
</table>
</form>
	<div id="abgne_float_ad">
    	<?php 
			if($listoutT['CavityNumber']>8){
				echo "<botton type='Button' name='ChSample1' id='ChSample1' class='button button-pill button-flat-highlight' onClick='ChSample(1)'>樣品{1~4}</botton><br>
					<botton type='Button' name='ChSample2' id='ChSample2' class='button button-pill button-flat-primary' onClick='ChSample(2)'>樣品{5~8}</botton><br>
					<botton type='Button' name='ChSample3' id='ChSample3' class='button button-pill button-flat-primary' onClick='ChSample(3)'>樣品{9~12}</botton><br>
					<botton type='Button' name='ChSample4' id='ChSample4' class='button button-pill button-flat-primary' onClick='ChSample(4)'>樣品{13~16}</botton><br>";				
			}elseif($listoutT['CavityNumber']>4){
				echo "<botton type='Button' name='ChSample1' id='ChSample1' class='button button-pill button-flat-highlight' onClick='ChSample(1)'>樣品{1~4}</botton><br>
					<botton type='Button' name='ChSample2' id='ChSample2' class='button button-pill button-flat-primary' onClick='ChSample(2)'>樣品{5~8}</botton><br>
					<botton type='Button' name='ChSample3' id='ChSample3' hidden >樣品{9~12}</botton><br>
					<botton type='Button' name='ChSample4' id='ChSample4' hidden >樣品{13~16}</botton><br>";	
			}else{
				echo "<botton type='Button' name='ChSample1' id='ChSample1' class='button button-pill button-flat-highlight' onClick='ChSample(1)'>樣品{1~4}</botton><br>
					<botton type='Button' name='ChSample2' id='ChSample2' hidden >樣品{5~8}</botton><br>
					<botton type='Button' name='ChSample3' id='ChSample3' hidden >樣品{9~12}</botton><br>
					<botton type='Button' name='ChSample4' id='ChSample4' hidden >樣品{13~16}</botton><br";	
			}			
		?>
   	</div>
</body>
</html>
