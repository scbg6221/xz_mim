﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
date_default_timezone_set('Asia/Taipei');

$Button_Date=$_POST['Button_DateSearch'];
if($Button_Date){
	$Date1=$_POST['Infor3'];
	$Date2=$_POST['Infor4'];
}else{
	$Date1=date("Y-m-d",date(strtotime("-7 day")));
	$Date2=date("Y-m-d");
};
mysqli_select_db($connect,$database);
$query_listout1="SELECT InspectionMethod,count(*)AS Num,sum(UNIX_TIMESTAMP(EndTime)-UNIX_TIMESTAMP(StartTime))AS Time FROM (SELECT * FROM ".$DBtable5." WHERE ServiceNumber BETWEEN '".str_replace('-','',$Date1)."170000' AND '".date('Ymd', strtotime ("+1 day", strtotime($Date2)))."170000')AS T1 WHERE Sequence ='0' GROUP BY InspectionMethod ORDER BY InspectionMethod Asc";
$listout1 = mysqli_query($connect,$query_listout1) or die(mysqli_error());

$query_listout2="SELECT * FROM (SELECT *,InspectionMethod As InspectionMethod1,(UNIX_TIMESTAMP(EndTime)-UNIX_TIMESTAMP(StartTime))As TimeUse FROM ".$DBtable5." WHERE Sequence = '0' and ServiceNumber BETWEEN '".str_replace('-','',$Date1)."170000' AND '".date('Ymd', strtotime ("+1 day", strtotime($Date2)))."170000') AS T1 JOIN (SELECT * FROM ".$DBtable3." WHERE DateTime BETWEEN '".$Date1."' AND '".$Date2."') AS T2 WHERE T1.ServiceNumber = T2.ServiceNumber ORDER BY T1.ServiceNumber ASC";
$listout2 = mysqli_query($connect,$query_listout2) or die(mysqli_error());

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_Application_ForMeetReport-1.php</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/tableExport/dist/tableExport.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 1050px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #B9B9FF;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
.sortable1 {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-top: 0.5%;
	width: 1050px;
}
.sortable1 td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-right: 1px solid #fff;
}
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:14px;
	}
Z1{
	background-color:#ffffff;
	color:#0080FF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#ffffff;
	color:#FF0080;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	background-color:#ffffff;
	color:#00E800;
	font-weight:bolder;
	font-size:16px;
	}
Z4{
	color:#000000 ;
	font-size:8px;
	}
Z5{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
Z6{
	background-color:#FF3333;
	font-weight:bolder;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#B9FFB7;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#CCEEFF;
	font-size:10px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:10px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:10px;
	}

</style>

<script>
$(function(){
	$( "#Infor3" ).datepicker(
		$.extend(
		$.datepicker.regional['zh-TW'],
		{
//			maxDate:5,
//			minDate:0,
			dateFormat:"yy-mm-dd",
      		onClose: function( selectedDate ) {$( "#Infor4" ).datepicker( "option", "minDate", selectedDate );},
    	}));
    $( "#Infor4" ).datepicker(
		$.extend(
		$.datepicker.regional['zh-TW'],
		{
//			maxDate:14,
//			minDate:0,
			dateFormat:"yy-mm-dd",
    	}));
});

function DateSearch(){
	document['form1'].action = 'Data_Application_ForReport-2.php?floor=<?php echo $floor ?>';
    document['form1'].target = 'Index_Content';
}

</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<?php echo "【 點擊表頭可以更改排序 】"?>
<table id="table-1" class="sortable1">
<tr>
	<td align="center">委託日期 :</td>
    <td align="center">
        <input type="text" name="Infor3" id="Infor3" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Date1 ?>">
        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<z>─</z>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
        <input type="text" name="Infor4" id="Infor4" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Date2 ?>">&emsp;&emsp;&emsp;
    </td>
    <td rowspan="2">
        <input type="submit" name="Button_DateSearch" id="Button_Button_DateSearch" class="BT1" style="font-size:15px; font-weight:bolder; width:100px; height:30px" value="搜尋案件" onclick="DateSearch()">
    </td>
    <td rowspan="2">
        <a href="javascript:;"><img src="./Images/Download_excel.jpg" data-type="xls" id="export" border="0" width="50px" title="下載報告"></a>
    </td>
</tr>
</table>
<table id="table-2" class="sortable">
<thead>
	<th>覆判方式</th>
	<th>案件數</th>
	<th>覆判等待時間(分鐘)</th>
</thead>
<?php
while($listout = mysqli_fetch_assoc($listout1)){
	echo "<tr height=35px>";
	switch ($listout['InspectionMethod']){
		Case "PJ" : echo "<td><Z1>自行覆判</Z1></td>"; break;
		Case "GV" : echo "<td><Z2>GV覆判</Z2></td>"; break;
	}
	echo "<td><Z0>" . $listout['Num'] . "</Z0></td>";
	echo "<td><Z0>" . round($listout['Time']/60,2) . "</Z0></td>";
	echo "</tr>";
}
?>
</table>
<table id="table-T" class="sortable">
<thead>
	<th>覆判方式</th>
    <th>批號</th>
	<th>機種</th>
	<th>模號</th>
    <th>穴數</th>
	<th>量測尺寸數</th>
	<th>覆判等待時間(分鐘)</th>
</thead>
<?php
while($listoutT = mysqli_fetch_assoc($listout2)){
	echo "<tr height=35px>";
	switch ($listoutT['InspectionMethod1']){
		Case "PJ" : echo "<td><Z1>自行</Z1></td>"; break;
		Case "GV" : echo "<td><Z2>GV</Z2></td>"; break;
	}
	echo "<td><Z0>" . $listoutT['TicketNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['PartNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['PartMold'] . "</Z0></td>";
	echo "<td><Z0>" . $listoutT['CavityNumber'] . "</Z0></td>";
	echo "<td><Z0>" . substr_count($listoutT['GVDim'],';') ."</Z0></td>";
	echo "<td><Z0>" . round($listoutT['TimeUse']/60,2) ."</Z0></td>";
echo "</tr>";
}
?>

</form>

<script>
	var $exportLink = document.getElementById('export');
	var exportData1 = document.getElementById('Infor3').value;
	var exportData2 = document.getElementById('Infor4').value;
	$exportLink.addEventListener('click', function(e){
		e.preventDefault();
		if(e.target.nodeName == "IMG"){
			tableExport('table-T', '巡檢量測系統-案件覆判總表_'+exportData1+'_'+exportData2, e.target.getAttribute('data-type'));
		}
	}, false);
</script>

</body>
</html>
