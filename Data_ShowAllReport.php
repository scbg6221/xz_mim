﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$Del=$_POST['Del'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d H:i:s",strtotime("-7 day"));

if ($Del){
echo $Del;
}

mysqli_select_db($connect,$database);
//$query_listoutT="SELECT * FROM ".$DBtable3." WHERE MeasureEndTime > '".$DateTime."' ORDER BY ServiceNumber Asc";
$query_listoutT="SELECT a.*, b.`Machine_Number` FROM `".$DBtable3."` a LEFT OUTER JOIN `machine_list` b ON a.`MeasureMachine`=b.`id` WHERE a.`MeasureEndTime` > '".$DateTime."' ORDER BY a.`ServiceNumber` ASC";
$listoutT = mysqli_query($connect,$query_listoutT) or die(mysqli_error());

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CheckReport-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="../../Public/library/Other/autoscroll.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 980px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #00BBFF;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:14px;
	}
Z1{
	background-color:#ffffff;
	color:#0080FF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#ffffff;
	color:#FF0080;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	background-color:#ffffff;
	color:#00E800;
	font-weight:bolder;
	font-size:16px;
	}
Z4{
	color:#000000 ;
	font-size:8px;
	}
Z5{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
Z6{
	background-color:#FF3333;
	font-weight:bolder;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#99FF99;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#CCEEFF;
	font-size:10px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:10px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:10px;
	}

</style>

<script>
$(function(){
	window.autoscroll.interval = 25;
	window.autoscroll.threshold = 20;
	window.autoscroll.velocity = 10;
	window.autoscroll.acceleration = 0;
	window.autoscroll.start();
});
</script>

</head>
<body background="Images/loginb.png">
<?php echo "【 點擊表頭可以更改排序 】"?>
<form id="form1" name="form1" method="post" >
<table id="table-T" class="sortable">
<thead>
	<th>報告狀態</th>
	<th>機種</th>
	<th>模號</th>
	<th>成型機台</th>
	<th>批號</th>
	<th>GV測量員</th>
    <th>測量機台</th>
	<th>IPQC檢測員</th>
</thead>
<?php
$N=0;
while($listout = mysqli_fetch_assoc($listoutT)){
	$N=$N+1;
	echo "<tr height=35px>";
	switch ($listout['Status']){
		Case "0" : echo "<td><Z1>首次檢視報告</Z1></td>";$status=1; break;
		Case "1" : echo "<td><Z2>GV & 自行覆判</Z2></td>";$status=2; break;
		Case "2" : echo "<td><Z2>自行覆判</Z2></td>";$status=2; break;
		Case "3" : echo "<td><Z2>GV覆判</Z2></td>";$status=2; break;
		Case "4" : echo "<td><Z1>重複檢視報告</Z1></td>";$status=1; break;
		Case "9" : echo "<td><Z3>報告確認完成</Z3></td>";$status=0; break;
	}
	echo "<td><Z0>" . $listout['PartNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['PartMold'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['FormingMachine'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['TicketNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['GV_Inspector'] ."</Z0><br><Z4>上傳報告 ". substr($listout['MeasureEndTime'],5,11) . "</Z4></td>";
	echo "<td><Z0>" . $listout['Machine_Number'] . "</Z0></td>";
	if ($listout['Status']=="0"){
		echo "<td><Z0>" . $listout['IPQC_Inspector'] . "</Z0><br><Z4>尚未檢視報告</Z4></td>";
	}elseif($listout['Status']=="9"){
		echo "<td><Z0>" . $listout['IPQC_Inspector'] . "</Z0><br><Z4>送測時間 ". substr($listout['ServiceNumber'],4,2) ."-".substr($listout['ServiceNumber'],6,2) ." ".substr($listout['ServiceNumber'],8,2) .":". substr($listout['ServiceNumber'],10,2) ."</Z4></td>";
	}else{
		echo "<td><Z0>" . $listout['IPQC_Inspector'] . "</Z0><br><Z4>尚未完成報告</Z4></td>";
	}

echo "</tr>";
}
?>
</table>
</form>
</body>
</html>
