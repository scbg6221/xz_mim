﻿<?php
session_start();
error_reporting(0);
$floor=1;
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}

//include '../../Public/MainWebUI/User_Count.php';
//include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');
require_once('../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php');
require_once('../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php');
require_once('../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php');
require_once('../../Public/library/pclzip/pclzip-2-8-2/pclzip.lib.php');

$CallC="";
$AllSN=array();

mysqli_select_db($connect,$database);
$Search = "SELECT ServiceNumber FROM ".$DBtable3." WHERE DateTime BETWEEN '2017-03-16' AND '2017-03-20' and Status = '9' ORDER BY ServiceNumber ASC";
$Search_Report = mysqli_query($connect,$Search);

if($Search_Report){
	
	while($row=mysqli_fetch_array($Search_Report)){
		array_push($AllSN,$row['ServiceNumber']);
	}
	
	foreach($AllSN as $key => $value) {

		mysqli_select_db($connect,$database);
		$query_listoutT="SELECT * FROM ".$DBtable3." WHERE ServiceNumber = '"."{$value}"."'";
		$listoutT = mysqli_query($connect,$query_listoutT) or die(mysqli_error());
		$query_listoutD="SELECT T1.ServiceNumber, T1.DimNO, T1.DimNOOrder, T1.DimSpec, T1.DimUpper, T1.DimLow, T1.Sample1, T1.Sample1Modify, T1.Sample2, T1.Sample2Modify, T1.Sample3, T1.Sample3Modify, T1.Sample4, T1.Sample4Modify, T1.Sample5, T1.Sample5Modify, T1.Sample6, T1.Sample6Modify, T1.Sample7, T1.Sample7Modify, T1.Sample8, T1.Sample8Modify, T2.Sample9, T2.Sample9Modify, T2.Sample10, T2.Sample10Modify, T2.Sample11, T2.Sample11Modify, T2.Sample12, T2.Sample12Modify, T2.Sample13, T2.Sample13Modify, T2.Sample14, T2.Sample14Modify, T2.Sample15, T2.Sample15Modify, T2.Sample16, T2.Sample16Modify, T1.SCAR FROM (SELECT * FROM ".$DBtable4." WHERE ServiceNumber = '"."{$value}"."')AS T1 LEFT JOIN (SELECT * FROM ".$DBtable6." WHERE ServiceNumber = '"."{$value}"."')AS T2 ON T1.DimNO=T2.DimNO AND T1.DimSpec=T2.DimSpec ORDER BY T1.DimNOOrder ASC";
		$listoutD = mysqli_query($connect,$query_listoutD) or die(mysqli_error());
		$query_listoutN="SELECT count(DimNO)As NO FROM ".$DBtable4." WHERE ServiceNumber = '".$SN."'";
		$listoutN = mysqli_query($connect,$query_listoutN) or die(mysqli_error());
		
		$listoutT = mysqli_fetch_assoc($listoutT);
		$listoutN = mysqli_fetch_assoc($listoutN);
		$DimNO = $listoutN['NO'];
		
		$filename=mb_convert_encoding(substr($listoutT['RawFileName'],15,strrpos($listoutT['RawFileName'],'.')-15),"big5","utf8");
		$Year=substr($listoutT['DateTime'],0,4);
		$MonthDay=substr($listoutT['DateTime'],5,2).substr($listoutT['DateTime'],8,2);
		if($listoutT['CavityNumber']>8){
			$cavityNum=16;
			$sheetN=ceil($DimNO/24);
		}elseif($listoutT['CavityNumber']>4){
			$cavityNum=8;
			$sheetN=ceil($DimNO/45);
		}else{
			$cavityNum=4;
			$sheetN=ceil($DimNO/90);
		}
		
		$xls1 = PHPExcel_IOFactory::load('Report/IPQC_Report/From/Conn_IPQC_'.($cavityNum/4).'T.xlsx');
		$xls1->setActiveSheetIndexByName($cavityNum.'M');
		$xls_sheet1 = $xls1->getActiveSheet();
		
		$objWriter1 = PHPExcel_IOFactory::createWriter($xls1,'Excel2007');
		$objWriter1->save('Report/IPQC_Report/From/Temp/'.$filename.'.xlsx');
		
		$xls = PHPExcel_IOFactory::load('Report/IPQC_Report/From/Temp/'.$filename.'.xlsx');
		$xls->setActiveSheetIndexByName($cavityNum.'M');
		$xls->getActiveSheet()->setTitle($cavityNum.'M_1');
		
		if($sheetN>1){
			for($sN=2;$sN<($sheetN+1);$sN++){
				$xls1 = PHPExcel_IOFactory::load('Report/IPQC_Report/From/Conn_IPQC_'.($cavityNum/4).'T.xlsx');
				$sheet = $xls1->getSheetByName($cavityNum.'M');
				$xls->addExternalSheet($sheet);
				$xls->setActiveSheetIndexByName($cavityNum.'M');
				$xls->getActiveSheet()->setTitle($cavityNum.'M_'.$sN);
			}
		}
		
		$xls->setActiveSheetIndexByName($cavityNum.'M_1');
		$xls_sheet = $xls->getActiveSheet();

		switch($cavityNum){
			case"4":
				$xls_sheet->setCellValue('C2',$listoutT['PartNumber']);
				$xls_sheet->setCellValue('H2',$listoutT['PartMold']);
				$xls_sheet->setCellValue('M2',$listoutT['TicketNumber']);
				$xls_sheet->setCellValue('S2',$listoutT['DrawingNumber']);
				$xls_sheet->setCellValue('C3',$listoutT['DateTime']);
				$xls_sheet->setCellValue('H3',$listoutT['FormingMachine']);
				$xls_sheet->setCellValue('M3',$listoutT['Material']);
				
				$N=1;
				$M=1;
				while($listout = mysqli_fetch_assoc($listoutD)){
					if($N<91){
						if($N<46){
							$xls_sheet->setCellValue('B'.($N+5),$listout['DimNO']);
							$xls_sheet->setCellValue('C'.($N+5),$listout['DimSpec']);
							$xls_sheet->setCellValue('D'.($N+5),$listout['DimSpec']+$listout['DimUpper']);
							$xls_sheet->setCellValue('E'.($N+5),$listout['DimSpec']+$listout['DimLow']);
							$xls_sheet->setCellValue('F'.($N+5),checkNA($listout['Sample1']));
							$xls_sheet->setCellValue('G'.($N+5),checkNA($listout['Sample2']));
							$xls_sheet->setCellValue('H'.($N+5),checkNA($listout['Sample3']));
							$xls_sheet->setCellValue('I'.($N+5),checkNA($listout['Sample4']));
							$xls_sheet->setCellValue('J'.($N+5),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'])));
							$xls_sheet->setCellValue('U'.($N+5),$listout['SCAR']);
						}else{
							$xls_sheet->setCellValue('L'.($N-40),$listout['DimNO']);
							$xls_sheet->setCellValue('M'.($N-40),$listout['DimSpec']);
							$xls_sheet->setCellValue('N'.($N-40),$listout['DimSpec']+$listout['DimUpper']);
							$xls_sheet->setCellValue('O'.($N-40),$listout['DimSpec']+$listout['DimLow']);
							$xls_sheet->setCellValue('P'.($N-40),checkNA($listout['Sample1']));
							$xls_sheet->setCellValue('Q'.($N-40),checkNA($listout['Sample2']));
							$xls_sheet->setCellValue('R'.($N-40),checkNA($listout['Sample3']));
							$xls_sheet->setCellValue('S'.($N-40),checkNA($listout['Sample4']));
							$xls_sheet->setCellValue('T'.($N-40),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'])));
							$xls_sheet->setCellValue('V'.($N-40),$listout['SCAR']);
						}
					}else{
						$M=$M+1;
						$xls->setActiveSheetIndexByName($cavityNum.'M_'.$M);
						$xls_sheet = $xls->getActiveSheet();
						
						$xls_sheet->setCellValue('C2',$listoutT['PartNumber']);
						$xls_sheet->setCellValue('H2',$listoutT['PartMold']);
						$xls_sheet->setCellValue('M2',$listoutT['TicketNumber']);
						$xls_sheet->setCellValue('S2',$listoutT['DrawingNumber']);
						$xls_sheet->setCellValue('C3',$listoutT['DateTime']);
						$xls_sheet->setCellValue('H3',$listoutT['FormingMachine']);
						$xls_sheet->setCellValue('M3',$listoutT['Material']);
						
						$N=$N-90;
						$xls_sheet->setCellValue('B'.($N+5),$listout['DimNO']);
						$xls_sheet->setCellValue('C'.($N+5),$listout['DimSpec']);
						$xls_sheet->setCellValue('D'.($N+5),$listout['DimSpec']+$listout['DimUpper']);
						$xls_sheet->setCellValue('E'.($N+5),$listout['DimSpec']+$listout['DimLow']);
						$xls_sheet->setCellValue('F'.($N+5),checkNA($listout['Sample1']));
						$xls_sheet->setCellValue('G'.($N+5),checkNA($listout['Sample2']));
						$xls_sheet->setCellValue('H'.($N+5),checkNA($listout['Sample3']));
						$xls_sheet->setCellValue('I'.($N+5),checkNA($listout['Sample4']));
						$xls_sheet->setCellValue('J'.($N+5),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'])));
						$xls_sheet->setCellValue('U'.($N+5),$listout['SCAR']);
					}
					$N=$N+1;
				}
			break;
			
			case"8":
				$xls_sheet->setCellValue('C2',$listoutT['PartNumber']);
				$xls_sheet->setCellValue('G2',$listoutT['PartMold']);
				$xls_sheet->setCellValue('J2',$listoutT['TicketNumber']);
				$xls_sheet->setCellValue('N2',$listoutT['DrawingNumber']);
				$xls_sheet->setCellValue('C3',$listoutT['DateTime']);
				$xls_sheet->setCellValue('G3',$listoutT['FormingMachine']);
				$xls_sheet->setCellValue('J3',$listoutT['Material']);
				
				$N=1;
				$M=1;
				while($listout = mysqli_fetch_assoc($listoutD)){
					if($N<46){
						$xls_sheet->setCellValue('B'.($N+5),$listout['DimNO']);
						$xls_sheet->setCellValue('C'.($N+5),$listout['DimSpec']);
						$xls_sheet->setCellValue('D'.($N+5),$listout['DimSpec']+$listout['DimUpper']);
						$xls_sheet->setCellValue('E'.($N+5),$listout['DimSpec']+$listout['DimLow']);
						$xls_sheet->setCellValue('F'.($N+5),checkNA($listout['Sample1']));
						$xls_sheet->setCellValue('G'.($N+5),checkNA($listout['Sample2']));
						$xls_sheet->setCellValue('H'.($N+5),checkNA($listout['Sample3']));
						$xls_sheet->setCellValue('I'.($N+5),checkNA($listout['Sample4']));
						$xls_sheet->setCellValue('J'.($N+5),checkNA($listout['Sample5']));
						$xls_sheet->setCellValue('K'.($N+5),checkNA($listout['Sample6']));
						$xls_sheet->setCellValue('L'.($N+5),checkNA($listout['Sample7']));
						$xls_sheet->setCellValue('M'.($N+5),checkNA($listout['Sample8']));
						$xls_sheet->setCellValue('N'.($N+5),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'],$listout['Sample5'],$listout['Sample6'],$listout['Sample7'],$listout['Sample8'])));
						$xls_sheet->setCellValue('O'.($N+5),$listout['SCAR']);
					}else{
						$M=$M+1;
						$xls->setActiveSheetIndexByName($cavityNum.'M_'.$M);
						$xls_sheet = $xls->getActiveSheet();
						
						$xls_sheet->setCellValue('C2',$listoutT['PartNumber']);
						$xls_sheet->setCellValue('G2',$listoutT['PartMold']);
						$xls_sheet->setCellValue('J2',$listoutT['TicketNumber']);
						$xls_sheet->setCellValue('N2',$listoutT['DrawingNumber']);
						$xls_sheet->setCellValue('C3',$listoutT['DateTime']);
						$xls_sheet->setCellValue('G3',$listoutT['FormingMachine']);
						$xls_sheet->setCellValue('J3',$listoutT['Material']);
									
						$N=$N-45;
						$xls_sheet->setCellValue('B'.($N+5),$listout['DimNO']);
						$xls_sheet->setCellValue('C'.($N+5),$listout['DimSpec']);
						$xls_sheet->setCellValue('D'.($N+5),$listout['DimSpec']+$listout['DimUpper']);
						$xls_sheet->setCellValue('E'.($N+5),$listout['DimSpec']+$listout['DimLow']);
						$xls_sheet->setCellValue('F'.($N+5),checkNA($listout['Sample1']));
						$xls_sheet->setCellValue('G'.($N+5),checkNA($listout['Sample2']));
						$xls_sheet->setCellValue('H'.($N+5),checkNA($listout['Sample3']));
						$xls_sheet->setCellValue('I'.($N+5),checkNA($listout['Sample4']));
						$xls_sheet->setCellValue('J'.($N+5),checkNA($listout['Sample5']));
						$xls_sheet->setCellValue('K'.($N+5),checkNA($listout['Sample6']));
						$xls_sheet->setCellValue('L'.($N+5),checkNA($listout['Sample7']));
						$xls_sheet->setCellValue('M'.($N+5),checkNA($listout['Sample8']));
						$xls_sheet->setCellValue('N'.($N+5),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'],$listout['Sample5'],$listout['Sample6'],$listout['Sample7'],$listout['Sample8'])));
						$xls_sheet->setCellValue('O'.($N+5),$listout['SCAR']);
					}
					$N=$N+1;
				}
			break;
			
			case"16":
				$xls_sheet->setCellValue('C2',$listoutT['PartNumber']);
				$xls_sheet->setCellValue('G2',$listoutT['PartMold']);
				$xls_sheet->setCellValue('J2',$listoutT['TicketNumber']);
				$xls_sheet->setCellValue('M2',$listoutT['DrawingNumber']);
				$xls_sheet->setCellValue('C3',$listoutT['DateTime']);
				$xls_sheet->setCellValue('G3',$listoutT['FormingMachine']);
				$xls_sheet->setCellValue('J3',$listoutT['Material']);
				
				$N=1;
				$M=1;
				while($listout = mysqli_fetch_assoc($listoutD)){
					if($N<49){
						$xls_sheet->setCellValue('B'.($N+6),$listout['DimNO']);
						$xls_sheet->setCellValue('C'.($N+6),$listout['DimSpec']);
						$xls_sheet->setCellValue('D'.($N+6),$listout['DimSpec']+$listout['DimUpper']);
						$xls_sheet->setCellValue('E'.($N+6),$listout['DimSpec']+$listout['DimLow']);
						$xls_sheet->setCellValue('F'.($N+6),checkNA($listout['Sample1']));
						$xls_sheet->setCellValue('G'.($N+6),checkNA($listout['Sample2']));
						$xls_sheet->setCellValue('H'.($N+6),checkNA($listout['Sample3']));
						$xls_sheet->setCellValue('I'.($N+6),checkNA($listout['Sample4']));
						$xls_sheet->setCellValue('J'.($N+6),checkNA($listout['Sample5']));
						$xls_sheet->setCellValue('K'.($N+6),checkNA($listout['Sample6']));
						$xls_sheet->setCellValue('L'.($N+6),checkNA($listout['Sample7']));
						$xls_sheet->setCellValue('M'.($N+6),checkNA($listout['Sample8']));
						$xls_sheet->setCellValue('F'.($N+7),checkNA($listout['Sample9']));
						$xls_sheet->setCellValue('G'.($N+7),checkNA($listout['Sample10']));
						$xls_sheet->setCellValue('H'.($N+7),checkNA($listout['Sample11']));
						$xls_sheet->setCellValue('I'.($N+7),checkNA($listout['Sample12']));
						$xls_sheet->setCellValue('J'.($N+7),checkNA($listout['Sample13']));
						$xls_sheet->setCellValue('K'.($N+7),checkNA($listout['Sample14']));
						$xls_sheet->setCellValue('L'.($N+7),checkNA($listout['Sample15']));
						$xls_sheet->setCellValue('M'.($N+7),checkNA($listout['Sample16']));
						$xls_sheet->setCellValue('N'.($N+6),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'],$listout['Sample5'],$listout['Sample6'],$listout['Sample7'],$listout['Sample8'],$listout['Sample9'],$listout['Sample10'],$listout['Sample11'],$listout['Sample12'],$listout['Sample13'],$listout['Sample14'],$listout['Sample15'],$listout['Sample16'])));
						$xls_sheet->setCellValue('O'.($N+6),$listout['SCAR']);
					}else{
						$M=$M+1;
						$xls->setActiveSheetIndexByName($cavityNum.'M_'.$M);
						$xls_sheet = $xls->getActiveSheet();
						
						$xls_sheet->setCellValue('C2',$listoutT['PartNumber']);
						$xls_sheet->setCellValue('G2',$listoutT['PartMold']);
						$xls_sheet->setCellValue('J2',$listoutT['TicketNumber']);
						$xls_sheet->setCellValue('M2',$listoutT['DrawingNumber']);
						$xls_sheet->setCellValue('C3',$listoutT['DateTime']);
						$xls_sheet->setCellValue('G3',$listoutT['FormingMachine']);
						$xls_sheet->setCellValue('J3',$listoutT['Material']);
									
						$N=$N-48;
						$xls_sheet->setCellValue('B'.($N+6),$listout['DimNO']);
						$xls_sheet->setCellValue('C'.($N+6),$listout['DimSpec']);
						$xls_sheet->setCellValue('D'.($N+6),$listout['DimSpec']+$listout['DimUpper']);
						$xls_sheet->setCellValue('E'.($N+6),$listout['DimSpec']+$listout['DimLow']);
						$xls_sheet->setCellValue('F'.($N+6),checkNA($listout['Sample1']));
						$xls_sheet->setCellValue('G'.($N+6),checkNA($listout['Sample2']));
						$xls_sheet->setCellValue('H'.($N+6),checkNA($listout['Sample3']));
						$xls_sheet->setCellValue('I'.($N+6),checkNA($listout['Sample4']));
						$xls_sheet->setCellValue('J'.($N+6),checkNA($listout['Sample5']));
						$xls_sheet->setCellValue('K'.($N+6),checkNA($listout['Sample6']));
						$xls_sheet->setCellValue('L'.($N+6),checkNA($listout['Sample7']));
						$xls_sheet->setCellValue('M'.($N+6),checkNA($listout['Sample8']));
						$xls_sheet->setCellValue('F'.($N+7),checkNA($listout['Sample9']));
						$xls_sheet->setCellValue('G'.($N+7),checkNA($listout['Sample10']));
						$xls_sheet->setCellValue('H'.($N+7),checkNA($listout['Sample11']));
						$xls_sheet->setCellValue('I'.($N+7),checkNA($listout['Sample12']));
						$xls_sheet->setCellValue('J'.($N+7),checkNA($listout['Sample13']));
						$xls_sheet->setCellValue('K'.($N+7),checkNA($listout['Sample14']));
						$xls_sheet->setCellValue('L'.($N+7),checkNA($listout['Sample15']));
						$xls_sheet->setCellValue('M'.($N+7),checkNA($listout['Sample16']));
						$xls_sheet->setCellValue('N'.($N+6),between($listout['DimSpec']+$listout['DimLow']-0.00001,$listout['DimSpec']+$listout['DimUpper']+0.00001,array($listout['Sample1'],$listout['Sample2'],$listout['Sample3'],$listout['Sample4'],$listout['Sample5'],$listout['Sample6'],$listout['Sample7'],$listout['Sample8'],$listout['Sample9'],$listout['Sample10'],$listout['Sample11'],$listout['Sample12'],$listout['Sample13'],$listout['Sample14'],$listout['Sample15'],$listout['Sample16'])));
						$xls_sheet->setCellValue('O'.($N+6),$listout['SCAR']);
					}
					$N=$N+2;
				}
			break;
		}
		
		///建立資料夾///
		$path0='Report/IPQC_Report/Report/'.$Year; 
		if(file_exists($path0)){}else{mkdir($path0,0777,true);}
		$path=$path0.'/'.$MonthDay.'/';
		if(file_exists($path)){}else{mkdir($path,0777,true);}
		
		$objWriter = PHPExcel_IOFactory::createWriter($xls,'Excel2007');
		$objWriter->save($path.$filename.'.xlsx');
		
		unlink('Report/IPQC_Report/From/Temp/'.$filename.'.xlsx');
	}
}

function checkNA($value){
	if($value=="-999.0000"){
		return "N/A";
	}else{
		return $value;
	}
}

function between($min, $max, $value){
	if(array_search("-999.0000",$value)!=0){
		$value=array_splice($value,0,array_search("-999.0000",$value));
	}
	//處理成陣列
	if (is_array($value)){
		$limit = $value;
	}else{
		$limit = explode(",", $value);
	}
	//合併成多個數值
	$value = array_merge($limit, $limit);
	$limit[] = $max;
	$limit[] = $min;

	 //使用max及min函數判斷是否在區間內
	if ((max($limit) == $max && min($limit) == $min) || (max($value) == $max && min($value) == $min)){
		$result = 'OK';
	}else{
		$result = 'NG';
	}
	return $result;
}
		
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CreateReportByDay-2</title>

</head>
</html>
