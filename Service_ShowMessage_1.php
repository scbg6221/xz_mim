<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$SN=$_REQUEST['SN'];

mysqli_select_db($connect,$database);
$sql="SELECT * FROM `servicerequest` WHERE `ServiceNumber`='$SN'";
$query= mysqli_query($connect,$sql) ;
$AA = mysqli_fetch_array($query);
	
///抓取各欄位數值///	
	$I1=$AA['ServiceNumber'];
	$I2=$AA['MachineNumber'];
	$I3=$AA['Priority'];
	$I4=$AA['Site'];
	$I5=$AA['RequestDepartment'];
	$I6=$AA['RequestDatetime'];
	$I7=$AA['RequestName'];
	$I8=$AA['RequestPhone'];
	$I9=$AA['RequestEmail'];
	$I10=$AA['RequestEmailCC'];
	$I11=$AA['ProductFoxlinkPN'];
	$I12=$AA['ProductName'];
	$I20=$AA['ProductNeedDate'];
	$I13=$AA['ProductFrom'];
	$I15=$AA['ProductPcsQuantity'];
	$I14=$AA['ProductOneQuantity'];
	$I16=$AA['ProductFirstMeasurement'];
	$I17=$AA['ProductDrawingPN'];
	$I19=$AA['ProductObject'];
	$I21=$AA['MeasurementObject'];
	$I22=$AA['Remark'];


?>

<!doctype html>

<html>
<head>
<meta charset="utf-8">
<title>Service_ShowMessage</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
//	font-weight: bold;
	color: #005DBE;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
//	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color:#000000;
	font-size:20px;
	}
priorityR {
	color:#FF8080;
}
priorityG {
	color:#80FF80;
}
.BT{
	font:bold;
	font:,"Arial Black", Gadget, sans-serif;
	border:#009;
	border:1px;
	border-radius:3px;
	height:80px;
	width:100px;
	background-color:#afeeee;
	color:#000000;
	font-size:16px;
	border-style:solid;
	cursor:pointer;
	}
BR{height:80px;}
</style>

</head>

<script type="text/javascript">
function result() {
    document['form1'].action = "Service_Application.php";
    document['form1'].target = 'Index_Content';
}
</script>

<body>
<form id="form1" name="form1" method="post" >
<table border="1">
	<tr>
    	<td height="50" colspan="4" align="center"><T>申  請  量  測  委  託  單</T></td>
    	<td height="50" colspan="2" align="center"><M>【請將產品送至量測室】</M></td>
    </tr>
	<tr>
    	<td width="150" height="60" align="center"><M>申請單流水號</M></td>
        <td width="150" height="60" colspan="2" align="center"><Z><?php echo $I1 ?></Z></td>
        <td width="150" height="60" align="center"><M>委託等級</M></td>
        <td width="150" height="60" colspan="2" align="center"><Z><?php if($I3=="ExtruUrgent"){echo "<priorityR>特急</priorityR>";}elseif($I3=="Urgent"){echo "<priorityG>加急</priorityG>";}else{echo "普通";}?></Z></td>
	</tr>
	<tr>
		<td height="30" colspan="6" align="center"><L>委託單位資訊</L></td>
	</tr>
	<tr>
        <td width="150" height="60" align="center"><M>廠區</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I4 ?></Z></td>
        <td width="150" height="60" align="center"><M>委託部門</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I5 ?></Z></td>
        <td width="150" height="60" align="center"><M>委託日期/時間</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I6 ?></Z></td>
	</tr>
	<tr>
        <td width="150" height="60" align="center"><M>姓名</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I7 ?></Z></td>
        <td width="150" height="60" align="center"><M>分機/手機</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I8 ?></Z></td>
        <td width="150" height="60" align="center"><M>電子信箱</M></td>
        <td width="150" height="60" align="center"><Z>案件負責人<br><?php echo $I9 ?><br>副本收件人<br><?php echo $I10 ?></Z></td>
	</tr>
	<tr>
    	<td height="30" colspan="6" align="center"><L>委託量測產品資訊</L></td>
	</tr>
	<tr>
        <td width="150" height="60" align="center"><M>正崴料號</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I11 ?></Z></td>
        <td width="150" height="60" align="center"><M>品名</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I12 ?></Z></td>
        <td width="150" height="60" align="center"><M>量測需求時間</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I20 ?></Z></td>
	</tr>
	<tr>
        <td width="150" height="60" align="center"><M>量測種類表單</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I13 ?></Z></td>
        <td width="150" height="60" align="center"><M>量測送樣數量<BR>( 單位 : pcs )</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I15 ?></Z></td>
        <td width="150" height="60" align="center"><M>量測尺寸數量<BR>( 1 pcs )</M></td>
        <td width="150" height="60" align="center"><Z><?php echo $I14 ?></Z></td>
	</tr>
	<tr>
        <td width="150" height="60" align="center"><M>量測狀況</M></td>
        <td width="150" height="60" colspan="2" align="center"><Z><?php if($I16 == 1){echo "第一次送測 (含產品設變)";}else{echo "已錄製過程式";} ?></Z></td>
        <td width="150" height="60" align="center"><M>量測圖紙</M></td>
        <td width="150" height="60" colspan="2" align="center"> 
        <Z><?php
			if ($I17){
				echo $I17;
			}else{
				echo "無上傳圖紙";
			}
			?></td>
        
        
	</tr>
	<tr>
       <td width="150" height="60" align="center"><M>委託目的</M></td>
       <td width="150" height="60" colspan="2" align="center"><Z><?php echo $I19 ?></Z></td>
       <td width="150" height="60" align="center"><M>預定量測機台</M></td>
       <td width="150" height="60" colspan="2" align="center"><Z><?php echo "尚未指定機台" ?></Z></td>
	</tr>
	<tr>
        <td width="150" height="120" align="center"><M>測量內容說明</M></td>
        <td height="120" colspan="5" align="left"><Z><?php echo $I21 ?></Z></td>
    </tr>
	<tr>
    	<td height="30" colspan="6" align="center"><L>備註</L></td>
	</tr>
  <tr>
	<td height="120" colspan="5" align="left"><Z><?php echo $I22 ?></Z></td>
		<td height="120" align="center"><input type="submit" name="aga" id="aga" value="再次申請" class="BT" onClick="result()"></td>
  </tr>
</table>
</form>
</body>
</html>