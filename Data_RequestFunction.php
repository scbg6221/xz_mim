<?php 
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$Type=$_GET['Type'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}

date_default_timezone_set('Asia/Taipei');

switch ($Type){
	
/////////////////////////////////////////////////////////////////////////////////////////////////////Data_RequestList-1.php

	case "listadd" :
		header("Content-Type:text/html; charset=utf-8");
		$Req=$_GET['Req'];
		$Req=explode('::',$Req);
		mysqli_select_db($connect,$database);
		
		$Cdata="SELECT * FROM ".$DBtable1." WHERE PartNumber = '".$Req[0]."' AND PartMold = '".$Req[1]."'";
		$Cdata_query=mysqli_query($connect,$Cdata);
		$Cdata_I=mysqli_fetch_array($Cdata_query);
		
		$DateTime="B".date("Ymd")."D".date("His")."L";
		
		if(!$Cdata_I['PartNumber']){
			$sql= "INSERT INTO ".$DBtable1." (PartNumber, PartMold, Material, DrawingNumber, CavityNumber, Barcode) VALUES ( '".$Req[0]."' , '".$Req[1]."' , '".$Req[2]."' , '".$Req[3]."' , '".$Req[4]."' , '".$DateTime."' )";
			$query= mysqli_query($connect,$sql) or die("新增失敗，Fail:A");
			echo "<script>
				alert('產品料號已建立成功 。');
				parent.Index_Content.location.href='Data_RequestList-1.php?floor=".$floor."';
			</script>";
		}else{
			echo "<script>
				alert('警告：\\n\\n   【產品料號】\\n\\n      已經存在 ， 請重新填寫 。');
				parent.Index_Content.location.href='Data_RequestList-1.php?floor=".$floor."';
			</script>";
		};
	break;
	case "listchange" :
		header("Content-Type:text/html; charset=utf-8");
		$Req=$_GET['Req'];
		$Req=explode('::',$Req);
		mysqli_select_db($connect,$database);
		
		$sql= "UPDATE ".$DBtable1." SET Material = '".$Req[2]."', DrawingNumber = '".$Req[3]."', CavityNumber = '".$Req[4]."' WHERE PartNumber = '".$Req[0]."' AND PartMold = '".$Req[1]."' AND Barcode = '".$Req[5]."'";
		$query= mysqli_query($connect,$sql) or die("修改失敗，Fail:A");
		echo "<script>
			alert('產品料號已修改成功 。');
			parent.Index_Content.location.href='Data_RequestList-1.php?floor=".$floor."';
		</script>";
	break;
	case "listdelete" :
		header("Content-Type:text/html; charset=utf-8");
		$Req=$_GET['Req'];
		$Req=explode('::',$Req);
		mysqli_select_db($connect,$database);
		
		$sql= "DELETE FROM ".$DBtable1." WHERE  Barcode = '".$Req[0]."'";
		$query= mysqli_query($connect,$sql) or die("刪除失敗，Fail:A");
		$DdataFile = 'Report/IPQC_Barcode/'.$Req[0].'.jpeg';
		unlink($DdataFile);	
		echo "<script>
			alert('產品料號已刪除成功 。');
			parent.Index_Content.location.href='Data_RequestList-1.php?floor=".$floor."';
		</script>";
	break;
	
/////////////////////////////////////////////////////////////////////////////////////////////////////Data_RequestList-3.php

	case "listchangemachine" :
		header("Content-Type:text/html; charset=utf-8");
		$Req=$_GET['Req'];
		$Req=explode('::',$Req);
		mysqli_select_db($connect,$database);
		
		$sql= "UPDATE ".$DBtable1." SET MeasureMachine = '".$Req[0]."' WHERE Barcode = '".$Req[1]."'";
		$query= mysqli_query($connect,$sql) or die("修改失敗，Fail:A");
		echo "<script>
			alert('量測機台已修改成功 。');
			parent.Index_Content.location.href='Data_RequestList-3.php?floor=".$floor."';
		</script>";
	break;

/////////////////////////////////////////////////////////////////////////////////////////////////////Data_Request-1.php

	case "person" :
		$q=$_GET['Sql'];
		$Report_search_array=array();
		mysqli_select_db($connectm,$databasem);
		$Search = "$q";
		$Search_Report = mysqli_query($connectm,$Search) or die(mysqli_error());
		
		if($Search_Report){
			while($row=mysqli_fetch_array($Search_Report)){
				array_push($Report_search_array,$row['name']);
			}
			echo json_encode($Report_search_array); 
		}
	break;	
	case "product" :
		$q=$_GET['Sql'];
		$Report_search_array=array();
		mysqli_select_db($connect,$database);
		$Search = "$q";
		$Search_Report = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_Report){
			while($row=mysqli_fetch_array($Search_Report)){
				if($row['MeasureMachine']==0){
					$DateTime = date("Y-m-d");
					$Machine = "SELECT `id` FROM `machine_list` LEFT JOIN (SELECT `MeasureMachine`, count(*) AS Num FROM `request_measure` WHERE DateTime = '".$DateTime."' and `Status` != '9' GROUP BY `MeasureMachine`)AS T2 ON `machine_list`.`id`=T2.`MeasureMachine` ORDER BY Num ASC LIMIT 1";
					$Machine_Report = mysqli_query($connect,$Machine) or die(mysqli_error());
					$machNum=mysqli_fetch_array($Machine_Report);
					$row['MeasureMachine']=$machNum['id'];
				}
				array_push($Report_search_array,$row['PartNumber']."::".$row['PartMold']."::".$row['Material']."::".$row['DrawingNumber']."::".$row['CavityNumber']."::".$row['MeasureMachine']);
			}
			echo json_encode($Report_search_array); 
		}
	break;
	case "ticket" :
		require_once('../../Public/Connections/projector_system_Oracle.php');
		$q=$_GET['Sql'];
		$Report_search_array=array();
		mysqli_select_db($connect_Oracle,$database_Oracle);
		$Search = "$q";
		$Search_Report = mysqli_query($connect_Oracle,$Search) or die(mysqli_error());
		
		if($Search_Report){
			while($row=mysqli_fetch_array($Search_Report)){
				array_push($Report_search_array,$row['Prod_Line_Code']."::".$row['Primary_Item_NO']."::".$row['Mold_Num']."::".$row['Eng_Revision']);
			}
			echo json_encode($Report_search_array); 
		}
	break;
	case "request" :
		$q1=$_GET['Sql'];
		$q2=$_GET['Sqll'];
		$TT=$_GET['TT'];
		$Report_search_array=array();
		mysqli_select_db($connect,$database);
		$Insert = "$q1";
		$Search = "$q2";
		
		$Search_Part = mysqli_query($connect,$Search) or die(mysqli_error());
		$Part_Number = mysqli_fetch_array($Search_Part);
		
		if (!$Part_Number['MAX(ServiceNumber)']){
			$query= mysqli_query($connect,$Insert) or die(mysqli_error());
			array_push($Report_search_array,"OK");
		}else{
			if ($TT-$Part_Number['MAX(ServiceNumber)']>1000){
				$query= mysqli_query($connect,$Insert) or die(mysqli_error());
				array_push($Report_search_array,"OK");
			}else{
				array_push($Report_search_array,"repeat");
			}
		}
		echo json_encode($Report_search_array); 
	break;

/////////////////////////////////////////////////////////////////////////////////////////////////////Data_RequestChangeMachine-1.php

	case "changemachine" :
		$q=$_GET['Sql'];
		$Report_search_array=array();
		mysqli_select_db($connect,$database);
		$Update = "$q";
		
		if($Update_Part = mysqli_query($connect,$Update)){
			array_push($Report_search_array,"OK");
		}else{
			array_push($Report_search_array,"NG");
		}
		echo json_encode($Report_search_array); 
	break;

/////////////////////////////////////////////////////////////////////////////////////////////////////Data_RequestMeasure-1.php

	case "measurestart" :
		header("Content-Type:text/html; charset=utf-8");
		$SN=$_GET['SN'];
		mysqli_select_db($connect,$database);
	
		$Starttime="Select ServiceNumber, MeasureStartTime FROM ".$DBtable2." WHERE Status = '2' AND MeasureMachine IN (Select MeasureMachine FROM ".$DBtable2." WHERE ServiceNumber = '".$SN."')";
		$Starttime_Query=mysqli_query($connect,$Starttime);
		$Starttime_I=mysqli_fetch_array($Starttime_Query);

		if(!$Starttime_I['ServiceNumber']){
			$DateTime=date("Y-m-d H:i:s");
			$sql= "UPDATE ".$DBtable2." SET Status = '2', MeasureStartTime = '".$DateTime."' WHERE ServiceNumber = '".$SN."' AND Status = '1' ";
			$query= mysqli_query($connect,$sql) or die("產品量測開始失敗，Fail:A");
			echo "<script>
				alert('開始進行量測產品 。');
				parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
			</script>";
		}else{
			$DateTime=$Starttime_I['MeasureStartTime'];
			$sql= "UPDATE ".$DBtable2." SET Status = '1', MeasureStartTime = '0000-00-00 00:00:00' WHERE ServiceNumber = '".$Starttime_I['ServiceNumber']."' AND Status = '2' ";
			$query= mysqli_query($connect,$sql) or die("產品量測開始失敗，Fail:B1");
			$sql= "UPDATE ".$DBtable2." SET Status = '2', MeasureStartTime = '".$DateTime."' WHERE ServiceNumber = '".$SN."' AND Status = '1' ";
			$query= mysqli_query($connect,$sql) or die("產品量測開始失敗，Fail:B2");
			echo "<script>
				alert('開始進行量測產品 。');
				parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
			</script>";
		};
	break;
	case "measureover1" :	
		require_once('../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php');
		require_once('../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php');
		require_once('../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php');
		require_once('Data_RequestMeasure-3.php');

		$ChineseName=$_SESSION['ChineseName'];
		$ID=$_SESSION['ID'];
		if ($_FILES["upload_file1"]){
			header("Content-Type:text/html; charset=utf-8");
			$SN=$_GET['SN'];
			mysqli_select_db($connect,$database);
			
			$Search = "Select * FROM ".$DBtable2." WHERE Status = '2' AND ServiceNumber = '".$SN."'";
			$Search_Part = mysqli_query($connect,$Search) or die(mysqli_error());
			$Part_Number = mysqli_fetch_array($Search_Part);
			
			if($Part_Number){
				$Year=substr($Part_Number['DateTime'],0,4);
				$MonthDay=substr($Part_Number['DateTime'],5,2).substr($Part_Number['DateTime'],8,2);				
				$uploaddir0='Report/IPQC_RawData/'.$Year; 
				if(file_exists($uploaddir0)){}else{mkdir($uploaddir0,0777,true);}
				$uploaddir=$uploaddir0.'/'.$MonthDay.'/';
				if(file_exists($uploaddir)){}else{mkdir($uploaddir,0777,true);}
				$_SESSION['upload_file1_name']=$_FILES["upload_file1"]["name"]; 
				$_SESSION['Error1']=$_FILES['upload_file1']['error'];
				$_SESSION['tmpfile1']=$_FILES["upload_file1"]["tmp_name"];
				$_SESSION['file1']=mb_convert_encoding($_FILES["upload_file1"]["name"],"big5","utf8"); 
				$reportfilename1=$SN."_".$_SESSION['file1'];
				if(move_uploaded_file($_SESSION['tmpfile1'],$uploaddir.$reportfilename1)){
					$fileExt1=substr($reportfilename1, strrpos($reportfilename1, '.') + 1);
					if($fileExt1 == "xls" or $fileExt1 == "xlsx") {
						$Fork = new Fork;
						$phpexcel = new PHPExcel;
						$phpexcel = PHPExcel_IOFactory::load($uploaddir.$reportfilename1);
						$SheetsC = $phpexcel->getSheetCount();
						$xlsx = $phpexcel->getActiveSheet();
						//$PNC  = $xlsx->getCell('D5')->getValue();
						//$PDC  = $xlsx->getCell('D6')->getValue();
						//$LNC  = $xlsx->getCell('D7')->getValue();
						//$PhNC  = $xlsx->getCell('L6')->getValue();
						
						if ($SheetsC == 1){
							//if($Part_Number['PartNumber']==$PNC){
								//if($Part_Number['PartMold']==$PDC){
									//if($Part_Number['FormingMachine']==$LNC){
										//if($Part_Number['TicketNumber']==$PhNC){
											
											$DataN1=$SN;
											$DataN2='0';
											$DataN3=$Part_Number['TicketNumber'];
											$DataN4=$Part_Number['PartNumber'];
											$DataN5=$Part_Number['PartMold'];
											$DataN6=$Part_Number['Material'];
											$DataN7=$Part_Number['DrawingNumber'];
											$DataN8=$Part_Number['CavityNumber'];
											$DataN9=$Part_Number['FormingMachine'];
											$DataN10=$Part_Number['MeasureMachine'];
											$DataN11=$ChineseName."-".$ID;
											$DataN12=$Part_Number['IPQC_Inspector'];
											$DataN13=mb_convert_encoding($reportfilename1,"utf8","big5");
											$DataN14=$Part_Number['DateTime'];
											$DataN15=$Part_Number['MeasureStartTime'];
											$DataN16=date("Y-m-d H:i:s");
											
											$Insert1 = "INSERT INTO ".$DBtable3."(ServiceNumber, Status, TicketNumber, PartNumber, PartMold, Material, DrawingNumber, CavityNumber, FormingMachine, MeasureMachine, GV_Inspector, IPQC_Inspector, RawFileName, DateTime, MeasureStartTime, MeasureEndTime)VALUES('$DataN1', '$DataN2', '$DataN3', '$DataN4', '$DataN5', '$DataN6', '$DataN7', '$DataN8', '$DataN9', '$DataN10', '$DataN11', '$DataN12', '$DataN13', '$DataN14', '$DataN15', '$DataN16')";
											$query = mysqli_query($connect,$Insert1) or die("警告 ： 數據上傳資料庫失敗，Fail:A");
											
											if($DataN8 < 9){
												$readData1="INSERT INTO ".$DBtable4." (ServiceNumber, DimNO, DimNOOrder, DimSpec, DimUpper, DimLow, Sample1, Sample2, Sample3, Sample4, Sample5, Sample6, Sample7, Sample8 ) VALUES ";
												for ($j = 3; $j <= $xlsx->getHighestRow(); $j++){
													if ($xlsx->getCellByColumnAndRow(0, $j)->getCalculatedValue()){
														$readData1=$readData1."('".$DataN1."'";
														for ($i = 1; $i <= 13; $i++){
															switch($i){
																case '1':
																	if (is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue())){
																		$k=$j;
																		$n=0;
																		do{
																			$k=$k-1;
																			$n=$n+1;
																		}while(is_null($xlsx->getCellByColumnAndRow($i, $k)->getCalculatedValue()));
																		if($k>2){
																			$Nnn=$xlsx->getCellByColumnAndRow($i, $k)->getCalculatedValue()."_".$n;
																		}else{
																			$Nnn="Unnamed_".$n;
																		}
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData1=$readData1.",'".$Nnn."','".$Fork -> DimNO_OrderName($Nnn)."'";
																break;
																case '2':case '3':case '4':
																break;
																case '5':
																	if ( is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue()) && is_null($xlsx->getCellByColumnAndRow($i-1, $j)->getCalculatedValue()) ){
																		$Nnn="0','".$xlsx->getCellByColumnAndRow($i-2, $j)->getCalculatedValue()."','0";
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i-2, $j)->getCalculatedValue()."','".$xlsx->getCellByColumnAndRow($i-1, $j)->getCalculatedValue()."','".$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData1=$readData1.",'".$Nnn."'";
																break;
																case '6':case '7':case '8':case '9':case '10':case '11':case '12':case '13':
																	if ( is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue())){
																		$Nnn="-999";
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData1=$readData1.",'".$Nnn."'";
																break;
															}
														}
														$readData1=$readData1."),";
													}
												}
												$readData1=str_split($readData1,strlen($readData1)-1);
												$Insert2 = $readData1[0];
												$query2 = mysqli_query($connect,$Insert2) or die("警告 ： 數據上傳資料庫失敗，Fail:B1");
												$Update = "UPDATE ".$DBtable2." SET Status = '9', MeasureEndTime = '".$DataN16."' WHERE ServiceNumber = '".$DataN1."' AND Status = '2' ";
												$query3 = mysqli_query($connect,$Update) or die("警告 ： 數據上傳資料庫失敗，Fail:C");
											}else{
												$readData1="INSERT INTO ".$DBtable4." (ServiceNumber, DimNO, DimNOOrder, DimSpec, DimUpper, DimLow, Sample1, Sample2, Sample3, Sample4, Sample5, Sample6, Sample7, Sample8 ) VALUES ";
												$readData2="INSERT INTO ".$DBtable6." (ServiceNumber, DimNO, DimNOOrder, DimSpec, DimUpper, DimLow, Sample9, Sample10, Sample11, Sample12, Sample13, Sample14, Sample15, Sample16 ) VALUES ";
												for ($j = 3; $j <= $xlsx->getHighestRow(); $j++){
													if ($xlsx->getCellByColumnAndRow(0, $j)->getCalculatedValue()){
														$readData1=$readData1."('".$DataN1."'";
														$readData2=$readData2."('".$DataN1."'";
														for ($i = 1; $i <= 21; $i++){
															switch($i){
																case '1':
																	if (is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue())){
																		$k=$j;
																		$n=0;
																		do{
																			$k=$k-1;
																			$n=$n+1;
																		}while(is_null($xlsx->getCellByColumnAndRow($i, $k)->getCalculatedValue()));
																		if($k>2){
																			$Nnn=$xlsx->getCellByColumnAndRow($i, $k)->getCalculatedValue()."_".$n;
																		}else{
																			$Nnn="Unnamed_".$n;
																		}
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData1=$readData1.",'".$Nnn."','".$Fork -> DimNO_OrderName($Nnn)."'";
																	$readData2=$readData2.",'".$Nnn."','".$Fork -> DimNO_OrderName($Nnn)."'";
																break;
																case '2':case '3':case '4':
																break;
																case '5':
																	if ( is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue()) && is_null($xlsx->getCellByColumnAndRow($i-1, $j)->getCalculatedValue()) ){
																		$Nnn="0','".$xlsx->getCellByColumnAndRow($i-2, $j)->getCalculatedValue()."','0";
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i-2, $j)->getCalculatedValue()."','".$xlsx->getCellByColumnAndRow($i-1, $j)->getCalculatedValue()."','".$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData1=$readData1.",'".$Nnn."'";
																	$readData2=$readData2.",'".$Nnn."'";
																break;
																case '6':case '7':case '8':case '9':case '10':case '11':case '12':case '13':
																	if ( is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue())){
																		$Nnn="-999";
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData1=$readData1.",'".$Nnn."'";
																break;
																case '14':case '15':case '16':case '17':case '18':case '19':case '20':case '21':
																	if ( is_null($xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue())){
																		$Nnn="-999";
																	}else{
																		$Nnn=$xlsx->getCellByColumnAndRow($i, $j)->getCalculatedValue();
																	}
																	$readData2=$readData2.",'".$Nnn."'";
																break;
															}
														}
														$readData1=$readData1."),";
														$readData2=$readData2."),";
													}
												}
												$readData1=str_split($readData1,strlen($readData1)-1);
												$readData2=str_split($readData2,strlen($readData2)-1);
												$Insert2 = $readData1[0];
												$Insert3 = $readData2[0];
												$query2 = mysqli_query($connect,$Insert2) or die("警告 ： 數據上傳資料庫失敗，Fail:B1");
												$query3 = mysqli_query($connect,$Insert3) or die("警告 ： 數據上傳資料庫失敗，Fail:B2");
												$Update = "UPDATE ".$DBtable2." SET Status = '9', MeasureEndTime = '".$DataN16."' WHERE ServiceNumber = '".$DataN1."' AND Status = '2' ";
												$query4 = mysqli_query($connect,$Update) or die("警告 ： 數據上傳資料庫失敗，Fail:C");
											}
											$sql= "UPDATE ".$DBtable3." SET Status = '9' , EndDate = '".$DataN16."' WHERE ServiceNumber = '".$DataN1."' ";
											$query= mysqli_query($connect,$sql) ;
											echo "<script>
												parent.Index_Content.location.href='Data_CreateReport-1.php?floor=".$floor."&SN=".$DataN1."';
											</script>";
											
/*											echo "<script>
												alert('提示：\\n\\n  量測報告 【 ".substr($DataN12,18)." 】 \\n\\n上傳成功!!');
												parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
											</script>";
*/							
										/*}else{
											unlink($uploaddir.$reportfilename1);
											echo "<script>
												alert('警告：\\n\\n   【產品節次】 與 報告內容 不符 ， 請重新確認報告 。\\n\\n   【送測節次】 ： ".$Part_Number['TicketNumber']."\\n\\n   【報告節次】 ： ".$PhNC."');
												parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
											</script>";
										};*/
									/*}else{
										unlink($uploaddir.$reportfilename1);
										echo "<script>
											alert('警告：\\n\\n   【產品線別】 與 報告內容 不符 ， 請重新確認報告 。\\n\\n   【送測線別】 ： ".$Part_Number['FormingMachine']."\\n\\n   【報告線別】 ： ".$LNC."');
											parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
										</script>";
									};*/
								/*}else{
									unlink($uploaddir.$reportfilename1);
									echo "<script>
										alert('警告：\\n\\n   【產品品名】 與 報告內容 不符 ， 請重新確認報告 。\\n\\n   【送測品名】 ： ".$Part_Number['PartMold']."\\n\\n   【報告品名】 ： ".$PDC."');
										parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
									</script>";
								};*/
							/*}else{
								unlink($uploaddir.$reportfilename1);
								echo "<script>
									alert('警告：\\n\\n   【產品料號】 與 報告內容 不符 ， 請重新確認報告 。\\n\\n   【送測料號】 ： ".$Part_Number['PartNumber']."\\n\\n   【報告料號】 ： ".$PNC."');
									parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
								</script>";
							};*/
						}else{
							unlink($uploaddir.$reportfilename1);
							echo "<script>
								alert('請重新上傳僅有 【一個分頁】 的報告檔案!!\\n\\n   【報告分頁數】 ： ".$SheetsC."');
								parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
							</script>";
						};
					}else{
						unlink($uploaddir.$reportfilename1);
						echo "<script>
							alert('請重新上傳 【.xls】 or 【.xlsx】 格式的報告檔案!!\\n\\n   【報告類型】 ： ".$fileExt1." 檔案');
							parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
						</script>";
					};	
				}else{ 
					echo "<script>
						alert('報告上傳失敗 ， 請重新上傳 。Fail:C3');
						parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
					</script>";
				};		
			};
		}else{
			echo "<script>
				alert('請確實選擇報告 ， 並重新上傳 。Fail:C4');
				parent.Index_Content.location.href='Data_RequestMeasure-1.php?floor=".$floor."';
			</script>";
		};
	break;
	
/////////////////////////////////////////////////////////////////////////////////////////////////////Data_ReMeasureGV.php

	case "remeasuregv" :
		header("Content-Type:text/html; charset=utf-8");
		
		mysqli_select_db($connect,$database);
		
		$DateTime=date("Y-m-d H:i:s");
		$arr1=array();
		$arr1=$_POST['arr1'];
		$inforarr=json_decode($arr1,true);
		$arr2=array();
		$arr2=$_POST['arr2'];
		$dataarr=json_decode($arr2,true);
		
		if($inforarr[3]>8){
			for($n=0;$n<count($dataarr);$n++){
				$str1="";
				$str2="";
				for($m=0;$m<8;$m++){
					if (isset($dataarr[$n][7+2*$m]) && $dataarr[$n][7+2*$m] != ''){
						$str1=$str1."Sample".($m+1)." = '".$dataarr[$n][7+2*$m]."' , Sample".($m+1)."Modify = '".$inforarr[0]."-".$inforarr[1]."' , ";
					}
				};
				for($m=8;$m<16;$m++){
					if (isset($dataarr[$n][7+2*$m]) && $dataarr[$n][7+2*$m] != ''){
						$str2=$str2."Sample".($m+1)." = '".$dataarr[$n][7+2*$m]."' , Sample".($m+1)."Modify = '".$inforarr[0]."-".$inforarr[1]."' , ";
					}
				};
				$REGV1="UPDATE ".$DBtable4." SET ".$str1." DimNO = '".$dataarr[$n][1]."' WHERE ServiceNumber = '".$inforarr[2]."' AND DimNO = '".$dataarr[$n][1]."'";
				$query = mysqli_query($connect,$REGV1);
				$REGV2="UPDATE ".$DBtable6." SET ".$str2." DimNO = '".$dataarr[$n][1]."' WHERE ServiceNumber = '".$inforarr[2]."' AND DimNO = '".$dataarr[$n][1]."'";
				$query = mysqli_query($connect,$REGV2);
			}
		}else{
			for($n=0;$n<count($dataarr);$n++){
				$str1="";
				for($m=0;$m<8;$m++){
					if (isset($dataarr[$n][7+2*$m]) && $dataarr[$n][7+2*$m] != ''){
						$str1=$str1."Sample".($m+1)." = '".$dataarr[$n][7+2*$m]."' , Sample".($m+1)."Modify = '".$inforarr[0]."-".$inforarr[1]."' , ";
					}
				};
				$REGV1="UPDATE ".$DBtable4." SET ".$str1." DimNO = '".$dataarr[$n][1]."' WHERE ServiceNumber = '".$inforarr[2]."' AND DimNO = '".$dataarr[$n][1]."'";
				$query = mysqli_query($connect,$REGV1);
			}
		}
		$sql="SELECT Sequence FROM ".$DBtable5." WHERE ServiceNumber = '".$inforarr[2]."' and InspectionMethod = 'GV' and Endtime = '0000-00-00 00:00:00'";
		$query = mysqli_query($connect,$sql);
		$SequenceN = mysqli_fetch_array($query);
		
		$sql="UPDATE ".$DBtable5." SET Sequence = '0' , EndTime = '".$DateTime."' WHERE ServiceNumber = '".$inforarr[2]."' AND InspectionMethod = 'GV' AND Sequence <> '0' AND Endtime = '0000-00-00 00:00:00'";
		$query = mysqli_query($connect,$sql);
		$sql="UPDATE ".$DBtable5." SET Sequence = Sequence - 1 WHERE  InspectionMethod = 'GV' and Endtime = '0000-00-00 00:00:00' and Sequence > '".$SequenceN[0]."' ";
		$query = mysqli_query($connect,$sql);
		
		$sql="UPDATE ".$DBtable3." SET Status = Status + 1 WHERE ServiceNumber = '".$inforarr[2]."' AND Status < '4'";
		$query = mysqli_query($connect,$sql);
		echo "1";
	break;
	
/////////////////////////////////////////////////////////////////////////////////////////////////////Data_ReMeasurePJ.php

	case "remeasurepj" :
		header("Content-Type:text/html; charset=utf-8");
		
		mysqli_select_db($connect,$database);
		
		$DateTime=date("Y-m-d H:i:s");
		$arr1=array();
		$arr1=$_POST['arr1'];
		$inforarr=json_decode($arr1,true);
		$arr2=array();
		$arr2=$_POST['arr2'];
		$dataarr=json_decode($arr2,true);
		
		if($inforarr[3]>8){
			for($n=0;$n<count($dataarr);$n++){
				$str1="";
				$str2="";
				for($m=0;$m<8;$m++){
					if (isset($dataarr[$n][7+2*$m]) && $dataarr[$n][7+2*$m] != ''){
						$str1=$str1."Sample".($m+1)." = '".$dataarr[$n][7+2*$m]."' , Sample".($m+1)."Modify = '".$inforarr[0]."-".$inforarr[1]."' , ";
					}
				};
				for($m=8;$m<16;$m++){
					if (isset($dataarr[$n][7+2*$m]) && $dataarr[$n][7+2*$m] != ''){
						$str2=$str2."Sample".($m+1)." = '".$dataarr[$n][7+2*$m]."' , Sample".($m+1)."Modify = '".$inforarr[0]."-".$inforarr[1]."' , ";
					}
				};
				$REGV1="UPDATE ".$DBtable4." SET ".$str1." DimNO = '".$dataarr[$n][1]."' WHERE ServiceNumber = '".$inforarr[2]."' AND DimNO = '".$dataarr[$n][1]."'";
				$query = mysqli_query($connect,$REGV1);
				$REGV2="UPDATE ".$DBtable6." SET ".$str2." DimNO = '".$dataarr[$n][1]."' WHERE ServiceNumber = '".$inforarr[2]."' AND DimNO = '".$dataarr[$n][1]."'";
				$query = mysqli_query($connect,$REGV2);
			}
		}else{
			for($n=0;$n<count($dataarr);$n++){
				$str1="";
				for($m=0;$m<8;$m++){
					if (isset($dataarr[$n][7+2*$m]) && $dataarr[$n][7+2*$m] != ''){
						$str1=$str1."Sample".($m+1)." = '".$dataarr[$n][7+2*$m]."' , Sample".($m+1)."Modify = '".$inforarr[0]."-".$inforarr[1]."' , ";
					}
				};
				$REGV1="UPDATE ".$DBtable4." SET ".$str1." DimNO = '".$dataarr[$n][1]."' WHERE ServiceNumber = '".$inforarr[2]."' AND DimNO = '".$dataarr[$n][1]."'";
				$query = mysqli_query($connect,$REGV1);
			}
		}
		
		$sql="UPDATE ".$DBtable5." SET Sequence = '0' , EndTime = '".$DateTime."' WHERE ServiceNumber = '".$inforarr[2]."' AND InspectionMethod = 'PJ' AND Sequence <> '0' AND Endtime = '0000-00-00 00:00:00'";
		$query = mysqli_query($connect,$sql);

		$sql="UPDATE ".$DBtable3." SET Status = Status + 2 WHERE ServiceNumber = '".$inforarr[2]."' AND Status < '4'";
		$query = mysqli_query($connect,$sql);
		echo "1";
	break;



/////////////////////////////////////////////////////////////////////////////////////////////////////

}

?>

