<?php 
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');
$nn = "";
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Data_RequestIpqcBarcode-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="../../Public/library/Other/autoscroll.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">
#table-2 {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;  
	margin-top:0.5%;
	width: 900px;
}
#table-2 thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align:center;
	color: #4B4B4B;
	background-color: #FFDD55 ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
#table-2 td {
	padding: 1px;
	text-align:center;
	color: #333;	
	line-height: 25px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-left: 1px solid #fff;
	border-right: 1px solid #fff;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
Z0{
	font-weight:bolder;
	font-size:24px;
	}
Z1{
	background-color:#D1BBFF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#FFB7DD;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
.TXTT1{
	text-align:center;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:30px;
	font-size:16px;
}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#FFDD55;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#FF8080;
	font-size:16px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#00D9D9;
	font-size:16px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#7FFF00;
	font-size:16px;
	}
</style>

<script type="text/javascript">
$(function () {
	setupEnterToNext();
});
function setupEnterToNext() {
	$(':input').keydown(function (e) {
		if (e.keyCode == 13) {
			AddIpqc();
			e.preventDefault();
		}
	});
}
function AddIpqc(){
	var Ipqc1 = (document.getElementById('Ipqc').value).replace(/\s+/g, "");
	if (Ipqc1 != ""){
		$("#IpqcNum").append(Ipqc1+'; ');
		document.getElementById('Ipqc').value="";
		$("#Ipqc").focus();
	}else{
		alert('請輸入 【人員工號】 。');	
	}
}
function DoDPF(){
	var IpqcNum = document.getElementById('IpqcNum').textContent;
	parent.Index_Content.location.href="Data_RequestIpqcBarcode-2.php?IpqcID="+IpqcNum+""; 
}
</script>

</head>

<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<table id="table-2">
<thead>
    <tr height="60px">
        <th colspan="2">
        	<Z0> IPQC巡檢人員_條碼生成器 </Z0>
        </th>
    </tr>
</thead>
<tbody>
    <tr height="250px">
    	<td width="500px">
        	<div id="IpqcNum" style="font-size:18px"></div>
        </td>
        <td>請輸入需產生條碼之【人員工號】<br><br>如多組人員 【請一次僅輸入一組工號】 <br><br>請勿使用 【空白】 & 【其他特殊符號】</td>
    </tr>
    <tr height="50px">
        <td>
            <input type="text" name="Ipqc" id="Ipqc" size="16" class="TXTT1">&emsp;&emsp;&emsp;
            <button type="button" name="AddB" id="AddB" value="0" class="BT2" onClick="AddIpqc()">增加工號</button>
        </td>
        <td>
            <div id="export">
                <img src="Images/pdf_download.jpg" border="0" width="50px" onClick="DoDPF()">
            </div>
        </td>
    </tr>
</tbody>
</table>
</form> 
</body>
</html>

	
