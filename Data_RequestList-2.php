<?php
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
require('../../Public/library/tcpdf/tcpdf.php');
require_once('../../Public/Connections/omm_system_xz_mim.php');
 
mysqli_select_db($connect,$database);
$query_listoutF="SELECT * FROM ".$DBtable1." ORDER BY Barcode Asc";
$listoutF = mysqli_query($connect,$query_listoutF) or die(mysqli_error());

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'utf8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sanyi_Sun');
$pdf->SetTitle('成型-IPQC量測巡檢-產品資訊');
$pdf->SetSubject('成型-IPQC量測巡檢-產品資訊');
$pdf->SetKeywords('IPQC, PartNumber');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
//$pdf->SetFont('dejavusans', '', 14, '', true);
//$pdf->SetFont('msungstdlight','',16);
$pdf->SetFont('cid0jp','', 18); 

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$N=0;
$str='';
while($listout = mysqli_fetch_assoc($listoutF)){
	$str=$str.'<tr>
		<th style="width:0.1px;"></th>
		<th style="text-align:center; height:72px; line-height:72px; width:120px; color:#1E2584; font-size:10; font-weight:bold;">'. $listout["PartNumber"] . '</th>
		<th style="text-align:center; height:72px; line-height:72px; width:120px; color:#1E2584; font-size:10; font-weight:bold;">'. $listout["PartMold"] . '</th>
		<th style="text-align:center; height:72px; line-height:72px; width:80px; color:#1E2584; font-size:10; font-weight:bold;">'. $listout["DrawingNumber"] .'版</th>
		<th style="text-align:center; height:72px; line-height:72px; width:50px; color:#1E2584; font-size:10; font-weight:bold;">' . $listout["CavityNumber"] . '穴</th>
		<th style="text-align:center; height:72px; line-height:72px; width:260px;"><img src="Report/IPQC_Barcode/'.$listout["Barcode"].'.jpeg"></th>
	</tr>';
}

$html ='<div border: 1px solid #ffffff;><table width="100%" border="1">'.$str.'</table></div>';

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('IPQC-Barcode.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

?>