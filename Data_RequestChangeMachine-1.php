﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
$mlistarr = array();

$querym = "SELECT * FROM `machine_list`";

$mreqsult= mysqli_query($connect, $querym);

while($mlist = mysqli_fetch_assoc($mreqsult))
{
	if($mlist['id'] != "0")
	{
		$mlistarr[$mlist['id']] = $mlist['Machine_Number'];
	}
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_RequestChangeMachine-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script src="../../Public/library/Other/autoscroll.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/SpryAssets/SpryTabbedPanels.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/SpryAssets/SpryTabbedPanels.css">

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 980px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color:#FFDD55;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 1px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
T{
	//font-family:"PMingLiU", Gadget, sans-serif;
	font-weight:bolder;
	color: #000000;
	font-size:30px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:16px;
	}
Z1{
	color:#D1BBFF;
	font-weight:bolder;
	font-size:14px;
	}
Z2{
	color:#00B300;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	color:#00FF00;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:25px;
	background-color:#CCEEFF;
	font-size:14px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#99FF99;
	font-size:14px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:10px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:10px;
	}
a{
	text-decoration:none;
	font-size:12px;
	color:#288bc4;
	}
a:hover{
	text-decoration:underline;
	}
</style>

<script>

function ChangeMachine(N)
{
	var SelectMachine =  document.getElementById('SelectMachine'+(String(N)).substr(0,1)).value;

	if (SelectMachine!="non")
	{
		var field = document.getElementById('Change'+N).value;

		var findshow = field.split('::');

		var A = findshow[0];

		var B = findshow[1];

		var C = findshow[2];

		var D = findshow[3];

		var E = findshow[4];

		var F = findshow[5];

		var sue = confirm('提示 :  將此產品更換量測機台？ \n\n   【批號】: '+B+'\n   【機種】: '+C+'\n   【模號】: '+D+'\n   【穴數】: '+E+'\n   【機台】: '+F+'\n\n   【更換機台】: '+ N);
		if (sue == true)
		{
			var Sqq = "UPDATE <?php echo $DBtable2 ?> SET MeasureMachine = '"+ SelectMachine +"' WHERE ServiceNumber = '"+ A +"' AND Status = '1'";

			$.getJSON("Data_RequestFunction.php?Type=changemachine&floor=<?php echo $floor ?>",{Sql:Sqq},function(result){
				
				$.each(result, function(i, field)
				{
					if (field != "OK")
					{
						alert("警告 ：\n\n更換量測機台 【失敗】 ， 請重新確認 。");

						parent.Index_Content.location.href="Data_RequestChangeMachine-1.php?floor=<?php echo $floor ?>";
					}
					else
					{
						alert("提示 ：\n\n更換量測機台 【完成】 ， 排定量測機台 【"+ N +"】 ， 等待量測 。");
						
						parent.Index_Content.location.href="Data_RequestChangeMachine-1.php?floor=<?php echo $floor ?>";
					}
				});
			});
		}
	}
	else
	{
		alert("請選擇巡檢產品要移動的 【量測機台】 ！！！");
	}
}

</script>
</head>

<body>

<div class="TabbedPanels" id="TabbedPanels1">
  <ul class="TabbedPanelsTabGroup">
    <?php
  	foreach($mlistarr as $val)
	{
		echo '<li class="TabbedPanelsTab" tabindex="0">'.$val.'</li>';
	}
	?>
</ul>
  <div class="TabbedPanelsContentGroup">
	<?php

  	$i = 1;

  	foreach($mlistarr as $key => $val)
	{
		echo '<div class="TabbedPanelsContent">'; 

		$MN = $key; 

		$DTN = $key; 

		$floor1 = $floor; 

		include('Data_RequestChangeMachine-2.php'); 

		echo '</div>';
	}
	?>
  </div>
</div>

<script type="text/javascript">
	var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script> 

</body>
</html>
