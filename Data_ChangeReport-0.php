﻿<?php
session_start();
//error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];

date_default_timezone_set('Asia/Taipei');
if (date("H")<17){
	$DateTime=date("Y-m-d",strtotime("-1 day"));
}else{
	$DateTime=date("Y-m-d");
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CheckReport-0</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/SpryAssets/SpryTabbedPanels.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/SpryAssets/SpryTabbedPanels.css">

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 980px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #FFDD55;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:16px;
	}
Z1{
	background-color:#ffffff;
	color:#0080FF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#ffffff;
	color:#FF0080;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	background-color:#ffffff;
	color:#00E800;
	font-weight:bolder;
	font-size:16px;
	}
Z4{
	color:#FF00FF ;
	font-size:14px;
	}
Z5{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
Z6{
	background-color:#FF3333;
	font-weight:bolder;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#99FF99;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#CCEEFF;
	font-size:10px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:10px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:10px;
	}
a{
	text-decoration:none;
	font-size:12px;
	color:#288bc4;
	}
a:hover{
	text-decoration:underline;
	}
</style>

<script>
function codeTable(N){
	var field = document.getElementById('Del'+N).value;
	var findshow = field.split('::');
	var A = findshow[0];
	var B = findshow[1];
	var C = findshow[2];
	var D = findshow[3];
	var E = findshow[4];
	var F = findshow[5];
	sue = confirm('警告 :  將變更此巡檢量測報告？ \n\n   【批號】: '+B+'\n   【機種】: '+C+'\n   【模號】: '+D+'\n   【穴數】: '+E+'\n   【機台】: '+F)
	if (sue == true){
		codeWindws = prompt('請輸入需變更報告的 【批號】:');
		if (codeWindws==B){
			parent.Index_Content.location.href="Data_ChangeReport-2.php?SN="+A+"&floor=<?php echo $floor ?>";
		}else{
			alert("【批號】 輸入有誤！");
		};
	};
};

</script>
</head>

<body>

<div class="TabbedPanels" id="TabbedPanels1">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d") ?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d",date(strtotime("-1 day"))) ?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d",date(strtotime("-2 day"))) ?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d",date(strtotime("-3 day"))) ?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d",date(strtotime("-4 day"))) ?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d",date(strtotime("-5 day"))) ?></li>
    <li class="TabbedPanelsTab" tabindex="0"><?php echo date("Y-m-d",date(strtotime("-6 day"))) ?></li>
</ul>
  <div class="TabbedPanelsContentGroup">
	<div class="TabbedPanelsContent"><?php $DTN=1; $DT=date("Y-m-d");$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
	<div class="TabbedPanelsContent"><?php $DTN=2; $DT=date("Y-m-d",date(strtotime("-1 day")));$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
	<div class="TabbedPanelsContent"><?php $DTN=3; $DT=date("Y-m-d",date(strtotime("-2 day")));$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
	<div class="TabbedPanelsContent"><?php $DTN=4; $DT=date("Y-m-d",date(strtotime("-3 day")));$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
	<div class="TabbedPanelsContent"><?php $DTN=5; $DT=date("Y-m-d",date(strtotime("-4 day")));$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
	<div class="TabbedPanelsContent"><?php $DTN=6; $DT=date("Y-m-d",date(strtotime("-5 day")));$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
	<div class="TabbedPanelsContent"><?php $DTN=7; $DT=date("Y-m-d",date(strtotime("-6 day")));$floor1=$floor; include('Data_ChangeReport-1.php') ?></div>
  </div>
</div>

<script type="text/javascript">
	var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script> 

</body>
</html>
