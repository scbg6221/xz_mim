<?php 
session_start();
error_reporting(0);
$TC=$_REQUEST['TC'];

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

mysqli_select_db($connect,$database);

$query_listoutF = "SELECT * FROM `servicerequest` WHERE `Status`='1' AND `Sequence`>'0' ORDER BY `Sequence` ASC";

$listoutF = mysqli_query($connect, $query_listoutF) or die(mysqli_error());

$mlistarr = array();

$querym = "SELECT * FROM `machine_list`";

$mreqsult= mysqli_query($connect,$querym);

while($mlist = mysqli_fetch_assoc($mreqsult))
{
	if($mlist['id'] != "0" )
	{
		$mlistarr[$mlist['Machine_Number']] = array();
	}
}
                                       
while($l= mysqli_fetch_assoc($listoutF))
{        
	if(array_key_exists($l["MachineNumber"],$mlistarr))
	{
		array_push($mlistarr[$l["MachineNumber"]], $l);
	}
    //$listarr[$l["MachineNumber"]][] = $l;
} 
//print_r($listarr);
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_ShowProcess-2</title>

<style type="text/css">
#table-2 {
	width:inherit;
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left: 0.5%;
	margin-top: 0.5%;
}
#table-2 thead {
	width:inherit;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: <?php echo "#".$TC ?> ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align:center;
	text-shadow: white 1px 1px 1px;
}
#table-2 td {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 10px;
	line-height: 12px;
	font-style: normal;
	font-weight: normal;
	text-align:center;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
L{
	font-weight: bold;
	color: #005DBE;
	font-size:20px;
	}
</style>

</head>
<body>

<table id="table-1">
<tr>
<?php
foreach ($mlistarr as $k1 => $v1) 
{
    echo '<td width="250" valign="top">
        <table id="table-2">
            <thead>
                <tr><th colspan="4"><L>'.$k1.' 號機台排程</L></th></tr>
                <th width="25">量測序號</th>
                <th>申請單流水號</th>
                <th>產品品名</th>
                <th width="25">委託人</th>
            </thead>';
       
            foreach ($v1 as $v2) 
            {
                echo '<tr>
                      <td>' . $v2['Sequence'] . '</td>
                      <td>' . $v2['ServiceNumber'] . '</td>
                      <td>' . $v2['ProductName'] . '</td>
                      <td>' . $v2['RequestName'] . '</td>
                      </tr>';
            }                 
        echo '</table>
              </td>';
}
?>
</tr>
</table>
</body>
</html>