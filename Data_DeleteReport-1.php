﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d",strtotime("-7 day"));
/*
if (date("H")<8){
	$DateTime=date("Y-m-d",strtotime("-7 day"));
}else{
	$DateTime=date("Y-m-d");
}
*/
mysqli_select_db($connect,$database);
//$query_listoutT="SELECT * FROM ".$DBtable3." WHERE DateTime >'".$DateTime."' ORDER BY `ServiceNumber` Asc";
$query_listoutT="SELECT a.*,b.`Machine_Number` FROM `".$DBtable3."` a LEFT OUTER JOIN `machine_list` b ON a.`MeasureMachine`=b.`id` WHERE a.`DateTime` >'".$DateTime."' ORDER BY a.`ServiceNumber` ASC";
$listoutT = mysqli_query($connect,$query_listoutT) or die(mysqli_error());

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CheckReport-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
//	width: auto;
	width: 980px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #FFDD55;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 15px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:16px;
	}
Z1{
	background-color:#ffffff;
	color:#0080FF;
	font-weight:bolder;
	font-size:16px;
	}
Z2{
	background-color:#ffffff;
	color:#FF0080;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	background-color:#ffffff;
	color:#00E800;
	font-weight:bolder;
	font-size:16px;
	}
Z4{
	color:#FF00FF ;
	font-size:14px;
	}
Z5{
	color:#FF3333 ;
	font-size:16px;
	font-weight:bolder;
	}
Z6{
	background-color:#FF3333;
	font-weight:bolder;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#99FF99;
	font-size:16px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#CCEEFF;
	font-size:10px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FFEE99;
	font-size:10px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:10px;
	}
.code{
	background:url(Images/loginc.png);
	font-family:Arial;
	font-style:italic;
	color:blue;
	font-size:30px;
	border:0;
	padding:2px 3px;
	letter-spacing:3px;
	font-weight:bolder;            
	float:left;           
	cursor:pointer;
	width:150px;
	height:60px;
	line-height:60px;
	text-align:center;
	vertical-align:middle;
	}
a{
	text-decoration:none;
	font-size:12px;
	color:#288bc4;
	}
a:hover{
	text-decoration:underline;
	}
</style>

<script>
function codeTable(N){
	var field = document.getElementById('Del'+N).value;
	var findshow = field.split('::');
	var A = findshow[0];
	var B = findshow[1];
	var C = findshow[2];
	var D = findshow[3];
	var E = findshow[4];
	var F = findshow[5];
	sue = confirm('警告 :  將刪除此巡檢量測報告？ \n\n   【批號】: '+B+'\n   【機種】: '+C+'\n   【模號】: '+D+'\n   【穴數】: '+E+'\n   【機台】: '+F)
	if (sue == true){
		codeWindws = prompt('請輸入需刪除報告的 【批號】:');
		if (codeWindws==B){
			$("#form1").append("<input type=hidden name=DelSN value='"+A+"'/>");
			parent.Index_Content.location.href="Data_DeleteReport-2.php?SN="+A+"&floor=<?php echo $floor ?>";
		}else{
			alert("【批號】 輸入有誤！");
		};	
	}else{
		
	};
};
/*
var code;
function createCode() {
	code = "";
	var codeLength = 6;
	var checkCode = document.getElementById("checkCode");
	var codeChars = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
		'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
	for (var i = 0; i < codeLength; i++){
		var charNum = Math.floor(Math.random() * 52);
		code += codeChars[charNum];
	}
	if (checkCode){
		checkCode.className = "code";
		checkCode.innerHTML = code;
	}
}
 
function validateCode(a){
	var inputCode = document.getElementById("inputCode").value;
	if (inputCode.length <= 0){
		alert("請輸入驗證碼！");
	}else if (inputCode.toUpperCase() != code.toUpperCase()){
		alert("驗證碼輸入有誤！");
		createCode();
	}else{
		alert("驗證碼正確！");
	}       
}
*/

</script>


</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >

<!--
    <table border="0" cellspacing="5" cellpadding="5" >
        <tr>
            <td></td><td> <div class="code" id="checkCode" onclick="createCode()" ></div></td>
            <td><a  href="#" onclick="createCode()">看不清换一张</a></td>
        </tr>
        <tr>
            <td>验证码：</td><td><input  style="float:left;" type="text"   id="inputCode" /></td><td>请输入验证码</td>
        </tr>
        <tr><td></td><td><input id="Button1"  onclick="validateCode();" type="button" value="确定" /></td><td></td>
        </tr>
    </table>
-->

<table id="table-T" class="sortable">
<thead>
	<th>報告狀態</th>
	<th>批號</th>
	<th>機種</th>
	<th>模號</th>
    <th>穴數</th>
	<th>成型機台</th>
	<th>GV測量員</th>
    <th>測量機台</th>
	<th>IPQC檢測員</th>
    <th>操作欄位</th>
</thead>
<?php
$N=0;
while($listout = mysqli_fetch_assoc($listoutT)){
	$N=$N+1;
	echo "<tr height=35px>";
	switch ($listout['Status']){
		Case "0" : echo "<td><Z1>首次檢視報告</Z1></td>"; break;
		Case "1" : echo "<td><Z2>GV & 自行覆判</Z2></td>"; break;
		Case "2" : echo "<td><Z2>自行覆判</Z2></td>"; break;
		Case "3" : echo "<td><Z2>GV覆判</Z2></td>"; break;
		Case "4" : echo "<td><Z1>重複檢視報告</Z1></td>"; break;
		Case "9" : echo "<td><Z3>報告確認完成</Z3></td>"; break;
	}
	echo "<td><Z0>" . $listout['TicketNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['PartNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['PartMold'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['CavityNumber'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['FormingMachine'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['GV_Inspector'] . "</Z0></td>";
	echo "<td><Z0>" . $listout['Machine_Number'] . "</Z0></td>";
	if ($listout['Status']=="0"){
		echo "<td><Z0>" . $listout['IPQC_Inspector'] . "</Z0></td>";
	}else{
		echo "<td><Z0>" . $listout['IPQC_Inspector'] . "</Z0></td>";
	}

	echo $BB = "<td><button type=button id=Del".$N." value=".$listout['ServiceNumber']."::".$listout['TicketNumber']."::".$listout['PartNumber']."::".$listout['PartMold']."::".$listout['CavityNumber']."::".$listout['FormingMachine']."  class=BT0 onClick='codeTable(".$N.")'>刪除</td>";
	echo "</tr>";
}
?>
</table>
</form>
</body>
</html>
