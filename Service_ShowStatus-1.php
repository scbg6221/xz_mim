<?php 
session_start();
error_reporting(0);
$TC=$_REQUEST['TC'];

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$submit=$_POST['submit'];
mysqli_select_db($connect,$database);

$query_listoutF="SELECT * FROM `servicerequest` WHERE `Status`<'2' and `Status`>'-2' ORDER BY `ServiceNumber` Desc";
$listoutF = mysqli_query($connect,$query_listoutF) or die(mysqli_error());
 
date_default_timezone_set('Asia/Taipei');
$DateTime=date("YmdHis",strtotime("-7 day"));
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_ShowStatus-1</title>

<script src="../../Public/library/Other/Sorttable.js"></script>

<style type="text/css">
#table-2 {
	
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0.5%;   
	margin-top:0.5%;
		
}
#table-2.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: <?php echo "#".$TC ?> ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	width:auto;
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 16px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
#table-2 td {
	padding: 5px;
	color: #333;
	line-height: 15px;
	width:auto;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	text-align:center;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
#table-2 aa {
	font-size: 14px;
	text-align:center;
	color: #d2691e;
}
#table-2 bb {
	font-size: 14px;
	text-align:center;
	color: #8a2be2;
}
#table-2 cc {
	font-size: 14px;
	text-align:center;
	color: #008000;
}
</style>
	</head>

<body>
<?php echo "【 點擊表頭可以更改排序 】"?>
<form id="form1" name="form1" method="post" >
<table id="table-2" class="sortable">
<thead>
<th>申請狀態</th>
<th>量測機台</th>
<th>申請單流水號</th>
<th>產品品名</th>
<th>正崴料號</th>
<th>送樣數量</th>
<th>量測尺寸數</th>
<th>委託部門</th>
<th>委託人</th>
<th>電話</th>
<th>需求日期</th>
<th>預計完成日期</th>
</thead>
<tbody>
<?php
while($listout = mysqli_fetch_assoc($listoutF)){	 
	if ( $listout['Status'] == -1 ){
		if ( strtotime($listout['ServiceNumber'])-strtotime($DateTime)>0 ){
			echo "<tr>";
			echo "<td>" . "<aa>退回申請</aa>" . "</td>";
			echo "<td>-</td>";
			echo "<td>" . $listout['ServiceNumber'] . "</td>";
			echo "<td>" . $listout['ProductName'] . "</td>";
			echo "<td>" . $listout['ProductFoxlinkPN'] . "</td>";
			echo "<td>" . $listout['ProductPcsQuantity'] . "</td>";
			echo "<td>" . $listout['ProductOneQuantity'] . "</td>";
			echo "<td>" . $listout['RequestDepartment'] . "</td>";
			echo "<td>" . $listout['RequestName'] . "</td>";
			echo "<td>" . $listout['RequestPhone'] . "</td>";
			echo "<td>" . $listout['ProductNeedDate'] . "</td>";
			echo "<td><aa>" . $listout['BackCause'] . "</aa></td>";
			echo "</tr>";
		}
	}else{
			echo "<tr>";
			switch ( $listout['Status'] ){
				case "0":
				  $SNS="<bb>簽核中</bb>";
				  break;
				case "1":
				  $SNS="<cc>進入排程</cc>";
				  break;
				default:
				  echo " ";
			}
			echo "<td>" . $SNS . "</td>";
			if ($listout['MachineNumber']=="NON_Machine"){
				echo "<td>尚未指定機台</td>";
				}else{
				echo "<td>" . $listout['MachineNumber'] . "</td>";
				}
			echo "<td>" . $listout['ServiceNumber'] . "</td>";
			echo "<td>" . $listout['ProductName'] . "</td>";
			echo "<td>" . $listout['ProductFoxlinkPN'] . "</td>";
			echo "<td>" . $listout['ProductPcsQuantity'] . "</td>";
			echo "<td>" . $listout['ProductOneQuantity'] . "</td>";
			echo "<td>" . $listout['RequestDepartment'] . "</td>";
			echo "<td>" . $listout['RequestName'] . "</td>";
			echo "<td>" . $listout['RequestPhone'] . "</td>";
			echo "<td>" . $listout['ProductNeedDate'] . "</td>";
			if ($listout['ForecastEndDate']=="0000-00-00"){
				echo "<td>-</td>";
			}else{
				echo "<td>" . $listout['ForecastEndDate'] . "</td>";
			}
			echo "</tr>";
			}
	}

?>
</tbody>
</table>
</form>
</body>
</html>