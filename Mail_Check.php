<meta charset="utf-8">

<?php

$sql="SELECT * FROM `servicerequest` WHERE `ServiceNumber`='$SN'";
$query= mysqli_query($connect,$sql) ;
$Uch = mysqli_fetch_array($query);

$from = "【ICBU-GV量測委託系統】";
$msgend = "<br><br>******************** 【 郵件為系統自動發出 ， 請勿直接回覆 】 ********************";

if (strchr($Uch['RequestEmail'],"@")==false){
	$RequestEmail=$Uch['RequestEmail']."@cn.foxlink.com.tw";
}else{
	$RequestEmail=str_split($Uch['RequestEmail'],stripos($Uch['RequestEmail'],"@"));
	$RequestEmail=$RequestEmail[0]."@cn.foxlink.com.tw";
};

$EmailCC = "";
$RequestEmailCC=$Uch['RequestEmailCC'];
while(strchr($RequestEmailCC,",")<>false){
	$RequestEmailCC_A=str_split($RequestEmailCC,stripos($RequestEmailCC,","));
	$RequestEmailCC_A=$RequestEmailCC_A[0]."@cn.foxlink.com.tw, ";
	$EmailCC=$EmailCC . $RequestEmailCC_A;
	$RequestEmailCC=substr(stristr($RequestEmailCC,","),1);
};
if (strlen(ltrim($RequestEmailCC))<>0){
	$RequestEmailCC_A=$RequestEmailCC."@cn.foxlink.com.tw";
	$EmailCC=$EmailCC . $RequestEmailCC_A;
};

switch ($mailm){
	case "mailPlease" :
		$to = $RequestEmail ;
		$ccto = $EmailCC ;
		$subject = "GV量測委託通知單 ： 【 委託單 : ".$SN." ， 已提出申請 ， 請將產品送至量測室完成審核 】";
		$msg ="Dear ".$Uch['RequestName']." : <br><br>
		您好 ， 您申請的量測委託案件進度 ： <br><br>
		量測委託單 ： 【 ".$SN." 】 ， 委託人 / 電話 : 【 ".$Uch['RequestName']." / ".$Uch['RequestPhone']." 】 ， <br><br>
		已提出申請 ， 請將產品送至量測室完成審核 ， <br><br>
		如有問題 ， 請與相關單位提出 ， 非常感謝 。 <br><br> ";
		break; 
	case "mailGo" :
		$to = $RequestEmail ;
		$ccto = $EmailCC ;
		$subject = "GV量測委託通知單 ： 【 委託單 : ".$SN." ， 已進入量測排程 ， 排定量測機台 ： ".$MN." ， 量測排序 ： ".$S." ， 預計完成日期 ： ".$Uch['ForecastEndDate']." 】";
		$msg ="Dear ".$Uch['RequestName']." : <br><br>
		您好 ， 您申請的量測委託案件進度 ： <br><br>
		量測委託單 : 【 ".$SN." 】 ， 排定量測機台 ： 【 ".$MN." 】 ， 量測排序 ： 【 ".$S." 】 ， 預計完成日期 ： 【 ".$Uch['ForecastEndDate']." 】 ， <br><br>
		如有問題 ， 請與相關單位提出 ， 非常感謝 。 <br><br> ";
		break; 
	case "mailBack" :
		$to = $RequestEmail ;
		$ccto = $EmailCC ;
		$subject = "GV量測委託通知單 ： 【 委託單 : ".$SN." ， 已退回申請 】";
		$msg ="Dear ".$Uch['RequestName']." : <br><br>
		您好 ， 您申請的量測委託案件進度 ： <br><br>
		量測委託單 : 【 ".$SN." 】 ， 已退回申請 ， 原因 ： 【 ".$Uch['BackCause']." 】 ， <br><br>
		如有問題 ， 請與相關單位提出 ， 非常感謝 。 <br><br> ";
		break; 
	case "mailOver" :
		$to = $RequestEmail ;
		$ccto = $EmailCC ;
		$subject = "GV量測委託通知單 ： 【 委託單 : ".$SN." ， 已完成量測 】";
		$msg ="Dear ".$Uch['RequestName']." : <br><br>
		您好 ， 您申請的量測委託案件進度 ： <br><br>
		量測委託單 : 【 ".$SN." 】 ， 已完成量測 ， 並將報告附於信件內 ， 請幫忙查收 ， <br><br>
		如有問題 ， 請與相關單位提出 ， 非常感謝 。 <br><br> ";
		$filename2="Report/Service_Report/".mb_convert_encoding($Uch['ProductReport'],"big5","utf8");
		break; 
}

$from = "=?UTF-8?B?". base64_encode($from)."?=";
$subject = "=?UTF-8?B?". base64_encode($subject)."?=";
$boundary = "==".md5(time());

$header ="Content-Type: multipart/mixed; boundary=\"{$boundary}\"\r\nFrom:".$from."\r\n";
$header .="CC:".$ccto."\r\n";

$message = "This is a multi-part message in MIME format.\n\n" ."--{$boundary}\n" . "Content-Type: text/html; charset=\"utf-8\"\n" . "Content-Transfer-Encoding: 8bit\n\n" . $msg . "\n\n";
$message .= "--{$boundary}\n";

if ($filename2){
	$mimeType = mime_content_type($filename2);
	if(!$mimeType)$mimeType ="application/unknown";
	$files[]=array($filename2,$mimeType,$Uch['ProductReport']);
	for($x=0;$x < count($files);$x++){
		$file = fopen($files[$x][0],"r");
		$data = fread($file,filesize($files[$x][0]));
		fclose($file);
		$data = chunk_split(base64_encode($data));
		$message .= "Content-Type: \"".$files[$x][1]."\";\r\n" . " name=\"=?utf-8?B?".base64_encode($files[$x][2])."?=\"\r\n" .
		"Content-Disposition: attachment;". " filename=\"=?utf-8?B?".base64_encode($files[$x][2])."?=\"\r\n" .
		"Content-Transfer-Encoding: base64\r\n\r\n" . $data . "\r\n\r\n";
		$message .= "--{$boundary}\n";   
	}
}

$message .= "--{$boundary}\n" . "Content-Type: text/html; charset=\"utf-8\"\n" . "Content-Transfer-Encoding: 8bit\n\n" . $msgend . "\n\n";

mail($to,$subject,$message,$header)

?>

