﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}

$mlistarr = array();

$querym = "SELECT * FROM `machine_list`";

$mreqsult= mysqli_query($connect,$querym);

while($mlist = mysqli_fetch_assoc($mreqsult))
{
	if($mlist['id'] != "0")
	{
		$mlistarr[$mlist['id']] = $mlist['Machine_Number'];
	}
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_RequestMeasure-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/SpryAssets/SpryTabbedPanels.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/SpryAssets/SpryTabbedPanels.css">

<style type="text/css">
.sortable {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	/* [disabled]margin-left:0.5%; */
	margin-top: 0.5%;
	width: 980px;
}
.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #CCCEF0;
	border-bottom: solid 1px #999;
}
.sortable th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: bold;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
.sortable td {
	padding: 5px;
	text-align:center;
	color: #333;	
	line-height: 1px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
T{
	//font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:30px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:16px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z0{
	font-weight:bolder;
	font-size:16px;
	}
Z1{
	color:#D1BBFF;
	font-weight:bolder;
	font-size:14px;
	}
Z2{
	color:#00B300;
	font-weight:bolder;
	font-size:16px;
	}
Z3{
	color:#00FF00;
	font-size:16px;
	}
.BT0{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:25px;
	background-color:#CCEEFF;
	font-size:14px;
	}
.BT1{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:30px;
	background-color:#99FF99;
	font-size:14px;
	}
.BT2{
	margin-right:5px;
	border:#000000;
	border:3px;
	border-radius:5px;
	height:25px;
	background-color:#FFEE99;
	font-size:14px;
	}
.BT3{
	margin-right:5px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:20px;
	background-color:#FF3333;
	font-size:10px;
	}
a{
	text-decoration:none;
	font-size:12px;
	color:#288bc4;
	}
a:hover{
	text-decoration:underline;
	}
</style>

<script>
function StartTime(N){
	var field = document.getElementById('Start'+N).value;
	var findshow = field.split('::');
	var A = findshow[0];
	var B = findshow[1];
	var C = findshow[2];
	var D = findshow[3];
	var E = findshow[4];
	var F = findshow[5];
	var sue = confirm('提示 :  將開始此產品量測？ \n\n   【批號】: '+B+'\n   【機種】: '+C+'\n   【模號】: '+D+'\n   【穴數】: '+E+'\n   【機台】: '+F);
	if (sue == true){
		parent.Index_Content.location.href="Data_RequestFunction.php?Type=measurestart&SN="+A+"&floor=<?php echo $floor ?>";
	}
}
function Report(N){	
	document.getElementById('Over'+N).disabled="disabled";
	var Tr = document.getElementById("table-T"+(String(N)).substr(0,1)).insertRow(parseInt((String(N)).substr(1))+1);	
	Tr.style.backgroundColor="#CCCEF0";
	Tr.innerHTML='<td height="100"><L>選擇報告 ： </L></td><td colspan="5"><input type="file" style="font-size:16px;width:550px;height:30px;" name="upload_file1" id="upload_file1" class="BT1" ></td><td colspan="2"><input type="submit" name="sureB" id="sureB" class="BT1" onClick="return OverTime('+N+')" value="確定 報告" >&emsp;&emsp;&emsp;<button type="button" name="cancelB" id="cancelB" class="BT2" onClick="Cancel()" >取消</button></td>';
}
function OverTime(N){
	var upload_file1 = document.getElementById("upload_file1").value;
	if (upload_file1){
		var field = document.getElementById('Over'+N).value;
		var findshow = field.split('::');
		var A = findshow[0];
		var B = findshow[1];
		var C = findshow[2];
		var D = findshow[3];
		var E = findshow[4];
		var F = findshow[5];
		var sue = confirm('提示 :  將開始此產品量測？ \n\n   【批號】: '+B+'\n   【機種】: '+C+'\n   【模號】: '+D+'\n   【穴數】: '+E+'\n   【機台】: '+F);
		if (sue == true){
			document['form'+(String(N)).substr(0,1)].action = "Data_RequestFunction.php?Type=measureover1&SN="+A+"&floor=<?php echo $floor ?>";
			document['form'+(String(N)).substr(0,1)].target = 'Index_Content';
		}else{
			return false;
		}
	}else{
		alert('請確實選擇 【 上傳報告 】  。');
		return false;
	}
}
function Cancel(){
	parent.Index_Content.location.href="Data_RequestMeasure-1.php?floor=<?php echo $floor ?>"; 
}
</script>
</head>

<body>

<div class="TabbedPanels" id="TabbedPanels1">
  <ul class="TabbedPanelsTabGroup">
  	<?php
  	foreach($mlistarr as $val)
	{
		echo '<li class="TabbedPanelsTab" tabindex="0">'.$val.'</li>';
	}
	?>
</ul>
  <div class="TabbedPanelsContentGroup">

  	<?php

  	$i = 1;

  	foreach($mlistarr as $key => $val)
	{
		echo '<div class="TabbedPanelsContent">'; 

		$MN = $key; 

		$DTN = $key; 

		$floor1 = $floor; 

		include('Data_RequestMeasure-2.php'); 

		echo '</div>';
	}
	?>
  </div>
</div>

<script type="text/javascript">
	var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script> 

</body>
</html>