<?php 
session_start();
error_reporting(0);
$TC=$_REQUEST['TC'];

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

date_default_timezone_set('Asia/Taipei');
$Button_Date=$_POST['Button_DateSearch'];
if($Button_Date){
	$Time1="0".strtok(str_replace(" ","",$_POST['Infor1']),":").strtok(" ");
	$T1=sprintf('%02d',(int)substr($Time1,-6,2));
	$T2=sprintf('%02d',(int)substr($Time1,-4,2));
	$T3=strtolower(substr($Time1,-2,2));
	$Time1=date('H:i', strtotime("{$T1}:{$T2} {$T3}"));
	$Time2="0".strtok(str_replace(" ","",$_POST['Infor2']),":").strtok(" ");
	$T1=sprintf('%02d',(int)substr($Time2,-6,2));
	$T2=sprintf('%02d',(int)substr($Time2,-4,2));
	$T3=strtolower(substr($Time2,-2,2));
	$Time2=date('H:i', strtotime("{$T1}:{$T2} {$T3}"));
	$Date1=$_POST['Infor3'];
	$Date2=$_POST['Infor4'];
}else{
	$Time1="20:00";
	$Time2="20:00";
	$Date1=date("Y-m-d",date(strtotime("-7 day")));
	$Date2=date("Y-m-d");
};

date_default_timezone_set('Asia/Taipei');

mysqli_select_db($connect,$database);
$query_listoutF="SELECT * FROM servicerequest WHERE Status='9' and ProductEndDate BETWEEN '".$Date1." ".$Time1."' AND '".$Date2." ".$Time2."' ORDER BY MachineNumber Desc";
$listoutF = mysqli_query($connect,$query_listoutF) or die(mysqli_error());
 
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_ShowStatus-3</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/tableExport/dist/tableExport.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/time_picker/dist/wickedpicker.min.js"></script>
<script src="../../Public/library/Other/Sorttable.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/time_picker/dist/wickedpicker.min.css"/>

<style type="text/css">
#table-2 {
	
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0.5%;   
	margin-top:0.5%;
		
}
#table-2.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: <?php echo "#".$TC ?> ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	width:auto;
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 16px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
#table-2 td {
	padding: 5px;
	color: #333;
	line-height: 15px;
	width:auto;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	text-align:center;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
#table-2 ee {
	font-size: 14px;
	text-align:center;
	color: #32cd32;
}
</style>

<script>
$(function(){
	$( "#Infor3" ).datepicker(
		$.extend(
		$.datepicker.regional['zh-TW'],
		{
//			maxDate:5,
//			minDate:0,
			dateFormat:"yy-mm-dd",
      		onClose: function( selectedDate ) {$( "#Infor4" ).datepicker( "option", "minDate", selectedDate );},
    	}));
    $( "#Infor4" ).datepicker(
		$.extend(
		$.datepicker.regional['zh-TW'],
		{
//			maxDate:14,
//			minDate:0,
			dateFormat:"yy-mm-dd",
    	}));
		
	$('.Infor1').wickedpicker({now:"<?php echo $Time1 ?>",twentyFour:false,title:'起始_時間選擇器'});
	$('.Infor2').wickedpicker({now:"<?php echo $Time2 ?>",twentyFour:false,title:'結束_時間選擇器'});	
});

function DateSearch(){
	document['form1'].action = 'Service_ShowStatus-3.php?TC=afeeee';
    document['form1'].target = 'Index_Content';
}

</script>

</head>

<body>
<form id="form1" name="form1" method="post" >
<table id="table-1" class="sortable1">
    <td align="center">完成日期 :</td>
    <td align="center">
        <input type="text" name="Infor3" id="Infor3" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Date1 ?>">&emsp;&emsp;
        <input type="text" name="Infor1" id="Infor1" style="font-size:12px; width:100px; height:20px; text-align:center;" class="Infor1">
        &emsp;&emsp;<z>─</z>&emsp;&emsp;
        <input type="text" name="Infor4" id="Infor4" style="font-size:12px; width:100px; height:20px; text-align:center;" value="<?php echo $Date2 ?>">&emsp;&emsp;
        <input type="text" name="Infor2" id="Infor2" style="font-size:12px; width:100px; height:20px; text-align:center;" class="Infor2">
        &emsp;&emsp;&emsp;
    </td>
    <td>
        <input type="submit" name="Button_DateSearch" id="Button_Button_DateSearch" class="BT1" style="font-size:15px; font-weight:bolder; width:100px; height:30px" value="搜尋案件" onclick="DateSearch()">&emsp;&emsp;&emsp;
    </td>
    <td>
        <a href="javascript:;"><img src="./Images/Download_excel.jpg" data-type="xls" id="export" border="0" width="50px" title="下載報告"></a>
    </td>
</tr>
</table>
<table id="table-2" class="sortable">
<thead>
<th>完成時間</th>
<th>量測人員</th>
<th>量測機台</th>
<th>產品品名</th>
<th>正崴料號</th>
<th>送樣數量</th>
<th>量測尺寸數</th>
<th>委託種類</th>
<th>委託部門</th>
<th>委託人</th>
<th>單據狀態</th>
</thead>
<tbody>
<?php
while($listout = mysqli_fetch_assoc($listoutF)){
		echo "<tr>";
		echo "<td>" . $listout['ProductEndDate'] . "</td>";
		echo "<td>" . $listout['ProductEndPersonnel'] . "</td>";
		echo "<td>" . $listout['MachineNumber'] . "</td>";
		echo "<td>" . $listout['ProductName'] . "</td>";
		echo "<td>" . $listout['ProductFoxlinkPN'] . "</td>";
		echo "<td>" . $listout['ProductPcsQuantity'] . "</td>";
		echo "<td>" . $listout['ProductOneQuantity'] . "</td>";
		echo "<td>" . $listout['ProductObject'] . "</td>";
		echo "<td>" . $listout['RequestDepartment'] . "</td>";
		echo "<td>" . $listout['RequestName'] . "</td>";
		switch ( $listout['Priority'] ){
			case "ExtruUrgent":
			  $pr="特急";
			  break;
			case "Urgent":
			  $pr="加急";
			  break;
			case "General":
			  $pr="普通";
			  break;
		}
		echo "<td>" . $pr . "</td>";
		echo "</tr>";

}

?>

</tbody>
</table> 
</form>

<script>
	var $exportLink = document.getElementById('export');
	var exportData1 = document.getElementById('Infor3').value +" : "+ document.getElementById('Infor1').value;;
	var exportData2 = document.getElementById('Infor4').value +" : "+ document.getElementById('Infor2').value;;
	$exportLink.addEventListener('click', function(e){
		e.preventDefault();
		if(e.target.nodeName == "IMG"){
			tableExport('table-2', '量測委託單-案件總表_'+exportData1+'_'+exportData2, e.target.getAttribute('data-type'));
		}
	}, false);
</script>

</body>
</html>
