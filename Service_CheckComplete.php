<?php 
session_start();
error_reporting(0);
$TC=$_REQUEST['TC'];

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');


$SN=$_POST['SN'];
mysqli_select_db($connect,$database);
if ($SN){
	date_default_timezone_set('Asia/Taipei');
	$DateTime=date("Y-m-d H:i:s");
	$ProductEndPersonnel=$_POST['ProductEndPersonnel'];

//報告檔案上傳//	
	$uploaddir="Report/Service_Report/";
	$_SESSION['upfile']=$SN."_".$_POST['upfile'];
	$_SESSION['upload_file_name']=$_FILES["upload_file"]["name"]; 
	$_SESSION['Error']=$_FILES['upload_file']['error'];
	$_SESSION['tmpfile']=$_FILES["upload_file"]["tmp_name"]; 
	$_SESSION['file2']=mb_convert_encoding($_FILES["upload_file"]["name"],"big5","utf8"); 

	if(move_uploaded_file($_SESSION['tmpfile'],$uploaddir.$SN."_".$_SESSION['file2'])){
		$sql="UPDATE `servicerequest` SET `Status`='9',`Sequence`='0',`ProductEndDate`='$DateTime',`ProductEndPersonnel`='$ProductEndPersonnel',`ProductReport`='$_SESSION[upfile]' WHERE `Status` = '1'and `Sequence`='1' and `ServiceNumber` = '$SN' ";
		$query= mysqli_query($connect,$sql) ;
		$mailm = "mailOver";
		include 'Mail_Check.php';
		$sql="SELECT `MachineNumber` FROM `servicerequest` WHERE `ServiceNumber` = '$SN' ";
		$query= mysqli_query($connect,$sql) ;
		$MN = mysqli_fetch_array($query);
		$sql= "SELECT MAX(`Sequence`) `MAX` FROM ( SELECT `Status` , `MachineNumber` , `Sequence` FROM `servicerequest` WHERE `Status` = '1' AND `MachineNumber` = '$MN[0]') AS T";
		$query= mysqli_query($connect,$sql) ;
		$AA = mysqli_fetch_array($query);
		$S=$AA['MAX'];
		for ($N=1;$N<$S;$N++){
			$N1=$N+1;
			$sql="UPDATE `servicerequest` SET `Sequence`='$N' WHERE `Status`='1' and `Sequence`='$N1' and `MachineNumber`='$MN[0]'";
			$query= mysqli_query($connect,$sql) ;
		}
		echo "<script>alert('委託單:".$SN."，完成量測，並上傳報告');</script> ";
	}else{ 
		echo "<script>alert('檔案上傳失敗，請重新操作');</script> ";
	} 
}

$query_listoutF="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`='1' ORDER BY `MachineNumber` Asc";
$listoutF = mysqli_query($connect,$query_listoutF) or die(mysqli_error());
 
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_CheckComplete</title>

<style type="text/css">
#table-2 {
	
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0.5%;   
	margin-top:0.5%;
		
}
#table-2 td{
	padding: 5px;
	text-align:center;
	color: #333;
}
#table-2 thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: <?php echo "#".$TC ?> ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	width:auto;
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 17px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: left;
	text-shadow: white 1px 1px 1px;
}
#table-2 td {
	
	line-height: 15px;
	width:auto;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
priorityR {
	color:#FF8080;
}
priorityG {
	color:#80FF80;
}
</style>
	</head>

<script type="text/javascript">
function result() {
    document['form1'].action = "Service_CheckComplete_1.php?TC=<? echo $TC ?>";
	document['form1'].target = 'Index_Content';
}
</script>

<body>
<form id="form1" name="form1" method="post" enctype="multipart/form-data" >
<table id="table-2">
<thead>
<th>報告完成</th>
<th>量測機台</th>
<th>委託等級</th>
<th>申請單流水號</th>
<th>產品品名</th>
<th>正崴料號</th>
<th>送樣數量</th>
<th>量測尺寸數</th>
<th>委託部門</th>
<th>委託人</th>
<th>電話</th>
<th>電子郵箱</th>
<th>委託日期</th>
<th>需求日期</th>
</thead>
<tbody>
<?php
$N=0;
while($listout = mysqli_fetch_assoc($listoutF)){
	$N=$N+1;	 
	echo "<tr>";
	echo "<td>" ."<input type=submit name=submit id=submit value=完成$N ; onClick=result()>". "</td>";
	echo "<td>" . $listout['MachineNumber'] . "</td>";
	if($listout['Priority']=="ExtruUrgent"){$I3="<priorityR>特急</priorityR>";}
		elseif($listout['Priority']=="Urgent"){$I3="<priorityG>加急</priorityG>";}
		else{$I3="普通";}
	echo "<td>" . $I3 . "</td>";
	echo "<td>" . $listout['ServiceNumber'] . "</td>";
	$aa[$N] = $listout['ServiceNumber'];
	echo "<td>" . $listout['ProductName'] . "</td>";
	echo "<td>" . $listout['ProductFoxlinkPN'] . "</td>";
	echo "<td>" . $listout['ProductPcsQuantity'] . "</td>";
	echo "<td>" . $listout['ProductOneQuantity'] . "</td>";
	echo "<td>" . $listout['RequestDepartment'] . "</td>";
	echo "<td>" . $listout['RequestName'] . "</td>";
	echo "<td>" . $listout['RequestPhone'] . "</td>";
	echo "<td>" . $listout['RequestEmail'] . "</td>";
	echo "<td>" . $listout['RequestDatetime'] . "</td>";
	echo "<td>" . $listout['ProductNeedDate'] . "</td>";
	echo "</tr>";
	}
	if ($N>0){
		$SN=implode("#",$aa);
		echo "<input type=hidden name=SN id=SN value=$SN>";
		echo "<input type=hidden name=TC id=TC value=$TC>";
	}
?>
</tbody>
</table>
</form>
</body>
</html>