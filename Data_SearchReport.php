<?php 

require_once('../../Public/Connections/omm_system_xz_mim.php');

session_start();
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
$SearchType=$_GET['SearchType'];
date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d H:i:s",strtotime("-7 day"));
 
/////////////////////////////////////////////////////////////////////////////////////////////////////

switch ($SearchType){

	case "SC1" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT PartNumber FROM ".$DBtable3." WHERE PartNumber LIKE '%$my_dataP%' and MeasureEndTime > '".$DateTime."' GROUP BY PartNumber ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['PartNumber']);
			}
		}
		break;
		
	case "SC2" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT PartMold FROM ".$DBtable3." WHERE PartMold LIKE '%$my_dataP%' and MeasureEndTime > '".$DateTime."' GROUP BY PartMold ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['PartMold']);
			}
		}
		break;
		
	case "SC3" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT FormingMachine FROM ".$DBtable3." WHERE FormingMachine LIKE '%$my_dataP%' and MeasureEndTime > '".$DateTime."' GROUP BY FormingMachine ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['FormingMachine']);
			}
		}
		break;
		
	case "SC4" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT TicketNumber FROM ".$DBtable3." WHERE TicketNumber LIKE '%$my_dataP%' and MeasureEndTime > '".$DateTime."' GROUP BY TicketNumber ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['TicketNumber']);
			}
		}
		break;
		
	case "SC1_NT" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT PartNumber FROM ".$DBtable3." WHERE PartNumber LIKE '%$my_dataP%' GROUP BY PartNumber ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['PartNumber']);
			}
		}
		break;
		
	case "SC2_NT" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT PartMold FROM ".$DBtable3." WHERE PartMold LIKE '%$my_dataP%' GROUP BY PartMold ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['PartMold']);
			}
		}
		break;
		
	case "SC3_NT" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT FormingMachine FROM ".$DBtable3." WHERE FormingMachine LIKE '%$my_dataP%' GROUP BY FormingMachine ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['FormingMachine']);
			}
		}
		break;
		
	case "SC4_NT" :
		$q=$_GET['term'];
		$my_dataP=mysqli_real_escape_string($connect,$q);
		$Report_search_array=array();
		
		mysqli_select_db($connect,$database);
		$Search = "SELECT TicketNumber FROM ".$DBtable3." WHERE TicketNumber LIKE '%$my_dataP%' GROUP BY TicketNumber ";
		$Search_ID = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_ID){
			while($row=mysqli_fetch_array($Search_ID)){
				array_push($Report_search_array,$row['TicketNumber']);
			}
		}
		break;	
		
	case "Report_CK" :
		$q=$_GET['Sql'];
		$Report_search_array=array();
		mysqli_select_db($connect,$database);
		$Search = "$q";
		$Search_Report = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_Report){
			while($row=mysqli_fetch_array($Search_Report)){
				array_push($Report_search_array,$row['ServiceNumber']."::".$row['PartNumber']."::".$row['PartMold']."::".$row['FormingMachine']."::".$row['TicketNumber']);
			}
		}
		break;
				
	case "Report_OV" :
		$q=$_GET['Sql'];
		$Report_search_array=array();
		mysqli_select_db($connect,$database);
		$Search = "$q";
		$Search_Report = mysqli_query($connect,$Search) or die(mysqli_error());
		
		if($Search_Report){
			while($row=mysqli_fetch_array($Search_Report)){
				array_push($Report_search_array,$row['ServiceNumber']."::".$row['PartNumber']."::".$row['PartMold']."::".$row['FormingMachine']."::".$row['TicketNumber']."::".$row['DateTime']);
			}
		}
		break;
}


echo json_encode($Report_search_array); 

?>