<?php
session_start();
error_reporting(0);
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}

//include '../../Public/MainWebUI/User_Count.php';
//include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

mysqli_select_db($connect,$database);
$Report_search_array=array();
$sql="SELECT ServiceNumber FROM ".$DBtable5." WHERE InspectionMethod = 'GV' and Sequence <> '0' ORDER BY Sequence Asc";
$SN= mysqli_query($connect,$sql) ;

date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d H:i:s",strtotime("-7 day"));

while($SNN = mysqli_fetch_assoc($SN)){
	$sql="SELECT ServiceNumber , PartMold , FormingMachine , TicketNumber FROM ".$DBtable3." WHERE ServiceNumber = '".$SNN['ServiceNumber']."' and MeasureEndTime > '".$DateTime."' ";
	$query= mysqli_query($connect,$sql) ;
	$GV= mysqli_fetch_assoc($query);
	if ($GV['ServiceNumber']){
		array_push($Report_search_array,$GV['ServiceNumber']."::".$GV['PartMold']."::".$GV['FormingMachine']."::".$GV['TicketNumber']);
	}
};

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Index_Search</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">

T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z1{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#0000FF ;
	font-weight:bolder;
	font-size:14px;
	}
Z2{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#CC00FF ;
	font-weight:bolder;
	font-size:14px;
	}
Z3{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#00DD00;
	font-weight:bolder;
	font-size:14px;
	}
.BT1{
	margin:3px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:40px;
	background-color:#CCEEFF;
	}
.BT2{
	font:bold;
	font:,"Arial Black", Gadget, sans-serif;
	border:#009;
	border:1px;
	border-radius:3px;
	margin-left:3px;
	height:25px;
	width:80px;
	background-color:#afeeee;
	color:#000000;
	font-size:16px;
	border-style:solid;
	cursor:pointer;
	}

</style>

<script>

function Report(N){
	parent.Index_Content.location.href="Data_ReMeasureGV-1.php?SN="+N+"&floor=<?php echo $floor ?>";
};
	
</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<table border="0">
	<tr>
    	<td width="1250" height="30" colspan="2" align="center" ><T>>>  GV  復 判 巡 檢 量 測 報 告  <<</T></td>
        
    </tr>
    <tr>
    	<td width="100"><table border="1" bgcolor="#FFFFFF">
            <tr><td><Z1>>模具編號<</Z1></td></tr>
            <tr><td><Z2>>成型機台<</Z2></td></tr>
            <tr><td><Z3>>產品批號<</Z3></td></tr>
        </table></td>
    
    
        <td align="Left">
            <div id="Report" style="height:75px" >
            <?php
            while($BB = array_shift($Report_search_array)){
                $ReportB1 = substr($BB,0,stripos($BB,"::",0));
                $ReportB2 = substr($BB,(strlen($ReportB1)+2),(stripos($BB,"::",strlen($ReportB1)+2))-(strlen($ReportB1)+2));
                $ReportB3 = substr($BB,(strlen($ReportB1)+strlen($ReportB2)+4),(stripos($BB,"::",strlen($ReportB1)+strlen($ReportB2)+4))-(strlen($ReportB1)+strlen($ReportB2)+4));
                $ReportB4 = substr($BB,(strlen($ReportB1)+strlen($ReportB2)+strlen($ReportB3)+6));
                echo "<button type=button id=GV".$ReportB1." value=".$ReportB1." class=BT1 onClick=Report(".$ReportB1.")><Z1>".$ReportB2."</Z1>_<Z2>".$ReportB3."</Z2><br>【<z3>".$ReportB4."</z3>】</button>";
            }
            ?>
            </div>
        </td>
    </tr>
</table>
</form>

</body>
</html>