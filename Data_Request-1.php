﻿<?php
session_start();
error_reporting(0);
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';

date_default_timezone_set('Asia/Taipei');

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_Request-1</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/CodeSeven-toastr/toastr.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet"  href="../../Public/library/JQuery/CodeSeven-toastr/toastr.css"/>

<style>
T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px; 
	}
Z{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#0080FF;
	font-weight:bolder;
	font-size:12px;
	}
Z0{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:20px;
	}
Z1{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:16px;
	}

.textcss-1 {
    /* Size and position */
    width: 200px;
	height:22px;
	margin-left:3px;
    /* Styles */
    background:#afeeee;
    border-radius: 2px;
    border:1px;
	border-color:#000000;
	border-style:solid;
    box-shadow:#F99;
    cursor: pointer;
    outline: none;
    /* Font settings */
    font-weight: bold;
    color:#000000;
	}
.BT1{
	font:bold;
	font:,"Arial Black", Gadget, sans-serif;
	border:#009;
	border:1px;
	border-radius:3px;
	margin-left:3px;
	height:25px;
	width:80px;
	background-color:#afeeee;
	color:#000000;
	font-size:16px;
	border-style:solid;
	cursor:pointer;
	}
.BT2{
	font:bold;
	font:,"Arial Black", Gadget, sans-serif;
	border:#009;
	border:1px;
	border-radius:3px;
	margin-left:3px;
	height:25px;
	width:500px;
	color:#000000;
	font-size:16px;
	border-style:solid;
	cursor:pointer;
	}
</style>

<script>

$(function () {
	// setup enter to next input element function
	setupEnterToNext();
});
// enter to next input element function
function setupEnterToNext() {
	// add keydown event for all inputs
	$(':input').keydown(function (e) {
		if (e.keyCode == 13 /*Enter*/) {
			// focus next input elements
			$(':input:visible:enabled:eq(' + ($(':input:visible:enabled').index(this) + 1) + ')').focus();
			e.preventDefault();
		}
	});
}

function ShowPerson() {
	var RequestPerson = document.getElementById("RequestPerson").value;
	var Sqq = "SELECT name FROM `xz_mim_omm_account` WHERE id = '"+RequestPerson+"' ";
	document.getElementById("RequestPersonShow1").value="";
	document.getElementById("RequestPersonShow2").value="";
	$.getJSON("Data_RequestFunction.php?Type=person&floor=<?php echo $floor ?>",{Sql:Sqq},function(result){
		if (result!=""){
			$.each(result, function(i, field){
				$("#RequestPersonShow1").val('人員姓名 : '+field);	
				$("#RequestPersonShow2").val('人員工號 : '+RequestPerson);
			});
		}else{
			alert('請輸入正確的 【 人員工號條碼 】  。');
			document.getElementById("RequestPerson").value = "";
		};
	});
}
function ShowProduct() {
	var RequestProduct = document.getElementById("RequestProduct").value;
	document.getElementById("RequestTicket").value = "";
	document.getElementById("RequestProductShow1").value="";
	document.getElementById("RequestProductShow2").value="";
	document.getElementById("RequestProductShow3").value="";
	document.getElementById("RequestProductShow4").value="";
	document.getElementById("RequestProductShow5").value="";
	document.getElementById("RequestProductShow6").value="";
	document.getElementById("RequestProductShow7").value="";
	if (RequestProduct.substr(0,1)+RequestProduct.substr(9,1)+RequestProduct.substr(16,1)=="BDL"){
		var Sqq = "SELECT PartNumber , PartMold , Material , DrawingNumber , CavityNumber , MeasureMachine FROM <?php echo $DBtable1 ?> WHERE Barcode = '"+RequestProduct+"' ";
		$.getJSON("Data_RequestFunction.php?Type=product&floor=<?php echo $floor ?>",{Sql:Sqq},function(result){
			if (result!=""){
				$.each(result, function(i, field){
					var findshow = field.split('::');
					$("#RequestProductShow1").val('產品料號 : '+findshow[0]);
					$("#RequestProductShow2").val('產品模號 : '+findshow[1]);
					$("#RequestProductShow3").val('材料規格 : '+findshow[2]);
					$("#RequestProductShow4").val('藍圖版次 : '+findshow[3]);
					$("#RequestProductShow5").val('模具穴數 : '+findshow[4]);
					$("#RequestProductShow7").val(findshow[5]);
				});
			}else{
				alert("查無此 【 產品條碼 】 ， 請與成型品保組長聯繫 。");
				document.getElementById("RequestProduct").value = "";
			};
		});
	}else{
		alert("請輸入正確的 【 產品條碼 】  。");
		document.getElementById("RequestProduct").value = "";
	}
}
function ShowTicket() {
	var RequestProductShow1 = (document.getElementById("RequestProductShow1").value).substr(7);
	if (RequestProductShow1 != ""){
		var RequestTicket = document.getElementById("RequestTicket").value;
		document.getElementById("RequestProductShow6").value="";
		if (RequestTicket.length==14){
			var Sqq = "SELECT Prod_Line_Code, Primary_Item_NO, Mold_Num, Eng_Revision FROM fl_rc_tickets WHERE Ticket_NO = '"+RequestTicket+"' ";
			$.getJSON("Data_RequestFunction.php?Type=ticket&floor=<?php echo $floor ?>",{Sql:Sqq},function(result){
				if (result!=""){
					$.each(result, function(i, field){
						var RequestProductShow2 = (document.getElementById("RequestProductShow2").value).substr(7);
						var RequestProductShow4 = (document.getElementById("RequestProductShow4").value).substr(7);
						var findshow = field.split('::');
						if (RequestProductShow1==findshow[1] && RequestProductShow2==findshow[2] && RequestProductShow4==findshow[3]){
							$("#RequestProductShow6").val('成型線別 : '+findshow[0]);	
						}else{
							alert("請確認 送測產品 的 【 產品料號、產品模號、藍圖版次 】 是否與設定一致 。\n\n產品條碼 ： 【 "+RequestProductShow1+" 、 "+RequestProductShow2+" 、 "+RequestProductShow4+" 】\n\n產品批號 ： 【 "+findshow[1]+" 、 "+findshow[2]+" 、 "+findshow[3]+" 】 ");
							document.getElementById("RequestTicket").value = "";
						};
					});
				}else{
					alert("查無此 【 產品批號 】 ， 請與成型品保組長聯繫 。");
					document.getElementById("RequestTicket").value = "";
				};
			});
		}else{
			alert("請輸入正確的 【 產品批號 】  。");
			document.getElementById("RequestTicket").value = "";
		}
	}else{
		alert("請先刷的 【 產品條碼 】  。");
		document.getElementById("RequestTicket").value = "";
	}
}
function Request() {
	var RequestPersonShow1 = (document.getElementById("RequestPersonShow1").value).substr(7);
	var RequestProductShow1 = (document.getElementById("RequestProductShow1").value).substr(7);
	var RequestProductShow6 = (document.getElementById("RequestProductShow6").value).substr(7);
	if (RequestPersonShow1 != "" && RequestProductShow1 != "" && RequestProductShow6 != ""){
		var Today = new Date();
		Ty = "0" + Today.getFullYear();
        To = "0" + (Today.getMonth()+1);
        Td = "0" + Today.getDate();
		Th = "0" + Today.getHours();
        Tm = "0" + Today.getMinutes();
        Ts = "0" + Today.getSeconds();
        Ty = Ty.substring(Ty.length - 4, Ty.length + 1);
        To = To.substring(To.length - 2, To.length + 1);
        Td = Td.substring(Td.length - 2, Td.length + 1);
        Th = Th.substring(Th.length - 2, Th.length + 1);
        Tm = Tm.substring(Tm.length - 2, Tm.length + 1);
        Ts = Ts.substring(Ts.length - 2, Ts.length + 1);
		var DateTime1 = Ty+To+Td+Th+Tm+Ts;
		if(Today.getHours() < 8){
			var dayOfMonth = Today.getDate();
			Today.setDate(dayOfMonth - 1);
			Ty = "0" + Today.getFullYear();
        	To = "0" + (Today.getMonth()+1);
        	Td = "0" + Today.getDate();
        	Ty = Ty.substring(Ty.length - 4, Ty.length + 1);
    	    To = To.substring(To.length - 2, To.length + 1);
	        Td = Td.substring(Td.length - 2, Td.length + 1);
			var DateTime2 = Ty+"-"+To+"-"+Td;
		}else{
			var DateTime2 = Ty+"-"+To+"-"+Td;
		};
		var RequestTicket = document.getElementById("RequestTicket").value;
		var RequestPersonShow2 = (document.getElementById("RequestPersonShow2").value).substr(7);
		var RequestProductShow2 = (document.getElementById("RequestProductShow2").value).substr(7);
		var RequestProductShow3 = (document.getElementById("RequestProductShow3").value).substr(7);
		var RequestProductShow4 = (document.getElementById("RequestProductShow4").value).substr(7);
		var RequestProductShow5 = (document.getElementById("RequestProductShow5").value).substr(7);
		var RequestProductShow7 = document.getElementById("RequestProductShow7").value;
		var Sqc = "SELECT MAX(ServiceNumber) FROM <?php echo $DBtable2 ?> WHERE TicketNumber = '"+RequestTicket+"'";
		var Sqq = "INSERT INTO <?php echo $DBtable2 ?> (ServiceNumber, Status, TicketNumber, PartNumber, PartMold, Material, DrawingNumber, CavityNumber, FormingMachine, MeasureMachine, IPQC_Inspector, DateTime) VALUES ( '"+DateTime1+"' , '1' , '"+RequestTicket+"' , '"+RequestProductShow1+"' , '"+RequestProductShow2+"' , '"+RequestProductShow3+"' , '"+RequestProductShow4+"' , '"+RequestProductShow5+"' , '"+RequestProductShow6+"' , '"+RequestProductShow7+"' , '"+RequestPersonShow1+"-"+RequestPersonShow2+"' , '"+DateTime2+"')";
		$.getJSON("Data_RequestFunction.php?Type=request&floor=<?php echo $floor ?>",{Sql:Sqq,Sqll:Sqc,TT:DateTime1},function(result){
			$.each(result, function(i, field){
				if (field == "repeat"){
					Remark("ng",RequestTicket,RequestProductShow1,RequestProductShow2);
				}else{
					Remark("ok",RequestTicket,RequestProductShow1,RequestProductShow2);
				};
			});
		});
	}else{
		alert("請輸入正確的 【 人員條碼 】 & 【 產品條碼 】 & 【 產品批號 】 。");
	};
};

function Remark(a,b,c,d){
	if(a=="ok"){
		document.getElementById("RequestProduct").value = "";
		document.getElementById("RequestTicket").value = "";
		document.getElementById("RequestProductShow1").value="";
		document.getElementById("RequestProductShow2").value="";
		document.getElementById("RequestProductShow3").value="";
		document.getElementById("RequestProductShow4").value="";
		document.getElementById("RequestProductShow5").value="";
		document.getElementById("RequestProductShow6").value="";
		document.getElementById("RequestProductShow7").value="";
		Command: toastr['success']('<Z0>'+b+'</Z0><br><Z1>【'+c+'】-【'+d+'】</Z1>','送測成功')
		toastr.options = {
			  'closeButton': false,
			  'debug': false,
			  'newestOnTop': false,
			  'progressBar': true,
			  'positionClass': 'toast-top-right',
			  'preventDuplicates': false,
			  'onclick': null,
			  'showDuration': '30000',
			  'hideDuration': '30000',
			  'timeOut': '30000',
			  'extendedTimeOut': '5000',
			  'showEasing': 'swing',
			  'hideEasing': 'linear',
			  'showMethod': 'fadeIn',
			  'hideMethod': 'fadeOut'
		};
	}else{
		Command: toastr['error']('<Z0>'+b+'</Z0><br><Z1>【'+c+'】-【'+d+'】</Z1>','送測失敗')
		toastr.options = {
			  'closeButton': false,
			  'debug': false,
			  'newestOnTop': false,
			  'progressBar': true,
			  'positionClass': 'toast-top-right',
			  'preventDuplicates': false,
			  'onclick': null,
			  'showDuration': '30000',
			  'hideDuration': '30000',
			  'timeOut': '30000',
			  'extendedTimeOut': '5000',
			  'showEasing': 'swing',
			  'hideEasing': 'linear',
			  'showMethod': 'fadeIn',
			  'hideMethod': 'fadeOut'
		};
	}
}
</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<table border="1" bgcolor="#FFFFFF">
	<tr><td height="80" colspan="3" align="center" bgcolor="#B9FFB7" ><T>成  型  巡  檢  送  測  產  品</T></td></tr>
    <tr>
    	<td width="150" height="100" align="center"><L>第 一 步 ：</L><br><z>送測人員工號</z></td>
        <td width="400" height="100" align="center">
        	<input type="text" name="RequestPerson" id="RequestPerson" style="font-size:18px; width:300px; height:60px; text-align:center; background-color:#FFFF80;" placeholder="請刷 【人員工號條碼】"  onchange="ShowPerson()">
		</td>
        <td width="300" rowspan="3" align="Left" valign="top">
        	<br>
            &emsp;&#9830;&emsp;<L>送測人員：</L><BR>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            <div style="padding-left:40px;height:80px">
                <input type="text" name="RequestPersonShow1" id="RequestPersonShow1" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled" ><br>
                <input type="text" name="RequestPersonShow2" id="RequestPersonShow2" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled" >
            </div>
            <br><br>
            &emsp;&#9830;&emsp;<L>送測產品：</L><BR>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            <div style="padding-left:40px;height:230px">
                <input type="text" name="RequestProductShow1" id="RequestProductShow1" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled"><br>
                <input type="text" name="RequestProductShow2" id="RequestProductShow2" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled"><br>
                <input type="text" name="RequestProductShow3" id="RequestProductShow3" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled"><br>
                <input type="text" name="RequestProductShow4" id="RequestProductShow4" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled"><br>
                <input type="text" name="RequestProductShow5" id="RequestProductShow5" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled"><br>
                <input type="text" name="RequestProductShow6" id="RequestProductShow6" style="font-size:16px; width:200px; height:30px; text-align:Left; border:hidden; background-color:#FFF" disabled="disabled"><br>
                <input type="text" name="RequestProductShow7" id="RequestProductShow7" hidden>
            </div>
        </td>
	</tr>
    <tr>
    	<td width="150" height="100" align="center"><L>第 二 步 ：</L><br><z>送測產品條碼</z></td>
        <td width="400" height="100" align="center">
        	<input type="text" name="RequestProduct" id="RequestProduct" style="font-size:18px; width:300px; height:60px; text-align:center; background-color:#80FFFF;" placeholder="請刷 【產品條碼】" onchange="ShowProduct()" >
		</td>
	</tr>
    <tr>
    	<td width="150" height="100" align="center"><L>第 三 步 ：</L><br><z>送測產品批號</z></td>
        <td width="400" height="100" align="center">
        	<input type="text" name="RequestTicket" id="RequestTicket" style="font-size:18px; width:300px; height:60px; text-align:center; background-color:#80FF80;" placeholder="請刷 【產品批號】" onchange="ShowTicket()" >
		</td>
	</tr>
	<tr>
        <td height="80" align="center" colspan="3">
	        <input type="button" name="Button_Request" id="Button_Request" class="BT1" style="width:100px; height:50px" value="送測產品" onclick="Request()">
        </td>
	</tr>
</table>
<?php echo '<meta http-equiv="refresh" content="300" ; URL="Data_Request-1.php?floor='.$floor.'">' ?>
</form>

</body>
</html>
