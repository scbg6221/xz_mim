<?php
session_start();
error_reporting(0);
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');


date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d H:i:s",strtotime("-7 day"));

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Index_Search</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>

<style type="text/css">

T{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000080;
	font-size:36px;
	}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #000000;
	font-size:20px;
	}
M{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:16px;
	}
Z1{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#0000FF ;
	font-weight:bolder;
	font-size:12px;
	}
Z2{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#CC00FF ;
	font-weight:bolder;
	font-size:12px;
	}
Z3{
	font-family:"PMingLiU", Gadget, sans-serif;
	color:#00DD00;
	font-weight:bolder;
	font-size:12px;
	}	
	
.BT1{
	margin:3px;
	border:#000000;
	border:1px;
	border-radius:5px;
	height:40px;
	background-color:#CCEEFF;
	}
.BT2{
	font:bold;
	font:,"Arial Black", Gadget, sans-serif;
	border:#009;
	border:1px;
	border-radius:3px;
	margin-left:3px;
	height:25px;
	width:80px;
	background-color:#afeeee;
	color:#000000;
	font-size:16px;
	border-style:solid;
	cursor:pointer;
	}
.ui-autocomplete {
    max-height: 140px;
    overflow-y: auto;
    overflow-x: hidden;
  }

</style>

<script>

function result() {
	document['form1'].action = "Data_CheckReport-1.php?floor=<?php echo $floor ?>";
    document['form1'].target = 'Index_Content';
	};
	
function Reset() {
	document.getElementById('Report').innerHTML="";
	};

function ReportChange() {

	var Today = new Date();
	if (Today.getHours()<8){
		Today.setDate((Today.getDate())-1);
		var Year = Today.getFullYear();
		var Month = Today.getMonth()+1;
		var Day = Today.getDate()-1;
	}else{
		var Year = Today.getFullYear();
		var Month = Today.getMonth()+1;
		var Day = Today.getDate();
	}
	var SC1 = document.getElementById("SC1").value;
	var SC2 = document.getElementById("SC2").value;
	var SC3 = document.getElementById("SC3").value;
	var SC4 = document.getElementById("SC4").value;
	var SC1P = "";
	var SC2P = "";
	var SC3P = "";
	var	SC4P = "";
	
	if (SC1){
		SC1P = " And PartNumber LIKE '%" + SC1 + "%'";
	}
	if (SC2){
		SC2P = " And PartMold LIKE '%" + SC2 + "%'";
	}
	if (SC3){
		SC3P = " And FormingMachine LIKE '%" + SC3 + "%'";
	}
	if (SC4){
		SC4P = " And TicketNumber LIKE '%" + SC4 + "%'";
	}
	var Sqq = "SELECT ServiceNumber , PartNumber, PartMold , FormingMachine, TicketNumber  FROM <?php echo $DBtable3 ?> WHERE Status != 9 And MeasureEndTime > '<?php echo $DateTime ?>'" + SC1P + SC2P + SC3P + SC4P + " GROUP BY ServiceNumber";
	document.getElementById('Report').innerHTML = "" ;
	$.getJSON("Data_SearchReport.php?SearchType=Report_CK&floor=<?php echo $floor ?>",{Sql:Sqq},function(result){
		$.each(result, function(i, field){
		var findshow = field.split('::');
		var A = findshow[0];
		var B = findshow[1];
		var C = findshow[2];
		var D = findshow[3];
		var E = findshow[4];
		$("#Report").append('<button type=submit name=SN id=SN value='+A+' class=BT1 onclick="result()"><z1>'+C+'</z1>-<z2>'+D+'</z2><br>【<z3>'+E+'</z3>】</button>');		
    });
  });
};

$(function() {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 	  
	$( "#SC1" ).autocomplete({
        source: function( request, response ) {
          $.getJSON( "Data_SearchReport.php?SearchType=SC1&floor=<?php echo $floor ?>", {
            term: extractLast( request.term )
          }, response )},
		position:{at:"right",collision:"fit"},
		close: function() {ReportChange()},
	});
	
	$( "#SC2" ).autocomplete({
        source: function( request, response ) {
          $.getJSON( "Data_SearchReport.php?SearchType=SC2&floor=<?php echo $floor ?>", {
            term: extractLast( request.term )
          }, response )},
		position:{at:"right",collision:"fit"},
		close: function() {ReportChange()},
	});
	
	$( "#SC3" ).autocomplete({
        source: function( request, response ) {
          $.getJSON( "Data_SearchReport.php?SearchType=SC3&floor=<?php echo $floor ?>", {
            term: extractLast( request.term )
          }, response )},
		position:{at:"right bottom",collision:"fit"},
		close: function() {ReportChange()},
	});
	
	$( "#SC4" ).autocomplete({
        source: function( request, response ) {
          $.getJSON( "Data_SearchReport.php?SearchType=SC4&floor=<?php echo $floor ?>", {
            term: extractLast( request.term )
          }, response )},
		position:{at:"right bottom",collision:"fit"},
		close: function() {ReportChange()},
	});
});

</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<table border="0">
	<tr>
    	<td width="1250" height="30" colspan="2" align="center"><T>>> 產 線 巡 檢 量 測 報 告  快 速 查 詢 <<</T></td>
        
    </tr>
    <tr>
        <td width="420"><table border="1">
            <tr>
                <td width="50" height="30" align="center"><M>機種</M></td>
                <td width="150" height="30" align="center"><input type="text" size="20" style="padding:3px" name="SC1" id="SC1"></td>
                <td width="50" height="30" align="center"><M>模號</M></td>
                <td width="150" height="30" align="center"><input type="text" size="20" style="padding:3px" name="SC2" id="SC2"></td>
            </tr>
            <tr>
                <td width="50" height="30" align="center"><M>機台</M></td>
                <td width="150" height="30" align="center"><input type="text" size="20" style="padding:3px"  name="SC3" id="SC3"></td>
                <td width="50" height="30" align="center"><M>批號</M></td>
                <td width="150" height="30" align="center"><input type="text" size="20" style="padding:3px"  name="SC4" id="SC4"></td>
            </tr>
        </table></td>
        <td align="Left" >
        	<div id="Report" style="height:75px" ></div>
        </td>
    </tr>
</table>
</form>

</body>
</html>