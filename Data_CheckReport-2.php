﻿<?php
session_start();
error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$ChineseName=$_SESSION['ChineseName'];
$ID=$_SESSION['ID'];
$floor=$_GET['floor'];
if($floor==1){
	$DBtable1="request_list";
	$DBtable2="request_measure";
	$DBtable3="measurecontent";
	$DBtable4="measuredata";
	$DBtable5="remeasure";
	$DBtable6="measuredata_b";
}elseif($floor==2){
	$DBtable1="3f_request_list";
	$DBtable2="3f_request_measure";
	$DBtable3="3f_measurecontent";
	$DBtable4="3f_measuredata";
	$DBtable5="3f_remeasure";
}
$GVT=$_POST['GVT'];
$GVT=explode('::',$GVT);
mysqli_select_db($connect,$database);

date_default_timezone_set('Asia/Taipei');
$DateTime=date("Y-m-d H:i:s");
if (date("H")<8){
	$DatePoint=date("Y-m-d 08:00:00",strtotime("-7 day"));
}else{
	$DatePoint=date("Y-m-d 08:00:00");
}

if (!$GVT[1] && !$GVT[2]){
	
	$SCT=explode(':SC:',$GVT[0]);
	if (!$SCT[1]){
		$sql= "UPDATE ".$DBtable3." SET Status = '9', IPQC_Inspector = '".$ChineseName."-".$ID."' , EndDate = '".$DateTime."' WHERE ServiceNumber = '".$SCT[0]."' ";
		$query= mysqli_query($connect,$sql) ;
		$SN=$SCT[0];
		echo "<script>
			parent.Index_Content.location.href='Data_CreateReport-1.php?floor=".$floor."&SN=".$SN."';
		</script> ";
	}else{
		$Word = '';
		while($EDim = substr($SCT[1],0,stripos($SCT[1],"; "))){
			$EDimW=explode('#',$EDim);
			$sql="UPDATE ".$DBtable4." SET SCAR = '".$EDimW[1]."', DimNO = '".$EDimW[0]."' WHERE ServiceNumber = '".$SCT[0]."' AND DimNO = '".$EDimW[0]."'";
			$query = mysqli_query($connect,$sql) ;
			$SCT[1]=substr($SCT[1],strlen($EDim)+2);
		}
		$sql= "UPDATE ".$DBtable3." SET Status = '9', IPQC_Inspector = '".$ChineseName."-".$ID."'  , EndDate = '".$DateTime."' WHERE ServiceNumber = '".$SCT[0]."' ";
		$query= mysqli_query($connect,$sql) ;
		$SN=$SCT[0];
		echo "<script>
			parent.Index_Content.location.href='Data_CreateReport-1.php?floor=".$floor."&SN=".$SN."';
		</script> ";
	}
}elseif (!$GVT[1]){
	
	$sql= "INSERT INTO ".$DBtable5." ( ServiceNumber , InspectionMethod , Sequence , GVDim , StartTime ) VALUES ( '".$GVT[0]."' , 'PJ' , '1' , '".$GVT[2]."' , '".$DateTime."' )";
	$query= mysqli_query($connect,$sql) or die('insert database fail');
	$sql= "UPDATE ".$DBtable3." SET Status = '2' , IPQC_Inspector = '".$ChineseName."-".$ID."' WHERE ServiceNumber = '".$GVT[0]."' ";
	$query= mysqli_query($connect,$sql) or die('insert database fail');	
	echo "<script>alert('成功提出 【 自行覆判 】 申請 ， \\n\\n請到量測室領回產品 ， 於投影機進行覆判 。');
		parent.Index_Search.location.href='Data_SearchTop_CK.php?floor=".$floor."';
		parent.Index_Content.location.href='Data_CheckReport-0.php?floor=".$floor."';
	</script> ";
	
}elseif(!$GVT[2]){
	
	$sql="SELECT MAX(Sequence) FROM ( SELECT Sequence FROM ".$DBtable5." WHERE InspectionMethod = 'GV' and StartTime > '".$DatePoint."' ) AS T";
	$query= mysqli_query($connect,$sql) ;
	$AA = mysqli_fetch_array($query);

	$sql= "INSERT INTO ".$DBtable5." ( ServiceNumber , InspectionMethod , Sequence , GVDim , StartTime )  VALUES ( '".$GVT[0]."' , 'GV' , '".($AA['0']+1)."' , '".$GVT[1]."' , '".$DateTime."' )";   
	$query= mysqli_query($connect,$sql) or die('insert database fail');
	$sql= "UPDATE ".$DBtable3." SET Status = '3' , IPQC_Inspector = '".$ChineseName."-".$ID."' WHERE ServiceNumber = '".$GVT[0]."' ";
	$query= mysqli_query($connect,$sql) or die('insert database fail');	
	echo "<script>alert('成功提出 【 GV覆判 】 申請 ， GV覆判等候順序為 ： 【 ".($AA['0']+1)." 】');
		parent.Index_Search.location.href='Data_SearchTop_CK.php?floor=".$floor."';
		parent.Index_Content.location.href='Data_CheckReport-0.php?floor=".$floor."';
	</script> ";
	
}else{
	
	$sql="SELECT MAX(Sequence) FROM ( SELECT Sequence FROM ".$DBtable5." WHERE InspectionMethod = 'GV' and StartTime > '".$DatePoint."' ) AS T";
	$query= mysqli_query($connect,$sql) ;
	$AA = mysqli_fetch_array($query);

	$sql= "INSERT INTO ".$DBtable5." ( ServiceNumber , InspectionMethod , Sequence , GVDim , StartTime )  VALUES ( '".$GVT[0]."' , 'GV' , '".($AA['0']+1)."' , '".$GVT[1]."' , '".$DateTime."' )";   
	$query= mysqli_query($connect,$sql) or die('insert database fail');
	$sql= "INSERT INTO ".$DBtable5." ( ServiceNumber , InspectionMethod , Sequence , GVDim , StartTime ) VALUES ( '".$GVT[0]."' , 'PJ' , '1' , '".$GVT[2]."' , '".$DateTime."' )";
	$query= mysqli_query($connect,$sql) or die('insert database fail');
	$sql= "UPDATE ".$DBtable3." SET Status = '1' , IPQC_Inspector = '".$ChineseName."-".$ID."' WHERE ServiceNumber = '".$GVT[0]."' ";
	$query= mysqli_query($connect,$sql) or die('insert database fail');	
	echo "<script>alert('成功提出 【 GV覆判 】 與 【 自行覆判 】 申請 ，\\n\\nGV覆判等候順序為 ： 【 ".($AA['0']+1)." 】 ，\\n\\n待GV覆判完成到量測室領回產品 ， 於投影機進行覆判 。');
		parent.Index_Search.location.href='Data_SearchTop_CK.php?floor=".$floor."';
		parent.Index_Content.location.href='Data_CheckReport-0.php?floor=".$floor."';
	</script> ";
		
}
?>
