<?php 
session_start();
error_reporting(0);
$TC=$_REQUEST['TC'];

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

//require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
//require_once'../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
//require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

$submit=$_POST['submit'];
if (strstr("$submit","下載")){
	$c1=str_split($submit,3);
	$c2=$_POST['SN'];
	$c2=explode("#",$c2);
	$SN = $c2[$c1[2]-1]; 
	$sql="SELECT * FROM `servicerequest` WHERE `ServiceNumber`='$SN'";
	$query= mysqli_query($connect,$sql) ;
	$AA = mysqli_fetch_array($query);
	
	//下載圖紙
	$filename=$AA['ProductReport']; // 下載的檔名
	if ( mb_strlen($filename, 'Big5') != strlen($filename) ) {
		$filename = iconv('UTF-8', 'Big5', $filename);
		}

	$file="Report/Service_Report/".$filename; // 實際檔案的路徑+檔名
	
    header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename."");//指定下載時的檔名
	header("Content-Length: " .(string)(filesize($file)));//防止資料傳輸不完整(office打的開,但WPS打不開)
	ob_end_clean();//清除緩衝區的內容(防止檔案損毀完全打不開)
	readfile($file);//輸出下載

};
	
mysqli_select_db($connect,$database);
$query_listoutF="SELECT * FROM `servicerequest` WHERE `Status`='9' ORDER BY `ProductEndDate` Desc";
$listoutF = mysqli_query($connect,$query_listoutF) or die(mysqli_error());

date_default_timezone_set('Asia/Taipei');
$DateTime=date("YmdHis",strtotime("-7 day"));

?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_ShowStatus-2</title>

<script src="../../Public/library/Other/Sorttable.js"></script>

<style type="text/css">
#table-2 {
	
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0.5%;   
	margin-top:0.5%;
		
}
#table-2.sortable thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: <?php echo "#".$TC ?> ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	width:auto;
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 16px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
	text-shadow: white 1px 1px 1px;
}
#table-2 td {
	padding: 5px;
	color: #333;
	line-height: 15px;
	width:auto;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 14px;
	text-align:center;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
#table-2 ee {
	font-size: 14px;
	text-align:center;
	color: #32cd32;
}
</style>
</head>

<script type="text/javascript">
function result() {
	document['form1'].action = "Service_ShowStatus-2.php";
	document['form1'].target = 'Index_Content';
}
</script>

<body>
<?php echo "【 點擊表頭可以更改排序 】"?>
<form id="form1" name="form1" method="post" >
<table id="table-2" class="sortable">
<thead>
<th>申請狀態</th>
<th>報告</th>
<th>完成時間</th>
<th>量測人員</th>
<th>申請單流水號</th>
<th>產品品名</th>
<th>正崴料號</th>
<th>送樣數量</th>
<th>量測尺寸數</th>
<th>委託部門</th>
<th>委託人</th>
<th>電話</th>
</thead>
<tbody>
<?php
$N=0;
while($listout = mysqli_fetch_assoc($listoutF)){
	if ( strtotime($listout['ProductEndDate'])-strtotime($DateTime)>0 ){
		$N=$N+1;
		echo "<tr>";
		switch ( $listout['Status'] ){
			case "9":
			  $SNS="<ee>量測完成</ee>";
			  break;
			default:
			  echo " ";
		}
		echo "<td>" . $SNS . "</td>";
		echo "<td>" ."<input type=submit name=submit id=submit value=下載$N ; onClick=result()>". "</td>";
		echo "<td>" . $listout['ProductEndDate'] . "</td>";
		echo "<td>" . $listout['ProductEndPersonnel'] . "</td>";
		echo "<td>" . $listout['ServiceNumber'] . "</td>";
		$aa[$N] = $listout['ServiceNumber'];
		echo "<td>" . $listout['ProductName'] . "</td>";
		echo "<td>" . $listout['ProductFoxlinkPN'] . "</td>";
		echo "<td>" . $listout['ProductPcsQuantity'] . "</td>";
		echo "<td>" . $listout['ProductOneQuantity'] . "</td>";
		echo "<td>" . $listout['RequestDepartment'] . "</td>";
		echo "<td>" . $listout['RequestName'] . "</td>";
		echo "<td>" . $listout['RequestPhone'] . "</td>";
		echo "</tr>";
	}
}
if ($N>0){
	$SN=implode("#",$aa);
	echo "<input type=hidden name=SN id=SN value=$SN>";
	echo "<input type=hidden name=TC id=TC value=$TC>";
}
?>
</tbody>
</table>
</form>
</body>
</html>
