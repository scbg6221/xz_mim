<?php
require('../../Public/library/tcpdf/tcpdf.php');
require_once('../../Public/Connections/omm_system_xz_mim.php');

date_default_timezone_set('Asia/Taipei');
$filedir='Report/IPQC_Barcode/IPQC/'.date("YmdHis");
mkdir($filedir);
$IpqcID=$_GET['IpqcID']." ";

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'utf8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sanyi_Sun');
$pdf->SetTitle('IPQC量測巡檢-人員資訊');
$pdf->SetSubject('IPQC量測巡檢-人員資訊');
$pdf->SetKeywords('IPQC, PartNumber');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
//$pdf->SetFont('dejavusans', '', 14, '', true);
//$pdf->SetFont('msungstdlight','',16);
$pdf->SetFont('cid0jp','', 18); 

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$str = "";
$N=1;
while($EDim = substr($IpqcID,0,stripos($IpqcID,"; "))){
	//$url = "http://192.168.11.141/MainWebsite/Public/library/Bracode/barcode_generator-2005-09-08/html/image.php?code=code93&o=1&t=40&r=1&text=".$EDim."&f=2&a1=&a2=";
	$url = "http://192.168.11.141/MainWebsite/Public/library/Bracode/barcode_generator-2005-09-08/html/image.php?code=code128&o=1&t=40&r=1&text=".$EDim."&f=2&a1=A&a2=";
	$JpegFile = $filedir."/".$EDim.".jpeg";
	$image_data = file_get_contents($url);
	$image = imagecreatefromstring($image_data);
	imagejpeg($image,$JpegFile);
	$str=$str.'
		<th style="width:0.1px;"></th>
		<th style="text-align:center; line-height:95px; width:210px;"><img src="http://192.168.11.141/MainWebsite/GV_System/FQ_Conn/'.$JpegFile.'"></th>
	';
	if($N%3 == 0){
		$str=$str.'</tr><tr>';
	}
	$IpqcID=substr($IpqcID,strlen($EDim)+2);
	$N=$N+1;
};
if($N%3 == 1){
	$str=substr($str,0,-9);
}

$html ='<div border: 1px solid #ffffff;><table width="100%" border="1"><tr>'.$str.'</tr></table></div>';

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('IPQC-Barcode.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


SureRemoveDir($filedir , true);
function SureRemoveDir($dir, $DeleteMe) {
	if(!$dh = @opendir($dir)) return;
	while (false !== ($obj = readdir($dh))) {
		if($obj=='.' || $obj=='..') continue;
		if (!@unlink($dir.'/'.$obj)) SureRemoveDir($dir.'/'.$obj, true);
	}
	if ($DeleteMe){
		closedir($dh);
		@rmdir($dir);
	}
}

?>