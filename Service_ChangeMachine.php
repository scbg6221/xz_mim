<?php 
session_start();
//error_reporting(0);

include '../../Public/MainWebUI/User_Count.php';

include '../../Public/MainWebUI/Login_Control.php';

require_once('../../Public/Connections/omm_system_xz_mim.php');

mysqli_select_db($connect,$database);

if (isset($_POST['MN']))
{
	$SN = $_POST['SN'];	

	$MN = $_POST['MN'];

	$sql="SELECT * FROM `servicerequest` WHERE `ServiceNumber`='$SN'";

	$query= mysqli_query($connect, $sql);

	$Uch = mysqli_fetch_array($query);

	$S_Old = $Uch['Sequence'];

	$MN_Old = $Uch['MachineNumber'];

	$sql= "SELECT MAX( `Sequence` ) `MAX` FROM ( SELECT `Sequence` FROM `servicerequest` WHERE `MachineNumber` = '$MN' ) As T ";
	$query= mysqli_query($connect,$sql) ; 

	$AA = mysqli_fetch_array($query);

	$S = $AA['MAX'] + 1;

	$sql= "UPDATE `servicerequest` SET `Sequence` = '$S' , `MachineNumber` = '$MN' WHERE `ServiceNumber` = '$SN'";

	$query= mysqli_query($connect,$sql);

	$sql= "SELECT MAX( `Sequence` ) `MAX` FROM ( SELECT `Sequence` FROM `servicerequest` WHERE `MachineNumber` = '$MN_Old' ) As T ";
	$query= mysqli_query($connect,$sql) ;

	$AA = mysqli_fetch_array($query);

	$S = $AA['MAX'];

	while($S > $S_Old) 
	{
		$S_1 = $S_Old + 1;

  		$sql= "UPDATE `servicerequest` SET `Sequence` = '$S_Old' WHERE `Sequence` = '$S_1' and `MachineNumber` = '$MN_Old'";

		$query= mysqli_query($connect, $sql) ;

		$S_Old = $S_Old + 1;
	} 
}

$query_listoutF = "SELECT * FROM `servicerequest` WHERE `Status`='1' AND `Sequence`>'0' ORDER BY `Sequence` ASC";

$listoutF = mysqli_query($connect, $query_listoutF) or die(mysqli_error());

$mlistarr = array();

$querym = "SELECT * FROM `machine_list`";

$mreqsult = mysqli_query($connect,$querym);

while($mlist = mysqli_fetch_assoc($mreqsult))
{
	if($mlist['id'] != "0" )
	{
		$mlistarr[$mlist['Machine_Number']] = array();
	}
}
                                       
while($l = mysqli_fetch_assoc($listoutF))
{        
	if(array_key_exists($l["MachineNumber"],$mlistarr))
	{
    	array_push($mlistarr[$l["MachineNumber"]], $l);
	}
    //$listarr[$l["MachineNumber"]][] = $l;
} 
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_ChangeMachine</title>

<style type="text/css">
#table-2 {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0.5%;   
	margin-top:0.5%;	
}
#table-2 thead {
	width:auto;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: center;
	color: #4B4B4B;
	background-color: #7fff00 ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	width:auto;
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
	text-shadow: white 1px 1px 1px;
	border-top: 1px solid #000000;
}
#table-2 td {
	padding: 5px;
	text-align:center;
	color: #333;
	line-height: 15px;
	width:auto;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
L{
	font-family:"PMingLiU", Gadget, sans-serif;
	font-weight: bold;
	color: #005DBE;
	font-size:20px;
}
.priorityR {
	color:#FF8080;
}
.priorityG {
	color:#80FF80;
}
</style>

</head>

<script type="text/javascript">
function result(MN, SN) 
{
	document.getElementById("SN").value = SN;

    document.form1.action = "Service_ChangeMachine_1.php";

    document.form1.target = 'Index_Content';

    document.form1.submit();
}
</script>
<body>
<form id="form1" name="form1" method="post" >
<table id="table-2">
<?php 
foreach ($mlistarr as $k1 => $v1)
{
	$snarr = array();

echo <<<EOF
	<thead>
	<tr><th colspan="14"><L> $k1 號機台排程</L><input type='hidden' name='DeviceList[]' value='$k1'></th></tr>
	<th>更換量測機台</th>
	<th>量測序號</th>
	<th>委託等級</th>
	<th>申請單流水號</th>
	<th>產品品名</th>
	<th>正崴料號</th>
	<th>送樣數量</th>
	<th>量測尺寸數</th>
	<th>委託部門</th>
	<th>委託人</th>
	<th>電話</th>
	<th>電子郵箱</th>
	<th>委託日期</th>
	<th>需求日期</th>
	</thead>
EOF;
	$N=1;

	foreach ($v1 as $v2) 
	{
		if($v2['Priority'] == "ExtruUrgent")
		{
			$I3 = "<span class='priorityR'>特急</span>";
		}
		elseif($v2['Priority'] == "Urgent")
		{
			$I3 = "<span class='priorityG'>加急</span>";
		}
		else
		{
			$I3 = "普通";
		}

		echo "<tr>";
		echo "<td>" ."<input type='button' value='更換機台".$N."' onclick='result(\"".$k1."\",\"".$v2['ServiceNumber']."\")'>". "</td>";
		echo "<td>" . $v2['Sequence'] . "</td>";
		echo "<td>" . $I3 . "</td>";
		echo "<td>" . $v2['ServiceNumber'] . "</td>";
		echo "<td>" . $v2['ProductName'] . "</td>";
		echo "<td>" . $v2['ProductFoxlinkPN'] . "</td>";
		echo "<td>" . $v2['ProductPcsQuantity'] . "</td>";
		echo "<td>" . $v2['ProductOneQuantity'] . "</td>";
		echo "<td>" . $v2['RequestDepartment'] . "</td>";
		echo "<td>" . $v2['RequestName'] . "</td>";
		echo "<td>" . $v2['RequestPhone'] . "</td>";
		echo "<td>" . $v2['RequestEmail'] . "</td>";
		echo "<td>" . $v2['RequestDatetime'] . "</td>";
		echo "<td>" . $v2['ProductNeedDate'] . "</td>";
		echo "</tr>";

		$N++;
	}
	if(count($v1) == 0)
	{
		echo "<tr><td><BR></td></tr>";
	}
}
?>
</table>
	<input type="hidden" id="SN" name="SN" />
</form>
</body>
</html>