<?php 

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once('../../Public/Connections/omm_system_xz_mim.php');

$TC=$_REQUEST['TC'];

mysqli_select_db($connect,$database);

$query_listoutF1="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`>'0' and `MachineNumber`='GV_1' ORDER BY `Sequence` Asc";
$listoutF1 = mysqli_query($connect,$query_listoutF1) or die(mysqli_error());
$query_listoutF2="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`>'0' and `MachineNumber`='GV_2' ORDER BY `Sequence` Asc";
$listoutF2 = mysqli_query($connect,$query_listoutF2) or die(mysqli_error());
$query_listoutF3="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`>'0' and `MachineNumber`='QV_1' ORDER BY `Sequence` Asc";
$listoutF3 = mysqli_query($connect,$query_listoutF3) or die(mysqli_error());
$query_listoutF4="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`>'0' and `MachineNumber`='QV_2' ORDER BY `Sequence` Asc";
$listoutF4 = mysqli_query($connect,$query_listoutF4) or die(mysqli_error());
$query_listoutF5="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`>'0' and `MachineNumber`='QV_3' ORDER BY `Sequence` Asc";
$listoutF5 = mysqli_query($connect,$query_listoutF5) or die(mysqli_error());
$query_listoutF6="SELECT * FROM `servicerequest` WHERE `Status`='1' and `Sequence`>'0' and `MachineNumber`='QV_4' ORDER BY `Sequence` Asc";
$listoutF6 = mysqli_query($connect,$query_listoutF6) or die(mysqli_error());


?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Service_ShowProcess-3</title>

<style type="text/css">
#table-2 {
	width:inherit;
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;       
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0.5%;   
	margin-top:0.5%;
		
}

#table-2 thead {
	width:inherit;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: <? echo "#".$TC ?> ;
	border-bottom: solid 1px #999;
}
#table-2 th {
	padding: 5px;
	color: #333;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align:center;
	text-shadow: white 1px 1px 1px;
	background-color: #C3FFC0;
}
#table-2 td {
	padding: 5px;
	text-align: center;
	color: #333;
	line-height: 12px;
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	font-size: 12px;
	text-align: center;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}
L{
	font-weight: bold;
	color: #005DBE;
	font-size:24px;
	}
C{
	font-weight: bold;
	color: #005DBE;
	font-size:100%;
	}
</style>


</head>
<body background="Images/loginb.png">

<table id="table-1">
<tr>
<td colspan="5" align="center" valign="center">
	<font size="+6" face="Arial Black" COLOR="#004EA0">
	<strong>【ICBU】	Service Application System </strong></font>
</td>
</tr>
<tr>
<td width="250" valign="top">
    <table id="table-2">
    <thead>
    <tr><th colspan="4"><L>GV_1 號機台排程</L></th></tr>
    <th width="25">量測序號</th>
    <th>申請單流水號</th>
    <th>產品品名</th>
    <th width="25">委託人</th>
    </thead>
    <?php
    while($listout = mysqli_fetch_assoc($listoutF1))
        {		 
        echo "<tr>";
        echo "<td>" . $listout['Sequence'] . "</td>";
        echo "<td>" . $listout['ServiceNumber'] . "</td>";
        echo "<td>" . $listout['ProductName'] . "</td>";
        echo "<td>" . $listout['RequestName'] . "</td>";
        echo "</tr>";
        }
    ?>
    </table>
</td>
<td width="250" valign="top">
    <table id="table-2">
    <thead>
    <tr><th colspan="4"><L>GV_2 號機台排程</L></th></tr>
    <th width="25">量測序號</th>
    <th>申請單流水號</th>
    <th>產品品名</th>
    <th width="25">委託人</th>
    </thead>
    <?php
    while($listout = mysqli_fetch_assoc($listoutF2))
        {		 
        echo "<tr>";
        echo "<td>" . $listout['Sequence'] . "</td>";
        echo "<td>" . $listout['ServiceNumber'] . "</td>";
        echo "<td>" . $listout['ProductName'] . "</td>";
        echo "<td>" . $listout['RequestName'] . "</td>";
        echo "</tr>";
        }
    ?>
    </table>
</td>
<td width="250" valign="top">
    <table id="table-2">
    <thead>
    <tr><th colspan="4"><L>QV_1 號機台排程</L></th></tr>
    <th width="25">量測序號</th>
    <th>申請單流水號</th>
    <th>產品品名</th>
    <th width="25">委託人</th>
    </thead>
    <?php
    while($listout = mysqli_fetch_assoc($listoutF3))
        {		 
        echo "<tr>";
        echo "<td>" . $listout['Sequence'] . "</td>";
        echo "<td>" . $listout['ServiceNumber'] . "</td>";
        echo "<td>" . $listout['ProductName'] . "</td>";
        echo "<td>" . $listout['RequestName'] . "</td>";
        echo "</tr>";
        }
    ?>
    </table>
</td>
<td width="250" valign="top">
    <table id="table-2">
    <thead>
    <tr><th colspan="4"><L>QV_2 號機台排程</L></th></tr>
    <th width="25">量測序號</th>
    <th>申請單流水號</th>
    <th>產品品名</th>
    <th width="25">委託人</th>
    </thead>
    <?php
    while($listout = mysqli_fetch_assoc($listoutF4))
        {		 
        echo "<tr>";
        echo "<td>" . $listout['Sequence'] . "</td>";
        echo "<td>" . $listout['ServiceNumber'] . "</td>";
        echo "<td>" . $listout['ProductName'] . "</td>";
        echo "<td>" . $listout['RequestName'] . "</td>";
        echo "</tr>";
        }
    ?>
    </table>
</td>
<td width="250" valign="top">
    <table id="table-2">
    <thead>
    <tr><th colspan="4"><L>QV_3 號機台排程</L></th></tr>
    <th width="25">量測序號</th>
    <th>申請單流水號</th>
    <th>產品品名</th>
    <th width="25">委託人</th>
    </thead>
    <?php
    while($listout = mysqli_fetch_assoc($listoutF5))
        {		 
        echo "<tr>";
        echo "<td>" . $listout['Sequence'] . "</td>";
        echo "<td>" . $listout['ServiceNumber'] . "</td>";
        echo "<td>" . $listout['ProductName'] . "</td>";
        echo "<td>" . $listout['RequestName'] . "</td>";
        echo "</tr>";
        }
    ?>
    </table>
</td>
<td width="250" valign="top">
    <table id="table-2">
    <thead>
    <tr><th colspan="4"><L>QV_4 號機台排程</L></th></tr>
    <th width="25">量測序號</th>
    <th>申請單流水號</th>
    <th>產品品名</th>
    <th width="25">委託人</th>
    </thead>
    <?php
    while($listout = mysqli_fetch_assoc($listoutF6))
        {		 
        echo "<tr>";
        echo "<td>" . $listout['Sequence'] . "</td>";
        echo "<td>" . $listout['ServiceNumber'] . "</td>";
        echo "<td>" . $listout['ProductName'] . "</td>";
        echo "<td>" . $listout['RequestName'] . "</td>";
        echo "</tr>";
        }
    ?>
    </table>
</td>
</tr>
<?php echo '<meta http-equiv="refresh" content=10 ; URL=Bottom_ProcessView-2_0.php>' ?>
</table>
</body>
</html>


